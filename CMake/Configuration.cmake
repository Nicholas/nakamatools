
# Enabling project folders so that things are nicely grouped in visual studio
IF( MSVC )
	MESSAGE( STATUS " > Enabling MSVC folder support")
	SET_PROPERTY(GLOBAL PROPERTY USE_FOLDERS ON)
ENDIF( MSVC )
# This policy will automatically link our exe to the qtmain.lib
# CMAKE_POLICY( SET CMP0020 NEW )

# This policy allows us to use COMPILE_DEFINITIONS_<config> when using the OLD style, the new style
# is replaced with target_compile_definitions
# This policy should be specified explicitly
IF( POLICY CMP0043 )
	CMAKE_POLICY( SET CMP0043 NEW )
ENDIF( POLICY CMP0043 )

# OLD
#	SET_PROPERTY( TARGET tgt APPEND PROPERTY COMPILE_DEFINITIONS_DEBUG DEBUG_MODE )
#	SET_PROPERTY( DIRECTORY APPEND PROPERTY COMPILE_DEFINITIONS_DEBUG DIR_DEBUG_MODE )

# NEW
#	SET_PROPERTY( TARGET tgt APPEND PROPERTY COMPILE_DEFINITIONS $<$<CONFIG:Debug>:DEBUG_MODE> )
#	TARGET_COMPILE_DEFINITIONS( tgt PRIVATE $<$<CONFIG:Debug>:DEBUG_MODE> )
#	SET_PROPERTY( DIRECTORY APPEND PROPERTY COMPILE_DEFINITIONS $<$<CONFIG:Debug>:DIR_DEBUG_MODE> )

#SET( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /Wall /Zo /DNOMINMAX" )
#MESSAGE( STATUS "cxx Flags:" ${CMAKE_CXX_FLAGS} )


