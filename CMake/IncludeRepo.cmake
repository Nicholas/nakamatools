
FUNCTION( INCLUDE_REPO varName collectorName  )
	MESSAGE( STATUS "Including: \"${varName}\"" )

	CONFIGURE_FILE( CMake/${collectorName}.cmake ${varName}-download/CMakeLists.txt )

	EXECUTE_PROCESS( COMMAND ${CMAKE_COMMAND} -G "${CMAKE_GENERATOR}" .
		RESULT_VARIABLE result
		WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/${varName}-download
		OUTPUT_QUIET
		ERROR_QUIET
		)

	IF( result )
		MESSAGE( FATAL_ERROR "CMake step for ${varName} failed: ${result}" )
	ENDIF()

	EXECUTE_PROCESS( COMMAND ${CMAKE_COMMAND} --build .
		RESULT_VARIABLE result
		WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/${varName}-download
		OUTPUT_QUIET
		ERROR_QUIET
		)

	IF(result)
		MESSAGE( FATAL_ERROR "Build step for ${varName} failed: ${result}" )
	ENDIF()

	ADD_SUBDIRECTORY(
		${CMAKE_BINARY_DIR}/${varName}-src
		${CMAKE_BINARY_DIR}/${varName}-build
		EXCLUDE_FROM_ALL
	)
ENDFUNCTION( INCLUDE_REPO )