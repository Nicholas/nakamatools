CMAKE_MINIMUM_REQUIRED( VERSION 2.8.2 )

PROJECT( cjson-download NONE )

INCLUDE( ExternalProject )

ExternalProject_Add( cjson
	GIT_REPOSITORY		https://github.com/DaveGamble/cJSON.git
	GIT_TAG				master
	SOURCE_DIR			"${CMAKE_BINARY_DIR}/cjson-src"
	BINARY_DIR			"${CMAKE_BINARY_DIR}/cjson-build"
	CONFIGURE_COMMAND	""
	BUILD_COMMAND		""
	INSTALL_COMMAND		""
	TEST_COMMAND		""
    UPDATE_COMMAND "" 
)
	
