NaKama Tools
============
---
A useful set of C++ tools, from a simple console interface to multi-threaded processing.

This is used to provide a quick starting point to get a project up and running.

### Components

- Console
- Library 
- Observer
- Processing
- Threading
- Timing
- Factory
- Functors
- Multimethods
  
Have a look at the [wiki] for more information about each of the components.

## Getting Started
### Requires

- [CMake][cmake] 3.8.0  
  CMake is used to generate the build files,
  it will also download the [googletest][gtest] git repository automatically during configuration.
  
- [Git][git]  
  Not only useful for downloading this repo its also used by CMake to get the [googletest][gtest] repository.

- A C++ compiler  
  Tested with the following:  
    - Windows 10 Visual Studio 2015
    - Windows 10 MinGW 7.2
    - Ubuntu 17.10 GNU Compiler Collection 7.2

### Includes

- [PrecompiledHeader.cmake][cmakepch] is included so that we can set the project to build with precompiled headers.

- Uses [googletest][gtest] as the unit testing framework.

### Compiling and Running Unit Tests
#### Make

- From your build directory use the following console commands.
```commandline
cmake path/to/repo/  
make  
./Tests/NaKama-Tools-Test
```

#### CLion

- Using clion, open the repository.
- CLion should then run its cmake configuration step.
- Build and run the NaKama-Tools-Tests configuration.
- To improve the google test support, edit the configurations and add a
new Google Test and select "NaKama-Tools-Test" as the target.
This configuration will run with CLions testing tools.


#### Visual Studios

- Use CMake to build your visual studio project files.
- In the solution explorer navigate to "Tests/NaKama-Tools-Test" and set this as your StartUp project.
- Then build and run. 
- Running with "Start Without Debugger" will keep the console window open so that you can review the unit test results.

### Including NaKama Tools

To use NaKama Tools you will need to include *path/to/repo/Source* and link to the *NaKama-Tools* library.

#### CMake

If you have downloaded the repository so that it can be worked on as its own project then we can
include the project using the following line.

```cmake
ADD_SUBDIRECTORY( path/to/repo )
```

If you would rather treat the tools as a black box project then we can get CMake to download it for us.
Rather than using just ADD_SUBDIRECTORY we use the following lines instead to download this
repository for us at configuration time.

```cmake
CONFIGURE_FILE( NaKamaToolsCollector.cmake NaKamaTools-download/CMakeLists.txt )

EXECUTE_PROCESS( COMMAND ${CMAKE_COMMAND} -G "${CMAKE_GENERATOR}" .
	RESULT_VARIABLE result
	WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/NaKamaTools-download )

IF( result )
	MESSAGE( FATAL_ERROR "CMake step for NaKama-Tools failed: ${result}" )
ENDIF()

EXECUTE_PROCESS( COMMAND ${CMAKE_COMMAND} --build .
	RESULT_VARIABLE result
	WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/NaKamaTools-download )

IF( result )
	MESSAGE( FATAL_ERROR "Build step for NaKamaTools failed: ${result}" )
ENDIF()

ADD_SUBDIRECTORY(
	${CMAKE_BINARY_DIR}/NaKamaTools-src
	${CMAKE_BINARY_DIR}/NaKamaTools-build )
```

The above executes CMake using the following new file _NaKamaToolsCollector.cmake_.


```cmake
CMAKE_MINIMUM_REQUIRED( VERSION 2.8.2 )

PROJECT( NaKamaTools-download NONE )

INCLUDE( ExternalProject )

EXTERNALPROJECT_ADD( NaKamaTools
	GIT_REPOSITORY    git@bitbucket.org:Nicholas/nakamatools.git
	GIT_TAG           master
	SOURCE_DIR        "${CMAKE_BINARY_DIR}/NaKamaTools-src"
	BINARY_DIR        "${CMAKE_BINARY_DIR}/NaKamaTools-build"
	CONFIGURE_COMMAND ""
	BUILD_COMMAND     ""
	INSTALL_COMMAND   ""
	TEST_COMMAND      ""
)
```

Then use the following to get NaKama Tools as part of the project.

```cmake
INCLUDE_DIRECTORIES( "${NaKamaTools_SOURCE_DIR}/Source" )
TARGET_LINK_LIBRARIES( example NaKama-Tools )
```

The above method is used to collect the Google C++ testing framework as is described [here][gtestsetup].




[wiki]: https://bitbucket.org/Nicholas/groundzero/wiki

[loki]: http://loki-lib.sourceforge.net/
[gtest]: https://github.com/google/googletest
[gtestsetup]: https://github.com/google/googletest/tree/master/googletest
[cmakepch]: https://github.com/larsch/cmake-precompiled-header
[cmake]: https://cmake.org/
[git]: https://git-scm.com/
[cpp11fun]:http://www.stroustrup.com/C++11FAQ.html#std-function
[cpp11var]:http://www.stroustrup.com/C++11FAQ.html#variadic-templates

