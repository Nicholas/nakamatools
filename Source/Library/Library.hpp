#pragma once
// Our library.
// 
// This is where we go if we want to find some data.
// Of course we need to have the right book name
// and, if they don't have that book, then we provide
// the needed items to get a new book just for you.
//
// Project   : NaKama-Tools
// File Name : Library.hpp
// Date      : 18/05/2015
// Author    : Nicholas Welters

#ifndef _LIBRARY_HPP
#define _LIBRARY_HPP

#include "Library.h"

namespace tools
{
	using std::lock_guard;
	using std::mutex;

	/// <summary> ctor </summary>
	/// <param name="factory"> The data factory that can build new objects. </param>
	/// <param name="deleter"> The object that reclaims the objects memory (or releases it). </param>
	template< typename Data, typename DataID, typename Deleter, typename... Arguments >
	Library< Data, DataID, Deleter, Arguments... >::Library( Factory factory, Deleter deleter )
		: factory( factory )
		, deleter( deleter )
		, bookMap( )
	{ }
	
	/// <summary>
	/// We will get an object from the library with the provided key type,
	/// if the key doesn't exist then a new object will be created.
	/// The library always has to be up to date you know ;)
	/// </summary>
	/// <param name="id"> The data key, this is hashed and used to find the data. </param>
	/// <param name="args"> This is the argument list for the construction of a new data type.
	///						This is needed so that we can build a new object if it doesn't exist
	///						in the library. </param>
	/// <returns> A smart pointer to your object. </returns>
	template< typename Data, typename DataID, typename Deleter, typename... Arguments >
	typename Library< Data, DataID, Deleter, Arguments... >::DataSPtr
			 Library< Data, DataID, Deleter, Arguments... >::checkOut( const DataID& id, Arguments... args )
	{
		Book& book = bookMap[ id ];

		{
			lock_guard< mutex > lock( book.key );

			if( book.refCount == 0 )
			{
				book.p_data = factory( args... );
			}

			book.refCount++;
		}

		Deleter bookDeleter = deleter;

		BookLibrarian bookReturn = [ &book, bookDeleter ]( Data* )
		{
			lock_guard< mutex > lock( book.key );

			book.refCount--;

			if( book.refCount == 0 )
			{
				bookDeleter( book.p_data );
				book.p_data = nullptr;
			}
		};

		return DataSPtr( book.p_data, bookReturn );
	}
} // namespace tools

#endif // _LIBRARY_HPP