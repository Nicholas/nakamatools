#pragma once
// Our flyweight library.
// 
// This is where we go if we want to find some data.
// Of course we need to have the right book name
// and, if they don't have that book, then we provide
// the needed items to get a new book just for you.
//
// TODO: Make this thread safe.
//
// Project   : NaKama-Tools
// File Name : Library.h
// Date      : 18/05/2015
// Author    : Nicholas Welters

#ifndef _LIBRARY_H
#define _LIBRARY_H

#include <memory>
#include <unordered_map>
#include <functional>
#include <mutex>

namespace tools
{
	/// <summary>
	/// Custom deleter function.
	/// When the object is unwanted,
	/// its space in memory can be reclaimed.
	/// </summary>
	template< typename Data >
	using DefaultBookDeleter = std::function< void ( Data* ) >;

	/// <summary>
	/// Our flyweight library.
	/// 
	/// This is where we go if we want to find some data.
	/// Of course we need to have the right book name
	/// and, if they don't have that book, then we provide
	/// the needed items to get a new book just for you.
	/// </summary>
	/// <template name="Data"> The data type that we are going to handle as a flyweight. </template>
	/// <template name="DataID"> A unique id for the data we are holding. </template>
	/// <template name="Deleter"> The deleter type that we can use to release the flyweight back to the system. </template>
	/// <template name="Arguments"> The list of arguments that the factory will take to build the flyweight type. </template>
	template< typename Data, typename DataID, typename Deleter = DefaultBookDeleter< Data >, typename... Arguments >
	class Library
	{
		public:
			/// <summary>
			/// Custom factory build function.
			/// The factory takes a set of known arguments
			/// and must return a newly allocated memory pointer.
			/// 
			/// A special case is setting the args to another
			/// functor so that we can have some real flexibility
			/// in our objects construction.
			///
			/// typedef Data* (*Factory)( Arguments... );
			/// </summary>
			typedef std::function< Data* ( Arguments... ) > Factory;

			/// <summary> This will hold the function to reference count each book. </summary>
			typedef std::function< void ( Data* ) > BookLibrarian;

			/// <summary>
			/// This will do the magic!
			/// The shared pointer get a custom deleter
			/// which decrements our ref count to release
			/// our data when nothing is using the data.
			/// The ref count is incremented each time
			/// we check out some a book.
			/// </summary>
			typedef std::shared_ptr< Data > DataSPtr;

		public:
			/// <summary> ctor </summary>
			/// <param name="factory"> The data factory that can build new objects. </param>
			/// <param name="deleter"> The object that reclaims the objects memory (or releases it). </param>
			Library( Factory factory, Deleter deleter );

			/// <summary> dtor </summary>
			virtual ~Library( ) = default;

			/// <summary>
			/// We will get an object from the library with the provided key type,
			/// if the key doesn't exist then a new object will be created.
			/// The library always has to be up to date you know ;)
			/// </summary>
			/// <param name="id"> The data key, this is hashed and used to find the data. </param>
			/// <param name="args"> This is the argument list for the construction of a new data type.
			///						This is needed so that we can build a new object if it doesn't exist
			///						in the library. </param>
			/// <returns> A smart pointer to your object. </returns>
			DataSPtr checkOut( const DataID& id, Arguments... args );

		private:
			/// <summary> This structure holds our reference counter. </summary>
			struct Book
			{
				/// <summary> A pointer to allocated memory holding our data. </summary>
				Data* p_data = nullptr;

				/// <summary> The number of usages of the data so that we can track when it should be deleted. </summary>
				unsigned long long refCount = 0;

				/// <summary> 
				/// A mutex for the book so that creation and deletion are thread safe.
				/// Not ideal but it will work
				/// </summary>
				std::mutex key;
			};

		private:
			/// <summary> The creator of new objects. </summary>
			Factory factory;

			/// <summary> The destroyer of unwanted objects. </summary>
			Deleter deleter;

			/// <summary> The shelves to store the books. </summary>
			std::unordered_map< DataID, Book > bookMap;
	};
} // namespace tools

#include "Library.hpp"

#endif // _LIBRARY_HPP

