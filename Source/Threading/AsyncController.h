#pragma once
// This class provides us with a system that is updated
// sequentially on a single thread so as to try and avoid
// creating inconsistent states across different CPUs.
//
// Currently it is left up to the implementer of the sub
// classes to decide what functions execute using the
// event buffer.
//
// Project   : NaKama-Tools
// File Name : Buffer.h
// Date      : 19/10/2016
// Author    : Nicholas Welters

#ifndef _ASYNC_CONTROLLER_H
#define _ASYNC_CONTROLLER_H

#include "Buffer.h"
#include <functional>

namespace tools
{
	/// <summary>  An interface to execute updates to a class that is thread safe. </summary>
	class AsyncController
	{
		public:
			/// <summary> ctor. </summary>
			/// <param name="name"> The controller name. </param>
			explicit AsyncController( const ::std::string& name );

			/// <summary> dtor. </summary>
			virtual ~AsyncController( ) = default;

			/// <summary> Allows us to query the object for its unique (hopefully) name. </summary>
			/// <returns> The name of the object. </returns>
			::std::string getName( );

			/// <summary>
			/// Runs the updates of all the queued events
			/// and then calls the derived classes update function
			/// to finalize the objects change of state.
			/// </summary>
			void execute( );

		protected:
			/// <summary> The update events type simplified name. </summary>
			typedef ::std::function< void () > UpdateEvent;

			/// <summary> The buffer type that will hold the updates. </summary>
			typedef ::tools::Buffer< UpdateEvent > UpdateEventBuffer;

			/// <summary>
			/// The interface the derived class will use to add events from
			/// external threads so that we can execute them in a controlled manner.
			/// </summary>
			/// <param name="updateEvent"> An update event from an uncontrolled thread. </param>
			void addUpdateEvent( const UpdateEvent& updateEvent );

		private:
			/// <summary> The derived classes interface to finalize its updates to create a consistent internal state. </summary>
			virtual void update( ) = 0;

		private:
			/// <summary> The controller name. </summary>
			::std::string name;

			/// <summary> The buffer that holds the external thread updates. </summary>
			UpdateEventBuffer updates;

	};
} // namespace tools

#endif // _ASYNC_CONTROLLER_H
