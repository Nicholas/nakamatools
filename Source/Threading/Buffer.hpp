#pragma once
// Template buffer.
// A thread safe queue so that we can send tasks from one thread to another.
//
// Project   : NaKama-Tools
// File Name : Buffer.hpp
// Date      : 20/06/2015
// Author    : Nicholas Welters

#include "Buffer.h"

#ifndef _BUFFER_HPP
#define _BUFFER_HPP

namespace tools
{
	/// <summary> ctor. </summary>
	template< class TYPE >
	Buffer< TYPE >::Buffer( )
		: lock( )
		, availableElement( )
		, queue( )
	{
	}

	/// <summary> Read all the elements from the buffer so that they can all be handled in one go. </summary>
	/// <returns> All elements in the buffer. </returns>
	template< class TYPE >
	std::list< TYPE > Buffer< TYPE >::readAll( )
	{
		std::unique_lock< std::mutex > uniqueLock(lock);

		std::list< TYPE > returnList;

		while( !queue.empty() )
		{
			returnList.push_back( queue.front( ) );
			queue.pop();
		}

		return returnList;
	}

	/// <summary> Remove the top element in the buffer. </summary>
	template< class TYPE >	
	void Buffer< TYPE >::pop( )
	{
		std::unique_lock< std::mutex > uniqueLock( lock );

		queue.pop( );
	}

	/// <summary> Read the top element of the buffer. </summary>
	/// <returns> The first elements in the buffer. </returns>
	template< class TYPE >
	TYPE& Buffer< TYPE >::front( )
	{
		std::unique_lock< std::mutex > uniqueLock( lock );
			
		if( queue.size( ) == 0 )
		{
			availableElement.wait( uniqueLock );
		}

		return queue.front( );
	}

	/// <summary> Added a new element to the back of the buffer. </summary>
	/// <param name="element"> A new element to insert in to the buffer. </param>
	template< class TYPE >
	void Buffer< TYPE >::push( const TYPE& element )
	{
		std::unique_lock< std::mutex > uniqueLock( lock );

		queue.push( element );

		if( queue.size( ) == 1 )
		{
			uniqueLock.unlock( );
			availableElement.notify_one( );
		}
	}

	/// <summary> Get the number of elements currently in the queue. </summary>
	template< class TYPE >
	size_t Buffer< TYPE >::size( )
	{
		std::unique_lock< std::mutex > uniqueLock( lock );

		return queue.size( );
	}
} // namespace tools

#endif // _BUFFER_HPP
