
// This class provides us with a system that is updated
// sequentially on a single thread so as to try and avoid
// creating inconsistent states across different CPUs.
//
// Currently it is left up to the implementer of the sub
// classes to decide what functions execute using the
// event buffer.
//
// Project   : NaKama-Tools
// File Name : Buffer.h
// Date      : 19/10/2016
// Author    : Nicholas Welters

#include "AsyncController.h"

namespace tools
{
	using ::std::string;
	using ::std::list;

	/// <summary> ctor. </summary>
	/// <param name="name"> The controller name. </param>
	AsyncController::AsyncController( const string& name )
		: name( name )
		, updates( )
	{ }

	/// <summary> Allows us to query the object for its unique (hopefully) name. </summary>
	/// <returns> The name of the object. </returns>
	string AsyncController::getName( )
	{
		return name;
	}

	/// <summary>
	/// Runs the updates of all the queued events
	/// and then calls the derived classes update function
	/// to finalize the objects change of state.
	/// </summary>
	void AsyncController::execute( )
	{
		list< UpdateEvent > updateEvents = updates.readAll( );

		for( const UpdateEvent& updateEvent : updateEvents )
		{
			updateEvent( );
		}

		update( );
	}

	/// <summary>
	/// The interface the derived class will use to add events from
	/// external threads so that we can execute them in a controlled manner.
	/// </summary>
	/// <param name="updateEvent"> An update event from an uncontrolled thread. </param>
	void AsyncController::addUpdateEvent( const UpdateEvent & vrEvent )
	{
		updates.push( vrEvent );
	}
}
