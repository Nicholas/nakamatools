#pragma once
// Template buffer.
// A thread safe queue so that we can send tasks from one thread to another.
//
// Project   : NaKama-Tools
// File Name : Buffer.h
// Date      : 20/06/2015
// Author    : Nicholas Welters

#ifndef _BUFFER_H
#define _BUFFER_H

#include <queue>
#include <mutex>
#include <condition_variable>
#include <list>

namespace tools
{
	/// <summary>
	/// Template buffer.
	/// A thread safe queue so that we can send tasks from one thread to another.
	/// </summary>
	template< class TYPE >
	class Buffer
	{
		public:
			/// <summary> ctor. </summary>
			Buffer( );

			/// <summary> dtor. </summary>
			~Buffer( ) = default;

			/// <summary> Read all the elements from the buffer so that they can all be handled in one go. </summary>
			/// <returns> All elements in the buffer. </returns>
			::std::list< TYPE > readAll( );
				
			/// <summary> Remove the top element in the buffer. </summary>
			void pop( );

			/// <summary> Read the top element of the buffer. </summary>
			/// <returns> The first elements in the buffer. </returns>
			TYPE& front( );

			/// <summary> Added a new element to the back of the buffer. </summary>
			/// <param name="element"> A new element to insert in to the buffer. </param>
			void push( const TYPE& element );

			/// <summary> Get the number of elements currently in the queue. </summary>
			size_t size( );

		private:
			/// <summary> Mutex used to protect the queue from concurrent access. </summary>
			::std::mutex lock;

			/// <summary> Condition used to get threads to wait for a available element. </summary>
			::std::condition_variable availableElement;

			/// <summary> The list used to hold the FIFO queue of processes. </summary>
			::std::queue< TYPE > queue;
	};
} // namespace tools

#include "Buffer.hpp"

#endif // _BUFFER_H
