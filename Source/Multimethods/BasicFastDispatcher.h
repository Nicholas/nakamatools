#pragma once
////////////////////////////////////////////////////////////////////////////////
// The Loki Library
// Copyright (c) 2001 by Andrei Alexandrescu
// This code accompanies the book:
// Alexandrescu, Andrei. "Modern C++ Design: Generic Programming and Design
//     Patterns Applied". Copyright (c) 2001. Addison-Wesley.
// Permission to use, copy, modify, distribute and sell this software for any
//     purpose is hereby granted without fee, provided that the above copyright
//     notice appear in all copies and that both that copyright notice and this
//     permission notice appear in supporting documentation.
// The author or Addison-Wesley Longman make no representations about the
//     suitability of this software for any purpose. It is provided "as is"
//     without express or implied warranty.
////////////////////////////////////////////////////////////////////////////////
//
// An O(1) double dispatcher.
//
// This class provides speed for dispatching on two types.
// The problem with this is that we need to inject an
// index in to the class. This prevents the class for working
// in multiple dispatchers.
//
//
// Project   : NaKama-Tools
// File Name : BasicFastDispatcher.h
// Date      : 30/01/2013
// Author    : Nicholas Welters

#ifndef _BASIC_FAST_DISPATCHER_H
#define _BASIC_FAST_DISPATCHER_H

#include <vector>

/// <summary> Added to the public section of each class that we want to support multiple dispatch. </summary>
#define IMPLEMENT_INDEXABLE_CLASS( SomeClass )\
	static int& getClassDispatchIndexStatic( )\
	{\
		static int index = -1;\
		return index;\
	}\
	virtual int& getClassDispatchIndex( )\
	{\
		if( typeid( *this ) != typeid( SomeClass ) )\
		{\
			throw ::std::runtime_error( "IMPLEMENT_INDEXABLE_CLASS :: Type miss match" );\
		}\
		return getClassDispatchIndexStatic( );\
	}
	// cout << typeid(*this).name() << " != " << typeid(SomeClass).name() << "\n";

namespace tools
{
	/// <summary> Constant-Time Double Dispatch Class. </summary>
	/// <template name="BaseLHS"> The base type for the left hand side argument. </template>
	/// <template name="BaseRHS"> The base type for the right hand side argument. </template>
	/// <template name="ResultType"> The return type of the dispatch operation. </template>
	/// <template name="CallbackType"> The dispatch callback operation type. </template>
	template
	<
		class BaseLHS,
		class BaseRHS,
		typename ResultType   = void,
		typename CallbackType = ResultType(*)(BaseLHS&,BaseRHS&)
	>
	class BasicFastDispatch
	{
		public:
			/// <summary> ctor </summary>
			BasicFastDispatch( );

			/// <summary> Register a callback with the dispatcher. </summary>
			/// <template name="SomeLHS"> The left hand side arguments concrete argument type. </template>
			/// <template name="SomeRHS"> The right hand side arguments concrete argument type. </template>
			/// <param name="p_function"> The function callback. </param>
			template<class SomeLHS, class SomeRHS>
			void add( CallbackType p_function );

			/// <summary> Matches the two types with the correct callback and then runs the operation. </summary>
			/// <param name="lhs"> The left hand side argument. </param>
			/// <param name="rhs"> The right hand side argument. </param>
			ResultType go( BaseLHS& lhs, BaseRHS& rhs );

		private:
			/// <summary> 
			/// Pack the call back with a boolean so that we can determine if it has been set or not.
			/// This removes the need for the user define a comparison operator
			/// to be able to check the callback with a null value.
			/// </summary>
			class Callback
			{
				public:
					/// <summary> ctor </summary>
					Callback( );

					/// <summary> Check the callback has been set. </summary>
					bool isSet( );

					/// <summary> Store a callback. </summary>
					/// <param name="p_function"> The function callback. </param>
					void set( CallbackType p_function );

					/// <summary> Get the stores callback. </summary>
					/// <returns> The callback. </returns>
					CallbackType& get( );

				private:
					/// <summary> The set state of the callback. </summary>
					bool callbackSet;

					/// <summary> The dispatchers callback. </summary>
					CallbackType callback;
			};

			/// <summary> A callback vector type. </summary>
			typedef ::std::vector< Callback > Row;

			/// <summary> A callback 2D matrix type. </summary>
			typedef ::std::vector< Row > Matrix;


			/// <summary> A matrix holding the callbacks for dispatching. </summary>
			Matrix callbacks;

			/// <summary>
			/// Tallies the number of columns added so far.
			/// This is redundant as we can get the value form the row size.
			/// </summary>
			int columns;
	};
} // namespace tools

#include "BasicFastDispatcher.hpp"

#endif // _BASIC_FAST_DISPATCHER_H
