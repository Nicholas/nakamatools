#pragma once
////////////////////////////////////////////////////////////////////////////////
// The Loki Library
// Copyright (c) 2001 by Andrei Alexandrescu
// This code accompanies the book:
// Alexandrescu, Andrei. "Modern C++ Design: Generic Programming and Design
//     Patterns Applied". Copyright (c) 2001. Addison-Wesley.
// Permission to use, copy, modify, distribute and sell this software for any
//     purpose is hereby granted without fee, provided that the above copyright
//     notice appear in all copies and that both that copyright notice and this
//     permission notice appear in supporting documentation.
// The author or Addison-Wesley Longman make no representations about the
//     suitability of this software for any purpose. It is provided "as is"
//     without express or implied warranty.
////////////////////////////////////////////////////////////////////////////////
//
// A Functor Double Dispatch Class.
//
// Provides a way to register functors with the base
// dispatcher. This lets us use functors that need 
// value semantics but they don't work with the
// run time polymorphism we are using for the
// dispatcher.
//
// Project   : NaKama-Tools
// File Name : FunctorDispatcher.h
// Date      : 30/01/2013
// Author    : Nicholas Welters

#ifndef _FUNCTOR_DISPATCHER_H
#define _FUNCTOR_DISPATCHER_H

#include "Functors/Functor.h"
#include "Functors/FunctorImpl.h"
#include "BasicFastDispatcher.h"

namespace tools
{
	/// <summary> Constant-Time Double Dispatch to Functor. </summary>
	/// <template name="BaseLHS"> The base type for the left hand side argument. </template>
	/// <template name="BaseRHS"> The base type for the right hand side argument. </template>
	/// <template name="ResultType"> The return type of the dispatch operation. </template>
	template
	<
		class BaseLHS,
		class BaseRHS = BaseLHS,
		typename ResultType = void
	>
	class FunctorDispatch
	{
		public:
			/// <summary> Register a functor callback with the dispatcher. </summary>
			/// <template name="SomeLHS"> The left hand side arguments concrete argument type. </template>
			/// <template name="SomeRHS"> The right hand side arguments concrete argument type. </template>
			/// <template name="Fun"> The callback functor type. </template>
			/// <param name="fun"> The functor callback. </param>
			template< class SomeLHS, class SomeRHS, class Fun >
			void add( const Fun& fun );

			/// <summary>
			/// Matches the two types with the correct callback and then runs the operation.
			/// Forwards the request to the back end dispatcher.
			/// </summary>
			/// <param name="lhs"> The left hand side argument. </param>
			/// <param name="rhs"> The right hand side argument. </param>
			ResultType go( BaseLHS& lhs, BaseRHS& rhs );

		private:
			typedef Functor< ResultType, BaseLHS&, BaseRHS& > FunctorType;
			typedef BasicFastDispatch< BaseLHS, BaseRHS, ResultType, FunctorType > BackEndType;

			BackEndType backEnd;
	};
} // namespace tools

#include "FunctorDispatch.hpp"

#endif // _FUNCTOR_DISPATCHER_H
