#pragma once
////////////////////////////////////////////////////////////////////////////////
// The Loki Library
// Copyright (c) 2001 by Andrei Alexandrescu
// This code accompanies the book:
// Alexandrescu, Andrei. "Modern C++ Design: Generic Programming and Design
//     Patterns Applied". Copyright (c) 2001. Addison-Wesley.
// Permission to use, copy, modify, distribute and sell this software for any
//     purpose is hereby granted without fee, provided that the above copyright
//     notice appear in all copies and that both that copyright notice and this
//     permission notice appear in supporting documentation.
// The author or Addison-Wesley Longman make no representations about the
//     suitability of this software for any purpose. It is provided "as is"
//     without express or implied warranty.
////////////////////////////////////////////////////////////////////////////////
//
// An O(1) double dispatcher.
//
// This class provides speed for dispatching on two types.
// The problem with this is that we need to inject an
// index in to the class. This prevents the class for working
// in multiple dispatchers.
//
//
// Project   : NaKama-Tools
// File Name : Library.h
// Date      : 30/01/2013
// Author    : Nicholas Welters

#include "BasicFastDispatcher.h"

#ifndef _BASIC_FAST_DISPATCHER_HPP
#define _BASIC_FAST_DISPATCHER_HPP

#include <string>

namespace tools
{
	using ::std::invalid_argument;
	using ::std::to_string;

	/// <summary> ctor </summary>
	template< class BaseLHS, class BaseRHS, typename ResultType, typename CallbackType >
	BasicFastDispatch< BaseLHS, BaseRHS, ResultType, CallbackType >::BasicFastDispatch( )
		: callbacks( )
		, columns( 0 )
	{ }

	template< class BaseLHS, class BaseRHS, typename ResultType, typename CallbackType >
	template<class SomeLHS, class SomeRHS>
	void BasicFastDispatch< BaseLHS, BaseRHS, ResultType, CallbackType >::add( CallbackType p_function )
	{
		int& indexLHS = SomeLHS::getClassDispatchIndexStatic();

		if( indexLHS < 0 )
		{
			callbacks.push_back( Row( ) );
			indexLHS = static_cast< int >( callbacks.size( ) ) - 1;
		}
		else if( static_cast< int >( callbacks.size( ) ) <= indexLHS )
		{
			callbacks.resize( indexLHS + 1 );
		}

		Row& thisRow = callbacks[ indexLHS ];
		int& indexRHS = SomeRHS::getClassDispatchIndexStatic( );

		if( indexRHS < 0 )
		{
			thisRow.resize( ++columns );
			indexRHS = static_cast< int >( thisRow.size( ) ) - 1;
		}
		else if( static_cast< int >( thisRow.size( ) ) <= indexRHS )
		{
			thisRow.resize( indexRHS + 1 );
		}

		thisRow[ indexRHS ].set( p_function );
	}

	template< class BaseLHS, class BaseRHS, typename ResultType, typename CallbackType >
	ResultType BasicFastDispatch< BaseLHS, BaseRHS, ResultType, CallbackType >::go( BaseLHS& lhs, BaseRHS& rhs )
	{
		int & indexLHS = lhs.getClassDispatchIndex();
		int & indexRHS = rhs.getClassDispatchIndex();

		if( indexLHS < 0 || indexLHS >= ( int ) callbacks.size( ) )
		{
			throw invalid_argument( "BasicFastDispatch::go LHS index out of bounds " + to_string( indexLHS ) + "." );
		}
		else if( indexRHS < 0 || indexRHS >= (int)callbacks[ indexLHS ].size( ) )
		{
			throw invalid_argument( "BasicFastDispatch::go LHS index out of bounds " + to_string( indexRHS ) + "." );
		}
		else if( !callbacks[ indexLHS ][ indexRHS ].isSet( ) )
		{
			throw invalid_argument( "BasicFastDispatch::go callback not set." );
		}
		else
		{
			return callbacks[ indexLHS ][ indexRHS ].get( )(lhs, rhs);
		}
	}

	/// <summary> ctor </summary>
	template<class BaseLHS, class BaseRHS, typename ResultType, typename CallbackType>
	BasicFastDispatch<BaseLHS, BaseRHS, ResultType, CallbackType>::Callback::Callback( )
		: callbackSet( false )
		, callback( )
	{ }

	/// <summary> Check the callback has been set. </summary>
	template<class BaseLHS, class BaseRHS, typename ResultType, typename CallbackType>
	bool BasicFastDispatch<BaseLHS, BaseRHS, ResultType, CallbackType>::Callback::isSet( )
	{
		return callbackSet;
	}
	/// <summary> Store a callback. </summary>
	/// <param name="p_function"> The function callback. </param>
	template<class BaseLHS, class BaseRHS, typename ResultType, typename CallbackType>
	void BasicFastDispatch<BaseLHS, BaseRHS, ResultType, CallbackType>::Callback::set( CallbackType p_function )
	{
		callback = p_function;
		callbackSet = true;
	}

	/// <summary> Get the stores callback. </summary>
	/// <returns> The callback. </returns>
	template<class BaseLHS, class BaseRHS, typename ResultType, typename CallbackType>
	CallbackType& BasicFastDispatch<BaseLHS, BaseRHS, ResultType, CallbackType>::Callback::get( )
	{
		return callback;
	}
} // namespace tools

#endif // _BASIC_FAST_DISPATCHER_H
