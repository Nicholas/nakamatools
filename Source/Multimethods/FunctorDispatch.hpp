#pragma once
////////////////////////////////////////////////////////////////////////////////
// The Loki Library
// Copyright (c) 2001 by Andrei Alexandrescu
// This code accompanies the book:
// Alexandrescu, Andrei. "Modern C++ Design: Generic Programming and Design
//     Patterns Applied". Copyright (c) 2001. Addison-Wesley.
// Permission to use, copy, modify, distribute and sell this software for any
//     purpose is hereby granted without fee, provided that the above copyright
//     notice appear in all copies and that both that copyright notice and this
//     permission notice appear in supporting documentation.
// The author or Addison-Wesley Longman make no representations about the
//     suitability of this software for any purpose. It is provided "as is"
//     without express or implied warranty.
////////////////////////////////////////////////////////////////////////////////
//
// A Functor Double Dispatch Class.
//
// Provides a way to register functors with the base
// dispatcher. This lets us use functors that need 
// value semantics but they don't work with the
// run time polymorphism we are using for the
// dispatcher.
//
// Project   : NaKama-Tools
// File Name : FunctorDispatcher.hpp
// Date      : 30/01/2013
// Author    : Nicholas Welters

#include "FunctorDispatch.h"

#ifndef _FUNCTOR_DISPATCHER_HPP
#define _FUNCTOR_DISPATCHER_HPP

namespace tools
{
	using ::std::unique_ptr;
	using ::std::make_unique;

	/// <summary> Register a functor callback with the dispatcher. </summary>
	/// <template name="SomeLHS"> The left hand side arguments concrete argument type. </template>
	/// <template name="SomeRHS"> The right hand side arguments concrete argument type. </template>
	/// <template name="Fun"> The callback functor type. </template>
	/// <param name="fun"> The functor callback. </param>
	template< class BaseLHS, class BaseRHS, typename ResultType >
	template< class SomeLHS, class SomeRHS, class Fun >
	void FunctorDispatch< BaseLHS, BaseRHS, ResultType >::add( const Fun& fun )
	{
		/// <summary> 
		/// A trampoline class that lets us wrap functors to concrete types and
		/// register the function as accepting the base types.
		///</summary>
		class Adapter : FunctorType::Implementation
		{
			public:
				explicit Adapter( const Fun& fun )
					: fun(fun)
				{ }

				virtual ~Adapter( ) = default;

				virtual ResultType operator( )( BaseLHS& lhs, BaseRHS& rhs )
				{
					return fun( dynamic_cast< SomeLHS& >( lhs ), dynamic_cast< SomeRHS& >( rhs ) );
				}
			private:
				Fun fun;

				virtual typename FunctorType::Implementation* clone( ) const
				{
					return new Adapter( fun );
				}
		};

		backEnd.template add< SomeLHS, SomeRHS >( FunctorType( unique_ptr< typename FunctorType::Implementation >( ( typename FunctorType::Implementation* ) new Adapter( fun ) ) ) );
	}

	/// <summary>
	/// Matches the two types with the correct callback and then runs the operation.
	/// Forwards the request to the back end dispatcher.
	/// </summary>
	/// <param name="lhs"> The left hand side argument. </param>
	/// <param name="rhs"> The right hand side argument. </param>
	template< class BaseLHS, class BaseRHS, typename ResultType >
	ResultType FunctorDispatch< BaseLHS, BaseRHS, ResultType >::go( BaseLHS& lhs, BaseRHS& rhs )
	{
		return backEnd.go( lhs, rhs );
	}
} // namespace tools

#endif // _FUNCTOR_DISPATCHER_HPP
