
// A High Resolution Timer.
//
// Project   : NaKama-Tools
// File Name : Timer.h
// Date      : 08/01/2018
// Author    : Nicholas Welters

# include "Timer.h"

namespace tools
{
	using ::std::chrono::high_resolution_clock;
	using ::std::chrono::seconds;
	using ::std::chrono::system_clock;
	using ::std::chrono::time_point;
	using ::std::chrono::duration;
	using ::std::max;

	/// <summary> ctor </summary>
	Timer::Timer( )
		: baseTime( )
		, pausedTime( )
		, stopTime( )
		, previousTime( )
		, currentTime( )

		, deltaTime( 0.0 )

		, stopped( false )
	{
	}

	/// <summary> Effectively lap time between ticks is calculated. </summary>
	void Timer::tick( )
	{
		if( stopped )
		{
			deltaTime = 0.0;
		}
		else
		{
			currentTime = high_resolution_clock::now( );
			deltaTime = max( duration< float, seconds::period >( currentTime - previousTime ).count( ), 0.0f );
			previousTime = currentTime;
		}
	}

	/// <summary> Get the lap time on the timer between each tick. </summary>
	float Timer::getDelta( )
	{
		return deltaTime;
	}

	/// <summary> Resets the time to zero. </summary>
	void Timer::reset( )
	{
		baseTime = high_resolution_clock::now( );
		previousTime = baseTime;
		stopped = false;
	}

	/// <summary> Stop the timer on its current time. </summary>
	void Timer::stop( )
	{
		if( ! stopped )
		{
			stopTime = high_resolution_clock::now( );
			stopped = true;
		}
	}

	/// <summary> Start the timer. </summary>
	void Timer::start( )
	{
		if( stopped )
		{
			high_resolution_clock::time_point startTime = high_resolution_clock::now( );

			pausedTime += (startTime - stopTime);
			previousTime = startTime;
			stopped = false;
		}
	}

	/// <summary> Get the total time the timer has been running </summary>
	float Timer::getTime( )
	{
		if( stopped )
		{
			return duration< float, seconds::period >( stopTime - baseTime ).count( );
		}
		else
		{
			return duration< float, seconds::period >( ( currentTime - pausedTime ) - baseTime.time_since_epoch( ) ).count( );
		}
	}

	/// <summary> Find out if the timer is still timing. </summary>
	bool Timer::isCounting( )
	{
		return ! stopped;
	}
} // namespace tools
