# pragma once
// A tick stats counter for ticks per second like information.
//
// Project   : NaKama-Tools
// File Name : TickCounter.h
// Date      : 08/01/2018
// Author    : Nicholas Welters

#ifndef _TICK_COUNTER_H
#define _TICK_COUNTER_H

# include <vector>

# define DEFAULT_PERIOD 1.0f

# define _9_1_ 0
# define _7_3_ 1

namespace tools
{
	/// <summary>  A tick stats counter for ticks per second like information. </summary>
	class TickCounter
	{
	public:
		/// <summary> ctor - Builds a ratio array with 9:1 and 7:3 by default. </summary>
		/// <param name="timePeriod"> The time period to measure the ticks over. </param>
		TickCounter( float timePeriod = DEFAULT_PERIOD );

		/// <summary> dtor </summary>
		~TickCounter( ) = default;


		/// <summary> Tick counter. </summary>
		/// <param name="timeDelta"> The time taken to perform the tick. </param>
		void countTick( float timeDelta );

		/// <summary> Average ticks over the measured time period. </summary>
		/// <returns> The ticks per period. </returns>
		float getCountByPeriod( );

		/// <summary> The slowest tick time over the measured time period. </summary>
		/// <returns> The slowest tick over the period. </returns>
		float getSlowestByPeriod( );

		/// <summary> The fastest tick time over the measured time period. </summary>
		/// <returns> The fastest tick over the period. </returns>
		float getFastestByPeriod( );

		/// <summary> Raw frame time for each tick. </summary>
		/// <returns> The ticks per period based on the frame time. </returns>
		float getCountByDelta( );

		/// <summary> Get the tick time based on a ration from the current tick time and the last tick time. </summary>
		/// <returns> The ticks time base on the ratio. </returns>
		float getCountByRatio( int ratio );

	private:
		/// <summary> The time period to measure an average tick time. </summary>
		float timePeriod;

		/// <summary> Average tick time over the time period. </summary>
		float byPeriod;

		/// <summary> The fastest tick time over the time period. </summary>
		float minimum;

		/// <summary> The fastest tick time currently being stored for the next time period update. </summary>
		float currentMinimum;

		/// <summary> The slowest tick time over the time period. </summary>
		float maximum;

		/// <summary> The fast tick time currently being stored for the next time period update. </summary>
		float currentMaximum;

		/// <summary> Per frame count. </summary>
		float byDelta;

		/// <summary> Ratio timing information structure. </summary>
		struct Ratio
		{
			float ratioLast;
			float ratioNext;

			float tickCount;
		};

		/// <summary> Ratio timing data. </summary>
		::std::vector< Ratio > ratioCounts;

		/// <summary> The number of ticks counted. </summary>
		unsigned int tickCount;

		/// <summary> The accumulated time between time periods. </summary>
		float timeCount;
	};
} // namespace tools

#endif // _TICK_COUNTER_H
