# pragma once
// A High Resolution Timer.
//
// Project   : NaKama-Tools
// File Name : Timer.h
// Date      : 08/01/2018
// Author    : Nicholas Welters

# include <chrono>

namespace tools
{
	/// <summary> A High Resolution Timer. </summary>
	class Timer
	{
		public:
			/// <summary> ctor </summary>
			Timer( );
		
			/// <summary> dtor </summary>
			~Timer( ) = default;

			/// <summary> Effectively lap time between ticks is calculated. </summary>
			void tick( );

			/// <summary> Get the lap time on the timer between each tick. </summary>
			float getDelta( );

			/// <summary> Resets the time to zero. </summary>
			void reset( );

			/// <summary> Stop the timer on its current time. </summary>
			void stop( );

			/// <summary> Start the timer. </summary>
			void start( );
		
			/// <summary> Get the total time the timer has been running </summary>
			float getTime( );

			/// <summary> Find out if the timer is still timing. </summary>
			bool isCounting( );

		private:
			/// <summary> The starting time. </summary>
			::std::chrono::high_resolution_clock::time_point baseTime;

			/// <summary> The amount of time that the timer was paused. </summary>
			::std::chrono::high_resolution_clock::time_point pausedTime;

			/// <summary> The time when the timer was paused. </summary>
			::std::chrono::high_resolution_clock::time_point stopTime;

			/// <summary> The last tick time. </summary>
			::std::chrono::high_resolution_clock::time_point previousTime;

			/// <summary> The tick time. </summary>
			::std::chrono::high_resolution_clock::time_point currentTime;

			/// <summary> The tick time in seconds. </summary>
			float deltaTime;

			/// <summary> Pauses the timer. </summary>
			bool stopped;
	};
} // namespace tools
