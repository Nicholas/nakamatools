
// A tick stats counter for ticks per second like information.
//
// Project   : NaKama-Tools
// File Name : TickCounter.cpp
// Date      : 08/01/2018
// Author    : Nicholas Welters

#include "TickCounter.h"

namespace tools
{
	using ::std::numeric_limits;
	using ::std::min;
	using ::std::max;

	/// <summary> ctor - Builds a ratio array with 9:1 and 7:3 by default. </summary>
	/// <param name="timePeriod"> The time period to measure the ticks over. </param>
	TickCounter::TickCounter( float _timePeriod )
		: timePeriod( _timePeriod )
		, byPeriod( 0.0f )
		, minimum( numeric_limits< float >::max( ) )
		, currentMinimum( numeric_limits< float >::max( ) )
		, maximum( numeric_limits< float >::min( ) )
		, currentMaximum( numeric_limits< float >::min( ) )
		, byDelta( 0.0f )
		, ratioCounts( 2 )
		, tickCount( 0 )
		, timeCount( 0 )
	{
		ratioCounts[ _9_1_ ].ratioLast = 0.9f;
		ratioCounts[ _9_1_ ].ratioNext = 0.1f;
		ratioCounts[ _9_1_ ].tickCount = 0.0f;

		ratioCounts[ _7_3_ ].ratioLast = 0.7f;
		ratioCounts[ _7_3_ ].ratioNext = 0.3f;
		ratioCounts[ _7_3_ ].tickCount = 0.0f;
	}

	/// <summary> Tick counter. </summary>
	/// <param name="timeDelta"> The time taken to perform the tick. </param>
	void TickCounter::countTick( float timeDelta )
	{
		timeCount += timeDelta;
		++tickCount;

		byDelta = timeDelta == 0 ? -1 : timePeriod / timeDelta;

		currentMinimum = min( currentMinimum, timeDelta );
		currentMaximum = max( currentMaximum, timeDelta );


		if( timeCount >= timePeriod )
		{
			byPeriod = ( float ) tickCount;

			minimum = currentMinimum;
			maximum = currentMaximum;

			currentMinimum = numeric_limits< float >::max( );
			currentMaximum = numeric_limits< float >::min( );

			timeCount -= timePeriod;
			tickCount = 0;

			/**
			::std::cout
				<< ">>>: " << timeDelta
				<< "\nAvg: " << byPeriod << " [ " << getSlowestByPeriod( ) << ", " << getFastestByPeriod( ) << "]"
				<< "\nDlt: " << byDelta
				<< "\n9_1: " << getCountByRatio( _9_1_ )
				<< "\n7_3: " << getCountByRatio( _7_3_ )
				<< "\n";
			//*/
		}

		for( size_t i = 0; i < ratioCounts.size( ); ++i )
		{
			ratioCounts[ i ].tickCount  = ratioCounts[ i ].ratioLast * ratioCounts[ i ].tickCount;
			ratioCounts[ i ].tickCount += ratioCounts[ i ].ratioNext * timeDelta;
		}
	}

	/// <summary> Average ticks over the measured time period. </summary>
	/// <returns> The ticks per period. </returns>
	float TickCounter::getCountByPeriod( )
	{
		return byPeriod;
	}

	/// <summary> The slowest tick time over the measured time period. </summary>
	/// <returns> The slowest tick over the period. </returns>
	float TickCounter::getSlowestByPeriod( )
	{
		return maximum <= 0 ? -1 : timePeriod / maximum;
	}

	/// <summary> The fastest tick time over the measured time period. </summary>
	/// <returns> The fastest tick over the period. </returns>
	float TickCounter::getFastestByPeriod( )
	{
		return minimum <= 0 ? -1 : timePeriod / minimum;
	}

	/// <summary> Raw frame time for each tick. </summary>
	/// <returns> The ticks per period based on the frame time. </returns>
	float TickCounter::getCountByDelta( )
	{
		return byDelta;
	}

	/// <summary> Get the tick time based on a ration from the current tick time and the last tick time. </summary>
	/// <returns> The ticks time base on the ratio. </returns>
	float TickCounter::getCountByRatio( int ratio )
	{
		return ratioCounts[ ratio ].tickCount == 0 ? -1 : timePeriod / ratioCounts[ ratio ].tickCount;
	}
} // namespace tools
