
// A generic process type that takes a functor to be executed in the process engine.
//
// Project   : NaKama-Tools
// File Name : Process.h
// Date      : 11/05/2014
// Author    : Nicholas Welters

#include "FunctorProcess.h"

namespace tools
{
	using std::function;

	/// <summary> ctor. </summary>
	/// <param name="functor"> The functor to be executed in the engine. </param>
	FunctorProcess::FunctorProcess( const function< void ( ) >& functor )
		: Process( )
		, functor( functor )
	{ }

	/// <summary> 
	/// The entry point to a tasks execution.
	/// This forwards the request to the functor.
	/// </summary>
	void FunctorProcess::execute( )
	{
		functor( );
	}
} // namespace tools
