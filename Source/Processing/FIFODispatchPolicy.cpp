
// The policy used to determine the next process to
// select and begin executing.
//
// This uses a stl queue to provide a FIFO order to
// the process execution.
//
// Project   : NaKama-Tools
// File Name : FIFODispatchPolicy.cpp
// Date      : 17/05/2014
// Author    : Nicholas Welters

#include "FIFODispatchPolicy.h"
#include "Process.h"

namespace tools
{
	using ::std::mutex;
	using ::std::unique_lock;

	/// <summary> ctor </summary>
	/// <param name="threads"> The number of threads in the tread pool. </param>
	FIFODispatchPolicy::FIFODispatchPolicy( const size_t & threads )
		: DispatchPolicy( threads )
		, lock( )
		, availableProcess( )
		, queue( )
	{ }

	/// <summary> Gets the next process in the process pool to be executed. </summary>
	/// <param name="threadID"> The thread to use to execute the process. </param>
	/// <returns> A smart pointer to a scheduled process. </returns>
	ProcessSPtr FIFODispatchPolicy::next( const size_t & threadID )
	{
		unique_lock< mutex > uniqueLock( lock );

		if( queue.size( ) == 0 && isRunning( ) )
		{
			availableProcess.wait( uniqueLock );
		}

		if( isRunning( ) )
		{
			ProcessSPtr pProcess = queue.front( );
			queue.pop( );

			return pProcess;
		}
		else
		{
			return DispatchPolicy::next( threadID );
		}
	}

	/// <summary> Scheduled a process to be executed by the thread pool. </summary>
	/// <param name="threadID"> A smart pointer to the process that will be scheduled for execution. </param>
	void FIFODispatchPolicy::add( const ProcessSPtr& pProcess )
	{
		unique_lock< mutex > uniqueLock( lock );

		if( isShuttingDown( ) )
		{
			return;
		}

		queue.push( pProcess );

		if( queue.size( ) == 1 )
		{
			uniqueLock.unlock( );
			availableProcess.notify_one( );
		}
	}

	/// <summary>
	/// Notify any threads that might be waiting for
	///	for a process to execute.
	/// </summary>
	void FIFODispatchPolicy::notifyThreads( )
	{
		availableProcess.notify_all( );
	}
} // namespace tools
