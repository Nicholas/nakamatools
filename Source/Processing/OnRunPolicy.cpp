
// Policy of the ProcessEngine of what should happen when the engine is started.
// 	-Join with the worker threads and exit when the engine shuts down.
// 	-Allow the worker threads to start performing there work.This allows the owner thread to continue with its own work.
//
// Project   : NaKama-Tools
// File Name : OnRunPolicy.cpp
// Date      : 11/05/2014
// Author    : Nicholas Welters

#include "OnRunPolicy.h"

namespace tools
{
	/// <summary> The caller doesn't join the thread pool. </summary>
	void UseWorkersPolicy::joinWorkerPolicy( )
	{ }

	/// <summary>
	/// Set the first element ID so that we can skip the owner if needed so that
	/// anything that will try and use thread 0 wont send processes to a list that never
	/// gets executed.
	/// </summary>
	/// <returns> The starting index, 0, for the worker threads. </returns>
	size_t UseWorkersPolicy::startingIndex( )
	{
		return 0;
	}

	/// <summary> The caller joins the thread pool and does some work. </summary>
	void JoinWorkersPolicy::joinWorkerPolicy( )
	{
		threadMain( 0 );
	}

	/// <summary>
	/// Set the first element ID so that we can skip the owner if needed so that
	/// anything that will try and use thread 0 wont send processes to a list that never
	/// gets executed.
	/// </summary>
	/// <returns> The starting index, 1, for the worker threads. </returns>
	size_t JoinWorkersPolicy::startingIndex( )
	{
		return 1;
	}
} // namespace tools
