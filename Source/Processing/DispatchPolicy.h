#pragma once
// The policy used to determine the next process to
// select and begin executing.
//
// Project   : NaKama-Tools
// File Name : DispatchPolicy.h
// Date      : 11/05/2014
// Author    : Nicholas Welters

#ifndef _DISPATCH_POLICY_H
#define _DISPATCH_POLICY_H

#include "Macros.h"

#include <memory>
#include <atomic>

namespace tools
{
	FORWARD_DECLARE( Process );

	/// <summary>
	/// The policy used to determine the next process to
	/// select and begin executing.
	///</summary>
	class DispatchPolicy
	{
		public:
			/// <summary> ctor </summary>
			/// <param name="threads"> The number of threads in the tread pool. </param>
			explicit DispatchPolicy( const size_t& threads );

			/// <summary> dtor. </summary>
			virtual ~DispatchPolicy( ) = default;

			/// <summary> Gets the next process in the process pool to be executed. </summary>
			/// <param name="threadID"> The thread to use to execute the process. </param>
			/// <returns> A smart pointer to a scheduled process. </returns>
			virtual ProcessSPtr next( const size_t& threadID );

			/// <summary>
			/// Notify any threads that might be waiting for
			///	for a process to execute.
			/// </summary>
			virtual void notifyThreads( ) = 0;

			/// <summary> Check if the engine is still running. </summary>
			/// <returns> The status of the process engines run time. </returns>
			virtual ::std::atomic< bool >& isRunning( ) = 0;

		protected:
			/// <summary> Check if the engine is shutting down. </summary>
			/// <returns> The status of the process engines run time. </returns>
			virtual ::std::atomic< bool >& isShuttingDown( ) = 0;

		private:
			/// <summary>
			/// Useful little process that is used to test for the running 
			/// state after waiting on a condition.
			/// </summary>
			ProcessSPtr voidProcess;
	};
} // namespace tools

#endif // _DISPATCH_POLICY_H
