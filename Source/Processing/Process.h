#pragma once
// The process interface for all tasks that will
// be executed in the process engine.
//
// Project   : NaKama-Tools
// File Name : Process.h
// Date      : 11/05/2014
// Author    : Nicholas Welters

#ifndef _PROCESS_H
#define _PROCESS_H

#include <memory>

namespace tools
{
	/// <summary>
	/// The process interface for all tasks that will
	/// be executed in the process engine.
	/// </summary>
	class Process
	{
		public:
			/// <summary> ctor. </summary>
			Process( );

			/// <summary> dtor. </summary>
			virtual ~Process( ) = default;

			/// <summary> Get the processes importance. </summary>
			/// <returns> The process priority. </returns>
			size_t getPriority( );

			/// <summary> Get the processes importance. Higher values will be scheduled sooner. </summary>
			/// <param name="priority"> The processes importance value. </param>
			void setPriority( const size_t& priority );

			/// <summary> The entry point to a tasks execution. </summary>
			virtual void execute( ) = 0;

		private:
			/// <summary> The priority of the process. </summary>
			size_t priority;
	};

	/// <summary>
	/// Smart pointer type of the process.
	/// This is the type stored and used by the processing engine.
	/// </summary>
	typedef ::std::shared_ptr< Process > ProcessSPtr;

	/// <summary>
	/// The process comparator to make sure we can order the processes in a priority queue.
	/// </summary>
	class ProcessSPtrComparator
	{
		public:
			/// <summary>  The comparator operator used to order processes in the priority queue. </summary>
			/// <param name="lhs"> The first left hand side parameter in the comparison. </param>
			/// <param name="rhs"> The first right hand side parameter in the comparison. </param>
			bool operator( )( const ProcessSPtr& lhs, const ProcessSPtr& rhs ) const;
	};
} // namespace tools

#endif // __ENGINE_PROCESS_HPP__
