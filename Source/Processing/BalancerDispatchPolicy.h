//#pragma once
// The policy used to determine the next process to
// select and begin executing.
//
// This uses a stl priority_queue to provide the order to
// the process execution.
//
// TODO: This needs to be updated so that we can keep a list of pools rather than just two.
//
// Project   : NaKama-Tools
// File Name : BalancerDispatchPolicy.h
// Date      : 17/05/2014
// Author    : Nicholas Welters

#ifndef _BALANCER_DISPATCH_POLICY_H
#define _BALANCER_DISPATCH_POLICY_H

#include "DispatchPolicy.h"
#include "Process.h"

#include <queue>
#include <mutex>
#include <condition_variable>
#include <tuple>
#include <array>

namespace tools
{
	/// <summary>
	/// The policy used to determine the next process to
	/// select and begin executing.
	///
	/// This uses a stl priority_queue to provide the order to
	/// the process execution.
	///</summary>
	class BalancerDispatchPolicy : public DispatchPolicy
	{

		public:
			/// <summary> A unique id so that we can schedule a process in a shared job pool. </summary>
			//static const size_t SHARED_JOB_QUEUE = ::std::numeric_limits< size_t >::max( );
			static const size_t SHARED_JOB_QUEUE;

		public:
			/// <summary> ctor </summary>
			/// <param name="threads"> The number of threads in the tread pool. </param>
			explicit BalancerDispatchPolicy( const size_t& threads );

			/// <summary> dtor. </summary>
			virtual ~BalancerDispatchPolicy( ) = default;

			/// <summary> Set up the dispatchers internal states. </summary>
			/// <param name="minimumSync"> The minimum number of sync processes being executed. </param>
			/// <param name="maximumSync"> The maximum number of sync processes being executed. </param>
			/// <param name="minimumAsync"> The minimum number of async processes being executed. </param>
			/// <param name="maximumAsync"> The maximum number of async processes being executed. </param>
			void initialiseDispatcher(
				const size_t& minimumSync,
				const size_t& maximumSync,
				const size_t& minimumAsync,
				const size_t& maximumAsync
			);

			/// <summary> Gets the next process in the process pool to be executed. </summary>
			/// <param name="threadID"> The thread to use to execute the process. </param>
			/// <returns> A smart pointer to a scheduled process. </returns>
			virtual ProcessSPtr next( const size_t& threadID );

			/// <summary> Scheduled a process to be executed by the "synchronise" thread pool. </summary>
			/// <param name="pProcess"> A smart pointer to the process that will be scheduled for execution. </param>
			/// <param name="threadID"> The thread job pool to add the process too. This is default to
			///							sending the job to the shared job pool. </param>
			void addSync( const ProcessSPtr& pProcess, const size_t& threadID = SHARED_JOB_QUEUE );

			/// <summary> Scheduled a process to be executed by the "asynchronise" thread pool. </summary>
			/// <param name="pProcess"> A smart pointer to the process that will be scheduled for execution. </param>
			/// <param name="threadID"> The thread job pool to add the process too. This is default to
			///							sending the job to the shared job pool. </param>
			void addAsync( const ProcessSPtr& pProcess, const size_t& threadID = SHARED_JOB_QUEUE );

			/// <summary>
			/// Notify any threads that might be waiting for
			///	for a process to execute.
			/// </summary>
			virtual void notifyThreads( );

		private:
			/// <summary> Scheduled a process to be executed in a selected thread job pool. </summary>
			/// <param name="pProcess"> A smart pointer to the process that will be scheduled for execution. </param>
			/// <param name="index"> The queue data array index to add the process to. </param>
			/// <param name="threadID"> The thread job pool to add the process too. This is default to
			///							sending the job to the shared job pool. </param>
			void add( const ProcessSPtr& pProcess, const size_t& index, const size_t& threadID );

			/// <summary> This makes sure that initialiseDispatcher is called. </summary>
			void initialisedTest( );

		private:
			/// <summary> Queue Data array positions && thread states. </summary>
			static const size_t    SYNC;
			static const size_t   ASYNC;
			static const size_t WAITING = 2;

			/// <summary> Shared Queue Datum Tuple positions. </summary>
			static const size_t QUEUES = 0;
			static const size_t  STATE = 1;

			/// <summary> Shared Queue Datum Tuple positions. </summary>
			static const size_t MINIMUM = 0;
			static const size_t MAXIMUM = 1;
			static const size_t CURRENT = 2;
			static const size_t   QUEUE = 3;

			/// <summary> The definition of the list used to hold the priority queue of processes. </summary>
			typedef ::std::priority_queue< ProcessSPtr, ::std::vector< ProcessSPtr >, ProcessSPtrComparator > Queue;

			/// <summary>
			/// A tuple definition for the shared async and sync data
			/// 
			/// size_t - Minimum number of threads.
			/// size_t - Maximum number of threads.
			/// size_t - Current number being executed.
			/// Queue  - The process queue to select from.
			/// </summary>
			typedef ::std::tuple< size_t, size_t, size_t, Queue > SharedQueueData;

			/// <summary>
			/// A tuple definition for the per thread async and sync data
			///
			/// Queue  - The threads sync process queue.
			/// Queue  - The threads async process queue.
			/// size_t - The current work state of the thread.
			/// </summary>
			typedef ::std::tuple< ::std::array< Queue, 2 >, size_t > ThreadQueueData;

			/// <summary> Mutex used to protect the queue from concurrent access </summary>
			::std::mutex lock;

			/// <summary> Condition used to get threads to wait for a available process. </summary>
			::std::condition_variable availableProcess;

			/// <summary> The array holding the Async and Sync queue data. </summary>
			::std::array< SharedQueueData, 2 > sharedQueueData;

			/// <summary>
			/// The array holds both the sync and async buffer for each thread
			/// for per thread scheduling and task tracking.
			/// </summary>
			::std::vector< ThreadQueueData > threadQueueData;

			/// <summary> The first queue to attempt to get a process from, ping pong buffering. </summary>
			size_t primary;

			/// <summary> The second queue to attempt to get a process from, ping pong buffering. </summary>
			size_t secondary;

			/// <summary> Boolean used to make sure that the user has initialised the dispatcher. </summary>
			bool initialised;
	};
} // namespace tools

#endif // _BALANCER_DISPATCH_POLICY_H
