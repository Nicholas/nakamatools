#pragma once
// The process engine.
// This will maintain a thread pool and dispatch
// processes to be executed by the threads.
//
// Project   : NaKama-Tools
// File Name : ProcessExecutor.hpp
// Date      : 11/05/2014
// Author    : Nicholas Welters

#include "ProcessExecutor.h"

#ifndef _PROCESS_EXECUTOR_HPP
#define _PROCESS_EXECUTOR_HPP

namespace tools
{
	using ::std::atomic;
	using ::std::lock_guard;
	using ::std::recursive_mutex;
	using ::std::unique_lock;
	using ::std::runtime_error;
	using ::std::out_of_range;
	using ::std::exception;
	using ::std::string;
	using ::std::to_string;

	/// <summary> ctor </summary>
	/// <param name="threads"> The number of worker threads in the tread pool. </param>
	template< class OnRunPolicyType, class DispatchPolicyType >
	ProcessExecutor< OnRunPolicyType, DispatchPolicyType >::ProcessExecutor( const size_t& workerThreads )
		: OnRunPolicyType( )
		, DispatchPolicyType( workerThreads + OnRunPolicyType::startingIndex( ) )
		, workerThreads( workerThreads )
		, running( false )
		, shuttingDown( false )
		, threadPool( )
		, threadExceptionMessage( "" )
		, threadExceptionLock( )
	{ }

	/// <summary> dtor. </summary>
	template< class OnRunPolicyType, class DispatchPolicyType >
	ProcessExecutor< OnRunPolicyType, DispatchPolicyType >::~ProcessExecutor( )
	{
		shutdown( );
		join( false );

		// HACK: Need a better way to do this
		if( threadExceptionMessage != "" )
		{
			printf( "-------------------------\n%s%s-------------------------\n", "Unchecked Exception\n", threadExceptionMessage.c_str( ) );
		}
	}

	/// <summary> Signals all the treads begin executing. </summary>
	template< class OnRunPolicyType, class DispatchPolicyType >
	void ProcessExecutor< OnRunPolicyType, DispatchPolicyType >::run( )
	{
		running = true;

		struct ThreadTrampoline
		{
			ProcessExecutor< OnRunPolicyType, DispatchPolicyType > * engine;
			size_t threadID;

			ThreadTrampoline( ProcessExecutor< OnRunPolicyType, DispatchPolicyType > * engine, const size_t & threadID )
				: engine( engine )
				, threadID( threadID )
			{ }

			void operator( )( )
			{
				engine->threadMain( threadID );
			}
		};

		size_t threadID = OnRunPolicyType::startingIndex( );
		//size_t threadID = 1;

		for( unsigned int i = 0; i < workerThreads; ++i, ++threadID )
		{
			threadPool.emplace_back( ThreadTrampoline( this, threadID ) );
		}

		OnRunPolicyType::joinWorkerPolicy( );
	}

	/// <summary> Signals all the treads to exit after completing there current task. </summary>
	template< class OnRunPolicyType, class DispatchPolicyType >
	void ProcessExecutor< OnRunPolicyType, DispatchPolicyType >::shutdown( )
	{
		running = false;
		shuttingDown = true;

		DispatchPolicyType::notifyThreads( );
	}

	/// <summary>
	/// Wait for the worker threads to shutdown and joins with them.
	/// This should be called before destroying the engine.
	/// </summary>
	/// <param name="throwException"> Throws an exception if any of the worker threads had an exception.
	///								  Disabled when called from the destructor. </param>
	template< class OnRunPolicyType, class DispatchPolicyType >
	void ProcessExecutor< OnRunPolicyType, DispatchPolicyType >::join( bool throwException )
	{
		for( unsigned int i = 0; i < threadPool.size( ); ++i )
		{
			threadPool[ i ].join( );
		}

		threadPool.clear( );

		if( throwException && threadExceptionMessage != "" )
		{
			throwWorkerException( );
		}
	}

	/// <summary> Check if the engine is still running. </summary>
	/// <returns> The status of the process engines run time. </returns>
	template< class OnRunPolicyType, class DispatchPolicyType >
	atomic< bool >& ProcessExecutor< OnRunPolicyType, DispatchPolicyType >::isRunning( )
	{
		return running;
	}


	/// <summary> Check if the engine is shutting down. </summary>
	/// <returns> The status of the process engines run time. </returns>
	template< class OnRunPolicyType, class DispatchPolicyType >
	atomic< bool >& ProcessExecutor< OnRunPolicyType, DispatchPolicyType >::isShuttingDown( )
	{
		return shuttingDown;
	}


	/// <summary> Worker thread entry point, and run time loop. </summary>
	template< class OnRunPolicyType, class DispatchPolicyType >
	void ProcessExecutor< OnRunPolicyType, DispatchPolicyType >::threadMain( const size_t & threadID )
	{
		try
		{
			while( running )
			{
				DispatchPolicyType::next( threadID )->execute( );
			}
		}

		catch( const runtime_error& e )
		{
			lock_guard< recursive_mutex > _( threadExceptionLock );

			threadExceptionMessage += string( "Thread " );
			threadExceptionMessage += to_string( threadID );
			threadExceptionMessage += string( ": " );
			threadExceptionMessage += string( e.what( ) );

			shutdown( );
		}
		catch( const out_of_range& e )
		{
			lock_guard< recursive_mutex > _( threadExceptionLock );

			threadExceptionMessage += string( "Thread " );
			threadExceptionMessage += to_string( threadID );
			threadExceptionMessage += string( ": " );
			threadExceptionMessage += string( e.what( ) );

			shutdown( );
		}
		catch( const exception& e )
		{
			lock_guard< recursive_mutex > _( threadExceptionLock );

			threadExceptionMessage += string( "Thread " );
			threadExceptionMessage += to_string( threadID );
			threadExceptionMessage += string( ": " );
			threadExceptionMessage += string( e.what( ) );

			shutdown( );
		}
		catch( ... )
		{
			lock_guard< recursive_mutex > _( threadExceptionLock );

			threadExceptionMessage += string( "Thread " );
			threadExceptionMessage += to_string( threadID );
			threadExceptionMessage += string( "Unknown Exception" );

			shutdown( );
		}

		// We don't want a worker thread from running this,
		// as a worker will call join on its self, or on a thread that
		// has been joined with the main thread.
		if( threadID == 0 && OnRunPolicyType::startingIndex( ) != 0 )
		{
			unique_lock< recursive_mutex > _( threadExceptionLock );

			if( threadExceptionMessage != "" )
			{
				_.unlock( );
				join( );
			}
		}
	}

	/// <summary> 
	/// If a worker has an exception then we need to forward that
	/// exception to our owner so that we are aware of it.
	/// </summary>
	template< class OnRunPolicyType, class DispatchPolicyType >
	void ProcessExecutor< OnRunPolicyType, DispatchPolicyType >::throwWorkerException( )
	{
		string message = threadExceptionMessage;
		threadExceptionMessage = "";

		throw runtime_error( message );
	}
} // namespace tools

#endif // _PROCESS_EXECUTOR_HPP
