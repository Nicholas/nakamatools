
// The process interface for all tasks that will
// be executed in the process engine.
//
// Project   : NaKama-Tools
// File Name : Process.cpp
// Date      : 11/05/2014
// Author    : Nicholas Welters

#include "Process.h"

namespace tools
{
	/// <summary> ctor. </summary>
	Process::Process( )
		: priority( 0 )
	{ }

	/// <summary> Get the processes importance. </summary>
	/// <returns> The process priority. </returns>
	size_t Process::getPriority( )
	{
		return priority;
	}

	/// <summary> Get the processes importance. Higher values will be scheduled sooner. </summary>
	/// <param name="priority"> The processes importance value. </param>
	void Process::setPriority( const size_t & priorityParam )
	{
		priority = priorityParam;
	}

	/// <summary>  The comparator operator used to order processes in the priority queue. </summary>
	/// <param name="lhs"> The first left hand side parameter in the comparison. </param>
	/// <param name="rhs"> The first right hand side parameter in the comparison. </param>
	bool ProcessSPtrComparator::operator( )( const ProcessSPtr& lhs, const ProcessSPtr& rhs ) const
	{
		return lhs->getPriority( ) < rhs->getPriority( );
	}
} // namespace tools
