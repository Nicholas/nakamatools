#pragma once
// The process engine.
// This will maintain a thread pool and dispatch
// processes to be executed by the threads.
//
// Project   : NaKama-Tools
// File Name : ProcessExecutor.h
// Date      : 11/05/2014
// Author    : Nicholas Welters

#ifndef _PROCESS_EXECUTOR_H
#define _PROCESS_EXECUTOR_H

#include "OnRunPolicy.h"
#include "FIFODispatchPolicy.h"

#include <vector>
#include <thread>
#include <atomic>
#include <mutex>
#include <string>

namespace tools
{
	/// <summary>
	/// The process engine.
	/// This will maintain a thread pool and dispatch
	/// processes to be executed by the threads.
	///</summary>
	/// <template name="OnRunPolicyType"> Defines what will happen to the owner thread when we run this executor.
	///									  The default is to use the pool and exit the run member function to continue other work. </template>
	/// <template name="DispatchPolicyType"> Defines how we will dispatch processes to the threads in the working pool.
	///										 The default will dispatch processes in first in first out order. </template>
	template
	<
		class OnRunPolicyType    = UseWorkersPolicy,
		class DispatchPolicyType = FIFODispatchPolicy
	>
	class ProcessExecutor
		: public OnRunPolicyType
		, public DispatchPolicyType
	{
		public:
			/// <summary> ctor </summary>
			/// <param name="threads"> The number of worker threads in the tread pool. </param>
			ProcessExecutor( const size_t& workerThreads );

			/// <summary> dtor. </summary>
			virtual ~ProcessExecutor( );

			/// <summary> Signals all the treads begin executing. </summary>
			void run( );

			/// <summary> Signals all the treads to exit after completing there current task. </summary>
			void shutdown( );

			/// <summary>
			/// Wait for the worker threads to shutdown and joins with them.
			/// This should be called before destroying the engine.
			/// </summary>
			/// <param name="throwException"> Throws an exception if any of the worker threads had an exception.
			///								  Disabled when called from the destructor. </param>
			void join( bool throwException = true );

			/// <summary> Check if the engine is still running. </summary>
			/// <returns> The status of the process engines run time. </returns>
			virtual ::std::atomic< bool >& isRunning( ) final override;

		protected:
			/// <summary> Check if the engine is shutting down. </summary>
			/// <returns> The status of the process engines run time. </returns>
			virtual ::std::atomic< bool >& isShuttingDown( ) final override;

			/// <summary> Worker thread entry point, and run time loop. </summary>
			virtual void threadMain( const size_t& threadID );

		private:
			/// <summary> 
			/// If a worker has an exception then we need to forward that
			/// exception to our owner so that we are aware of it.
			/// </summary>
			void throwWorkerException( );

			/// <summary> The flag used to control the thread life time. </summary>
			::std::atomic< bool > running;

			/// <summary>
			/// This is going to stop processes being added to the engine
			/// after its been shutdown. THe use of shared pointers ends
			/// up being a potential memory leak.
			/// </summary>
			::std::atomic< bool > shuttingDown;

			/// <summary> The total number of worker threads to add to the thread pool. </summary>
			size_t workerThreads;

			/// <summary> The thread pool that gets populated when run is called. </summary>
			::std::vector< ::std::thread > threadPool;

			/// <summary> A string holding any exception message that might have been thrown from a worker thread. </summary>
			::std::string threadExceptionMessage;

			/// <summary> The threadExceptionMessage protection mutex. </summary>
			::std::recursive_mutex threadExceptionLock;
	};
} // namespace tools

#include "ProcessExecutor.hpp"

#endif // _PROCESS_EXECUTOR_H
