
// The policy used to determine the next process to
// select and begin executing.
//
// This uses a stl priority_queue to provide the order to
// the process execution.
//
// TODO: This needs to be updated so that we can keep a list of pools rather than just two.
//
// Project   : NaKama-Tools
// File Name : BalancerDispatchPolicy.cpp
// Date      : 17/05/2014
// Author    : Nicholas Welters

#include "BalancerDispatchPolicy.h"

#include <chrono>

namespace tools
{
	using namespace ::std::chrono_literals;

	using ::std::get;
	using ::std::runtime_error;
	using ::std::mutex;
	using ::std::unique_lock;

	const size_t BalancerDispatchPolicy::SHARED_JOB_QUEUE = ::std::numeric_limits< size_t >::max( );
	const size_t BalancerDispatchPolicy::SYNC = 0;
	const size_t BalancerDispatchPolicy::ASYNC = 1;

	/// <summary> ctor </summary>
	/// <param name="threads"> The number of threads in the tread pool. </param>
	BalancerDispatchPolicy::BalancerDispatchPolicy( const size_t& numberOfThreads )
		: DispatchPolicy( numberOfThreads )
		, lock( )
		, availableProcess( )
		, sharedQueueData( )
		, threadQueueData( numberOfThreads )
		, primary( SYNC )
		, secondary( ASYNC )
		, initialised( false )
	{
		for( size_t i = 0; i < numberOfThreads; ++i )
		{
			get< STATE >( threadQueueData[ i ] ) = WAITING;
		}
	}

	/// <summary> Set up the dispatchers internal states. </summary>
	/// <param name="minimumSync"> The minimum number of sync processes being executed. </param>
	/// <param name="maximumSync"> The maximum number of sync processes being executed. </param>
	/// <param name="minimumAsync"> The minimum number of async processes being executed. </param>
	/// <param name="maximumAsync"> The maximum number of async processes being executed. </param>
	void BalancerDispatchPolicy::initialiseDispatcher(
		const size_t& minimumSync,
		const size_t& maximumSync,
		const size_t& minimumAsync,
		const size_t& maximumAsync
	)
	{
		get< MINIMUM >( sharedQueueData[ SYNC ] ) = minimumSync;
		get< MAXIMUM >( sharedQueueData[ SYNC ] ) = maximumSync;
		get< CURRENT >( sharedQueueData[ SYNC ] ) = 0;

		get< MINIMUM >( sharedQueueData[ ASYNC ] ) = minimumAsync;
		get< MAXIMUM >( sharedQueueData[ ASYNC ] ) = maximumAsync;
		get< CURRENT >( sharedQueueData[ ASYNC ] ) = 0;

		initialised = true;
	}

	/// <summary> This makes sure that initialiseDispatcher is called. </summary>
	void BalancerDispatchPolicy::initialisedTest( )
	{
		if( !initialised )
		{
			throw runtime_error( "BalancerDispatchPolicy::initialiseDispatcher was not called." );
		}
	}

	/// <summary> Gets the next process in the process pool to be executed. </summary>
	/// <param name="threadID"> The thread to use to execute the process. </param>
	/// <returns> A smart pointer to a scheduled process. </returns>
	ProcessSPtr BalancerDispatchPolicy::next( const size_t& threadID )
	{
		unique_lock< mutex > uniqueLock( lock );

		initialisedTest( );

		//////////////////////////////
		// Reset the threads state //
		////////////////////////////
		if( get< STATE >( threadQueueData[ threadID ] ) != WAITING )
		{
			get< CURRENT >( sharedQueueData[ get< STATE >( threadQueueData[ threadID ] ) ] )--;
			availableProcess.notify_one( );
		}

		size_t state = WAITING;

		size_t shaderQueueSizeA = get< QUEUE >( sharedQueueData[ primary   ] ).size( );
		size_t shaderQueueSizeB = get< QUEUE >( sharedQueueData[ secondary ] ).size( );

		size_t threadQueueSizeA = get< QUEUES >( threadQueueData[ threadID ] )[ primary   ].size( );
		size_t threadQueueSizeB = get< QUEUES >( threadQueueData[ threadID ] )[ secondary ].size( );

		size_t queueSizeA = shaderQueueSizeA + threadQueueSizeA;
		size_t queueSizeB = shaderQueueSizeB + threadQueueSizeB;

		size_t minimumToExecuteA = get< MINIMUM >( sharedQueueData[ primary ] );
		size_t maximumToExecuteA = get< MAXIMUM >( sharedQueueData[ primary ] );

		size_t minimumToExecuteB = get< MINIMUM >( sharedQueueData[ secondary ] );
		size_t maximumToExecuteB = get< MAXIMUM >( sharedQueueData[ secondary ] );

		size_t currentlyExecutingA = get< CURRENT >( sharedQueueData[ primary ] );
		size_t currentlyExecutingB = get< CURRENT >( sharedQueueData[ secondary ] );

		bool needToExecuteA = queueSizeA != 0 && currentlyExecutingA < minimumToExecuteA;
		bool needToExecuteB = queueSizeB != 0 && currentlyExecutingB < minimumToExecuteB;
		bool shouldExecuteA = queueSizeA != 0 && currentlyExecutingA < maximumToExecuteA;
		bool shouldExecuteB = queueSizeB != 0 && currentlyExecutingB < maximumToExecuteB;

		////////////////////////////////////////////////////////////
		// Make sure the minimum number of processes are running //
		//////////////////////////////////////////////////////////
		if( needToExecuteA )
		{
			state = primary;
		}
		else if( needToExecuteB )
		{
			state = secondary;
		}
		////////////////////////////////////////////////////////////
		// Make sure the maximum number of processes are running //
		//////////////////////////////////////////////////////////
		else if( shouldExecuteA )
		{
			state = primary;
		}
		else if( shouldExecuteB )
		{
			state = secondary;
		}

		//////////////////////////////////////////
		// Assigning the threads current state //
		////////////////////////////////////////
		get< STATE >( threadQueueData[ threadID ] ) = state;

		// Swap the primary and secondary queues.
		size_t temp = primary;
		primary = secondary;
		secondary = temp;

		if( state == WAITING )
		{
			// Make sure that something hasn't already
			// shut the system down while we have been looking
			// for a process.
			if( isRunning( ) )
			{
				//////////////////////////////////////////////
				// Wait for a signal to try get a process. //
				////////////////////////////////////////////
				availableProcess.wait_for( uniqueLock, 1s );
			}

			// Return the void process and try again.
			return DispatchPolicy::next( threadID );
		}
		else
		{
			/////////////////////////////////////////////////
			// Update the current state data and dispatch //
			///////////////////////////////////////////////
			get< CURRENT >( sharedQueueData[ state ] )++;

			// Select A Process To Execute
			ProcessSPtr pProcess;
			if( get< QUEUES >( threadQueueData[ threadID ] )[ state ].size( ) )
			{
				pProcess = get< QUEUES >( threadQueueData[ threadID ] )[ state ].top( );
				get< QUEUES >( threadQueueData[ threadID ] )[ state ].pop( );
			}
			else
			{
				pProcess = get< QUEUE >( sharedQueueData[ state ] ).top( );
				get< QUEUE >( sharedQueueData[ state ] ).pop( );
			}

			return pProcess;
		}
	}

	/// <summary> Scheduled a process to be executed in a selected thread job pool. </summary>
	/// <param name="pProcess"> A smart pointer to the process that will be scheduled for execution. </param>
	/// <param name="index"> The queue data array index to add the process to. </param>
	/// <param name="threadID"> The thread job pool to add the process too. This is default to
	///							sending the job to the shared job pool. </param>
	void BalancerDispatchPolicy::add( const ProcessSPtr& pProcess, const size_t& index, const size_t& threadID )
	{
		unique_lock< mutex > uniqueLock( lock );

		initialisedTest( );

		if( isShuttingDown( ) )
		{
			return;
		}

		if( threadID == SHARED_JOB_QUEUE )
		{
			get< QUEUE >( sharedQueueData[ index ] ).push( pProcess );
		}
		else
		{
			get< QUEUES >( threadQueueData[ threadID ] )[ index ].push( pProcess );
		}

		if( get< QUEUE >( sharedQueueData[ index ] ).size( ) == 1 )
		{
			uniqueLock.unlock( );
			availableProcess.notify_one( );
		}
		else if( get< QUEUE >( sharedQueueData[ index ] ).size( ) == 0 
			  && get< QUEUES >( threadQueueData[ threadID ] )[ index ].size( ) == 1 )
		{
			// We need to notify more than one as we have to notify this specific thread.
			uniqueLock.unlock( );
			availableProcess.notify_all( );
		}
	}

	/// <summary> Scheduled a process to be executed by the "synchronise" thread pool. </summary>
	/// <param name="pProcess"> A smart pointer to the process that will be scheduled for execution. </param>
	/// <param name="threadID"> The thread job pool to add the process too. This is default to
	///							sending the job to the shared job pool. </param>
	void BalancerDispatchPolicy::addSync( const ProcessSPtr& pProcess, const size_t& threadID )
	{
		add( pProcess, SYNC, threadID );
	}

	/// <summary> Scheduled a process to be executed by the "asynchronise" thread pool. </summary>
	/// <param name="pProcess"> A smart pointer to the process that will be scheduled for execution. </param>
	/// <param name="threadID"> The thread job pool to add the process too. This is default to
	///							sending the job to the shared job pool. </param>
	void BalancerDispatchPolicy::addAsync( const ProcessSPtr& pProcess, const size_t& threadID )
	{
		add( pProcess, ASYNC, threadID );
	}

	/// <summary>
	/// Notify any threads that might be waiting for
	///	for a process to execute.
	/// </summary>
	void BalancerDispatchPolicy::notifyThreads( )
	{
		availableProcess.notify_all( );
	}

} /* End of namespace Engine */
