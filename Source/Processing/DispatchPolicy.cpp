
// The policy used to determine the next process to
// select and begin executing.
//
// Project   : NaKama-Tools
// File Name : DispatchPolicy.h
// Date      : 11/05/2014
// Author    : Nicholas Welters

#include "DispatchPolicy.h"
#include "Process.h"

namespace tools
{
	/// <summary> A blank process that has many uses. </summary>
	class VoidProcess: public Process
	{
		public:
			virtual void execute( )
			{ }
	};

	/// <summary> ctor </summary>
	/// <param name="threads"> The number of threads in the tread pool. </param>
	DispatchPolicy::DispatchPolicy( const size_t& threads )
		: voidProcess( new VoidProcess )
	{
		( void* )( threads );
	}

	/// <summary> Gets the next process in the process pool to be executed. </summary>
	/// <param name="threadID"> The thread to use to execute the process. </param>
	/// <returns> A smart pointer to a scheduled process. </returns>
	ProcessSPtr DispatchPolicy::next( const size_t& threadID )
	{
		( void* ) threadID;
		return voidProcess;
	}
} // namespace tools
