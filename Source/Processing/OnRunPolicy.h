#pragma once
// Policy of the ProcessEngine of what should happen when the engine is started.
// - Join with the worker threads and exit when the engine shuts down.
// - Allow the worker threads to start performing there work.This allows the
//   owner thread to continue with its own work.
//
// Project   : NaKama-Tools
// File Name : OnRunPolicy.h
// Date      : 11/05/2014
// Author    : Nicholas Welters

#ifndef _ON_RUN_POLICY_H
#define _ON_RUN_POLICY_H

namespace tools
{
	/// <summary> Policy interface of the ProcessEngine of what should happen when the engine is started. </summary>
	class OnRunPolicy
	{
		protected:
			/// <summary> Policy used when run is called on the Process Executor. </summary>
			virtual void joinWorkerPolicy( ) = 0;

			/// <summary>
			/// Set the first element ID so that we can skip the owner if needed so that
			/// anything that will try and use thread 0 wont send processes to a list that never
			/// gets executed.
			/// </summary>
			/// <returns> The starting index for the worker threads. </returns>
			virtual size_t startingIndex( ) = 0;

			/// <summary> Worker thread entry point, and run time loop. </summary>
			/// <param name="threadID"> The thread to run the process on. </param>
			virtual void threadMain( const size_t & threadID ) = 0;
	};

	/// <summary>
	/// This will set run to be non-blocking.
	/// 
	/// The owning thread will not join with the worker threads
	/// to execute processes.
	/// </summary>
	class UseWorkersPolicy : public OnRunPolicy
	{
		protected:
			/// <summary> The caller doesn't join the thread pool. </summary>
			virtual void joinWorkerPolicy( ) final override;

			/// <summary>
			/// Set the first element ID so that we can skip the owner if needed so that
			/// anything that will try and use thread 0 wont send processes to a list that never
			/// gets executed.
			/// </summary>
			/// <returns> The starting index, 0, for the worker threads. </returns>
			virtual size_t startingIndex( ) final override;
	};

	/// <summary>
	/// This will set run to be blocking.
	/// 
	/// The owning thread will join with the worker threads
	/// to execute processes.
	/// </summary>
	class JoinWorkersPolicy : public OnRunPolicy
	{
		protected:
			/// <summary> The caller joins the thread pool and does some work. </summary>
			virtual void joinWorkerPolicy( ) final override;

			/// <summary>
			/// Set the first element ID so that we can skip the owner if needed so that
			/// anything that will try and use thread 0 wont send processes to a list that never
			/// gets executed.
			/// </summary>
			/// <returns> The starting index, 1, for the worker threads. </returns>
			virtual size_t startingIndex( ) final override;
	};
} // namespace tools

#endif // _ON_RUN_POLICY_H
