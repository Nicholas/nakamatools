#pragma once
// The policy used to determine the next process to
// select and begin executing.
//
// This uses a stl queue to provide a FIFO order to
// the process execution.
//
// Project   : NaKama-Tools
// File Name : FIFODispatchPolicy.h
// Date      : 17/05/2014
// Author    : Nicholas Welters

#ifndef _FIFO_DISPATCH_POLICY_H
#define _FIFO_DISPATCH_POLICY_H

#include "DispatchPolicy.h"

#include <queue>
#include <mutex>
#include <condition_variable>

namespace tools
{
	/// <summary>
	/// The policy used to determine the next process to
	/// select and begin executing.
	///
	/// This uses a stl queue to provide a FIFO order to
	/// the process execution.
	///</summary>
	class FIFODispatchPolicy : public DispatchPolicy
	{
		public:
			/// <summary> ctor </summary>
			/// <param name="threads"> The number of threads in the tread pool. </param>
			explicit FIFODispatchPolicy( const size_t& threads );

			/// <summary> dtor. </summary>
			virtual ~FIFODispatchPolicy( ) = default;

			/// <summary> Gets the next process in the process pool to be executed. </summary>
			/// <param name="threadID"> The thread to use to execute the process. </param>
			/// <returns> A smart pointer to a scheduled process. </returns>
			virtual ProcessSPtr next( const size_t& threadID ) final override;

			/// <summary> Scheduled a process to be executed by the thread pool. </summary>
			/// <param name="threadID"> A smart pointer to the process that will be scheduled for execution. </param>
			void add( const ProcessSPtr& pProcess );

			/// <summary>
			/// Notify any threads that might be waiting for
			///	for a process to execute.
			/// </summary>
			virtual void notifyThreads( ) final override;

		private:
			/// <summary> Mutex used to protect the queue from concurrent access. </summary>
			::std::mutex lock;

			/// <summary> Condition used to get threads to wait for a available process. </summary>
			::std::condition_variable availableProcess;

			/// <summary> The list used to hold the FIFO queue of processes. </summary>
			::std::queue< ProcessSPtr > queue;
	};
} // namespace tools

#endif // _FIFO_DISPATCH_POLICY_H
