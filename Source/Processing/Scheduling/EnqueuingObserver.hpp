#pragma once
// A process observer that will schedule a process for execution
// in a process executor when it gets the completed notification
// signal.
//
// Project   : NaKama-Tools
// File Name : EnqueuingObserver.hpp
// Date      : 23/07/2014
// Author    : Nicholas Welters

#include "EnqueuingObserver.h"

#ifndef _ENQUEUING_OBSERVER_HPP
#define _ENQUEUING_OBSERVER_HPP

#include "../Process.h"

namespace tools
{
	using ::std::shared_ptr;

	/// <summary> ctor. </summary>
	/// <param name="pProcess"> The  process to schedule when this is notified. </param>
	/// <param name="pProcessExecutor"> The executor to send the process to. </param>
	/// <param name="threadID"> The thread id that the process needs to be executed on. </param>
	template< class ProcessExecutor, class EnqueuePolicy >
	EnqueuingObserver< ProcessExecutor, EnqueuePolicy >::EnqueuingObserver( const ProcessSPtr& pProcess, const ProcessExecutorSPtr& pProcessExecutor, const size_t& threadID )
		: pProcessExecutor( pProcessExecutor )
		, pProcess( pProcess )
		, threadID( threadID )
		, subjectCounter( { 0 } )
		, completionCounter( { 0 } )
	{ }

	/// <summary> Notify this that a subject has completed execution. </summary>
	template< class ProcessExecutor, class EnqueuePolicy >
	void EnqueuingObserver< ProcessExecutor, EnqueuePolicy >::completedNotification( )
	{
		++completionCounter;
		int counter = subjectCounter;
		if( completionCounter.compare_exchange_strong( counter, 0 ) )
		{
			EnqueuePolicy::enqueue( pProcess, pProcessExecutor, threadID );
		}
	}

	/// <summary> Set the observers that it should wait for an subjects notification signal. </summary>
	template< class ProcessExecutor, class EnqueuePolicy >
	void EnqueuingObserver< ProcessExecutor, EnqueuePolicy >::addCompletedSubject( )
	{
		++subjectCounter;
	}

	/// <summary> Adds the process to the executor for execution by the specified thread. <summary>
	/// <param name="pProcess"> The  process to schedule when this is notified. </param>
	/// <param name="pProcessExecutor"> The executor to send the process to. </param>
	/// <param name="threadID"> The thread id that the process needs to be executed on. </param>
	template< class FIFOProcessExecutor >
	void FIFOEnqueuPolicy< FIFOProcessExecutor >::enqueue( const ProcessSPtr& pProcess, const shared_ptr< FIFOProcessExecutor >& pProcessExecutor, const size_t& threadID )
	{
		pProcessExecutor->add( pProcess, threadID );
	}

	/// <summary> Adds the process to the executor for execution by the specified thread. <summary>
	/// <param name="pProcess"> The  process to schedule when this is notified. </param>
	/// <param name="pProcessExecutor"> The executor to send the process to. </param>
	/// <param name="threadID"> The thread id that the process needs to be executed on. </param>
	template< class AsyncProcessExecutor >
	void AsyncEnqueuPolicy< AsyncProcessExecutor >::enqueue( const ProcessSPtr& pProcess, const shared_ptr< AsyncProcessExecutor >& pProcessExecutor, const size_t& threadID )
	{
		pProcessExecutor->addAsync( pProcess, threadID );
	}

	/// <summary> Adds the process to the executor for execution by the specified thread. <summary>
	/// <param name="pProcess"> The  process to schedule when this is notified. </param>
	/// <param name="pProcessExecutor"> The executor to send the process to. </param>
	/// <param name="threadID"> The thread id that the process needs to be executed on. </param>
	template< class SyncProcessExecutor >
	void SyncProcessPolicy< SyncProcessExecutor >::enqueue( const ProcessSPtr& pProcess, const shared_ptr< SyncProcessExecutor >& pProcessExecutor, const size_t& threadID )
	{
		pProcessExecutor->addSync( pProcess, threadID );
	}
} // namespace tools

#endif // _ENQUEUING_OBSERVER_HPP
