#pragma once
// A functor wrapper for the ScheduledProcess type.
//
// Project   : NaKama-Tools
// File Name : ScheduledFunctorProcess.hpp
// Date      : 23/11/2017
// Author    : Nicholas Welters

#include "ScheduledFunctorProcess.h"

#ifndef _SCHEDULED_FUNCTOR_PROCESS_HPP
#define _SCHEDULED_FUNCTOR_PROCESS_HPP

namespace tools
{
	using ::std::function;

	/// <summary> ctor. </summary>
	/// <param name="functor"> ScheduledFunctorProcess functor to be executed in the engine. </param>
	template< class CompletedRunPolicyType, class SkippedRunPolicyType, class DelayPolicyType, class IntervalPolicyType >
	ScheduledFunctorProcess< CompletedRunPolicyType, SkippedRunPolicyType, DelayPolicyType, IntervalPolicyType >::ScheduledFunctorProcess( const function< void ( ) >& functor )
		: ScheduledProcess< CompletedRunPolicyType, SkippedRunPolicyType, DelayPolicyType, IntervalPolicyType >( )
		, functor( functor )
	{
	}

	/// <summary>  The entry point to a scheduled task. </summary>
	template< class DelayPolicyType, class IntervalPolicyType, class SkippedRunPolicyType, class CompletedRunPolicyType >
	void ScheduledFunctorProcess< DelayPolicyType, IntervalPolicyType, SkippedRunPolicyType, CompletedRunPolicyType >::run( )
	{
		functor( );
	}
} // namespace tools

#endif // _SCHEDULED_PROCESS_HPP