#pragma once
// A functor wrapper for the ScheduledProcess type.
//
// Project   : NaKama-Tools
// File Name : ScheduledFunctorProcess.h
// Date      : 23/11/2017
// Author    : Nicholas Welters

#ifndef _SCHEDULED_FUNCTOR_PROCESS_H
#define _SCHEDULED_FUNCTOR_PROCESS_H

#include "ScheduledProcess.h"

#include <functional>

namespace tools
{
	/// <summary>
	/// A functor wrapper for the ScheduledProcess type.
	///</summary>
	/// <template name="CompletedRunPolicyType"> What the process will do when its completed. </template>
	/// <template name="SkippedRunPolicyType"> What the process does if its execution was skipped due to the delay or interval policy. </template>
	/// <template name="DelayPolicyType"> How the process determines if it should be delayed. </template>
	/// <template name="IntervalPolicyType"> How the process determines if its waiting to be executed again. </template>
	template
	<
		class CompletedRunPolicyType = CompletedRunPolicy,
		class SkippedRunPolicyType   = SkippedRunPolicy,
		class DelayPolicyType        = DelayPolicy,
		class IntervalPolicyType     = IntervalPolicy
	>
	class ScheduledFunctorProcess
		: public ScheduledProcess< CompletedRunPolicyType, SkippedRunPolicyType, DelayPolicyType, IntervalPolicyType >
	{
		public:
			/// <summary> ctor. </summary>
			/// <param name="functor"> The functor to be executed in the engine. </param>
			explicit ScheduledFunctorProcess( const ::std::function< void( ) >& functor );

			/// <summary> dtor. </summary>
			virtual ~ScheduledFunctorProcess( ) = default;

			/// <summary>  The entry point to a scheduled task. </summary>
			virtual void run( ) final override;

		private:
			/// <summary> The functor that we want to scheduled for processing. </summary>
			::std::function< void( ) > functor;
	};
} // namespace tools
	
#include "ScheduledFunctorProcess.hpp"

#endif // _SCHEDULED_FUNCTOR_PROCESS_H
