
// This defines the scheduled process observer pattern policies.
//
// The policies are implemented as subjects of the observer pattern.
// This lets us assign observes to the execution state of a process.
//
// The following policies are implemented.
// CompletedRunPolicySubject
// SkippedRunPolicySubject
//
// The following interfaces are also defined.
// CompletedRunPolicyObserver
// SkippedRunPolicyObserver
//
// This allows us to assign multiple observers to a single execution state of a process.
//
// Knowing when a process has executed is important to chaining processes together.
// This lets us create a loop that has a single tick event followed by a render
// and update events which then all the same tick event waiting on them
// to maintain a synchronise cycle.
//
// Project   : NaKama-Tools
// File Name : ObserverPolicies.cpp
// Date      : 23/07/2014
// Author    : Nicholas Welters

#include "ObservedPolicies.h"

namespace tools
{
	////////////////
	// Completed //
	//////////////
	/// <summary> ctor. </summary>
	CompletedRunPolicySubject::CompletedRunPolicySubject( )
		: observers( )
	{ }

	/// <summary> Notifies the observers that this has completed execution. </summary>
	void CompletedRunPolicySubject::completed( )
	{
		for( CompletedRunPolicyObserverWPtr pObserver : observers )
		{
			CompletedRunPolicyObserverSPtr observer = pObserver.lock( );

			if( observer )
			{
				observer->completedNotification( );
			}
		}
	}

	/// <summary> Adds an observer to the subject. </summary>
	/// <param name="pObserver"> An observer interested in the completion of the processes execution. </param>
	void CompletedRunPolicySubject::addCompletedObserver( const CompletedRunPolicyObserverWPtr& pObserver )
	{
		CompletedRunPolicyObserverSPtr observer = pObserver.lock( );

		if( observer )
		{
			observer->addCompletedSubject( );
			observers.push_back( pObserver );
		}
	}


	//////////////
	// Skipped //
	////////////
	SkippedRunPolicySubject::SkippedRunPolicySubject( )
		: observers( )
	{ }

	/// <summary> Notifies the observers that this has skipped its execution. </summary>
	void SkippedRunPolicySubject::skipped( )
	{
		for( SkippedRunPolicyObserverWPtr pObserver : observers )
		{
			SkippedRunPolicyObserverSPtr observer = pObserver.lock( );

			if( observer )
			{
				observer->skippedNotification( );
			}
		}
	}

	/// <summary> Adds an observer to the subject. </summary>
	/// <param name="pObserver"> An observer interested in the skipping of the processes execution. </param>
	void SkippedRunPolicySubject::addSkippedObserver( const SkippedRunPolicyObserverWPtr& pObserver )
	{
		SkippedRunPolicyObserverSPtr observer = pObserver.lock( );

		if( observer )
		{
			//pObserver->addSkippedSubject( );
			observers.push_back( pObserver );
		}
	}
} // namespace tools
