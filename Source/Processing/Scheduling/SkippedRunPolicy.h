#pragma once
// The policy process is run if the event isn't be executed by a thread.
// This usually means that the process should reschedule its self again
// so that it might be executed later.
//
// Project   : NaKama-Tools
// File Name : SkippedRunPolicy.h
// Date      : 11/05/2014
// Author    : Nicholas Welters

#ifndef _SKIPPED_RUN_POLICY_H
#define _SKIPPED_RUN_POLICY_H

namespace tools
{
	/// <summary>
	/// The policy process is run if the event isn't be executed by a thread.
	/// This usually means that the process should reschedule its self again
	/// so that it might be executed later.
	/// </summary>
	class SkippedRunPolicy
	{
		public:
			/// <summary> Called if the observed process is skipped. </summary>
			virtual void skipped( )
			{ };
	};
} // namespace tools

#endif // _SKIPPED_RUN_POLICY_H
