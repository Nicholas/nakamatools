#pragma once
// The process interface for all tasks that need to
// be executed in the process engine at some specified schedule.
//
// All the default policies do nothing, making the class work
// in the same way as the Process class except execute is replaced.
//
// Project   : NaKama-Tools
// File Name : ScheduledProcess.h
// Date      : 11/05/2014
// Author    : Nicholas Welters

#ifndef _SCHEDULED_PROCESS_H
#define _SCHEDULED_PROCESS_H

#include "../Process.h"

#include "CompletedRunPolicy.h"
#include "DelayPolicy.h"
#include "IntervalPolicy.h"
#include "SkippedRunPolicy.h"

namespace tools
{
	/// <summary>
	/// The process interface for all tasks that need to
	/// be executed in the process engine at some specified schedule.
	///
	/// All the default policies do nothing, making the class work
	/// in the same way as the Process class except execute is replaced.
	///</summary>
	/// <template name="CompletedRunPolicyType"> What the process will do when its completed. </template>
	/// <template name="SkippedRunPolicyType"> What the process does if its execution was skipped due to the delay or interval policy. </template>
	/// <template name="DelayPolicyType"> How the process determines if it should be delayed. </template>
	/// <template name="IntervalPolicyType"> How the process determines if its waiting to be executed again. </template>
	template
	<
		class CompletedRunPolicyType = CompletedRunPolicy,
		class SkippedRunPolicyType   = SkippedRunPolicy,
		class DelayPolicyType        = DelayPolicy,
		class IntervalPolicyType     = IntervalPolicy
	>
	class ScheduledProcess
		: public Process
		, public CompletedRunPolicyType
		, public SkippedRunPolicyType
		, public DelayPolicyType
		, public IntervalPolicyType
	{
		public:
			/// <summary> dtor. </summary>
			virtual ~ScheduledProcess( ) = default;

			/// <summary> The entry point to a tasks execution. </summary>
			virtual void execute( ) final override;

			/// <summary>  The entry point to a scheduled task. </summary>
			virtual void run( ) = 0;
	};
} // namespace tools
	
#include "ScheduledProcess.hpp"

#endif // _SCHEDULED_PROCESS_H
