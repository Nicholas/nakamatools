#pragma once
// The policy performed to test is a scheduled process should
// still wait to be executed for the first time.
//
// Project   : NaKama-Tools
// File Name : DelayPolicy.h
// Date      : 11/05/2014
// Author    : Nicholas Welters

#ifndef _DELAY_POLICY_H
#define _DELAY_POLICY_H

namespace tools
{
	/// <summary>
	/// The policy performed to test is a scheduled process should
	/// still wait to be executed for the first time.
	/// </summary>
	class DelayPolicy
	{
		public:
			/// <summary> Used to determine if the process is still waiting to be executed the first time. </summary>
			/// <returns> The status of the processes scheduling delay. </returns>
			bool isDelayed( )
			{
				return false;
			};
	};
} // namespace tools

#endif // _DELAY_POLICY_H
