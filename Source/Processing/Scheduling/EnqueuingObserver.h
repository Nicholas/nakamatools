#pragma once
// A process observer that will schedule a process for execution
// in a process executor when it gets the completed notification
// signal.
//
// Project   : NaKama-Tools
// File Name : EnqueuingObserver.h
// Date      : 23/07/2014
// Author    : Nicholas Welters

#ifndef _ENQUEUING_OBSERVER_H
#define _ENQUEUING_OBSERVER_H

#include "ObservedPolicies.h"
#include "Macros.h"

#include <memory>
#include <atomic>

namespace tools
{
	FORWARD_DECLARE( Process );

	/// <summary>
	/// A process observer that will schedule a process for execution
	/// in a process executor when it gets the completed notification
	/// signal.
	///</summary>
	/// <template name="ProcessExecutor"> The process executor that will be given the process for execution. </template>
	/// <template name="EnqueuePolicy"> The policy that will define how the observer sends the process to the executor. </template>
	template< class ProcessExecutor, class EnqueuePolicy >
	class EnqueuingObserver : public CompletedRunPolicyObserver, public EnqueuePolicy
	{
		public:
			/// <summary> The process executors shared smart pointer. </summary>
			typedef ::std::shared_ptr< ProcessExecutor > ProcessExecutorSPtr;

		public:
			/// <summary> ctor. </summary>
			/// <param name="pProcess"> The  process to schedule when this is notified. </param>
			/// <param name="pProcessExecutor"> The executor to send the process to. </param>
			/// <param name="threadID"> The thread id that the process needs to be executed on. </param>
			EnqueuingObserver( const ProcessSPtr& pProcess, const ProcessExecutorSPtr& pProcessExecutor, const size_t& threadID );

			/// <summary> dtor. </summary>
			virtual ~EnqueuingObserver( ) = default;

			/// <summary> Notify this that a subject has completed execution. </summary>
			virtual void completedNotification( ) final override;

			/// <summary> Set the observers that it should wait for an subjects notification signal. </summary>
			virtual void addCompletedSubject( ) final override;

		private:
			/// <summary> The process that we will get executed in the Process Executor.
			ProcessSPtr pProcess;

			/// <summary> The executor service that executes our process. </summary>
			ProcessExecutorSPtr pProcessExecutor;

			/// <summary> This is the thread that the process needs to be executed on. </summary>
			size_t threadID;
			
			/// <summary>
			/// A total subject count so that we can wait for multiple processes
			/// to finish executing before scheduling the owned process.
			/// </summary>
			::std::atomic_int subjectCounter;

			/// <summary> The number of subjects that have notified this observer of there completion. </summary>
			::std::atomic_int completionCounter;

	};

	/// <summary> The policy that will adds a process to a FIFO based process executor. <summary>
	/// <template name="ProcessExecutor"> The process executor that will be given the process for execution. </template>
	template< class FIFOProcessExecutor >
	class FIFOEnqueuPolicy
	{
		public:
			/// <summary> Adds the process to the executor for execution by the specified thread. <summary>
			/// <param name="pProcess"> The  process to schedule when this is notified. </param>
			/// <param name="pProcessExecutor"> The executor to send the process to. </param>
			/// <param name="threadID"> The thread id that the process needs to be executed on. </param>
			void enqueue( const ProcessSPtr& pProcess, const ::std::shared_ptr< FIFOProcessExecutor >& pProcessExecutor, const size_t& threadID );
	};

	/// <summary> The policy that will adds a process to a async compatible process executor. <summary>
	/// <template name="AsyncProcessExecutor"> The process executor that will be given the process for execution. </template>
	template< class AsyncProcessExecutor >
	class AsyncEnqueuPolicy
	{
		public:
			/// <summary> Adds the process to the executor for execution by the specified thread. <summary>
			/// <param name="pProcess"> The  process to schedule when this is notified. </param>
			/// <param name="pProcessExecutor"> The executor to send the process to. </param>
			/// <param name="threadID"> The thread id that the process needs to be executed on. </param>
			void enqueue( const ProcessSPtr& pProcess, const ::std::shared_ptr< AsyncProcessExecutor >& pProcessExecutor, const size_t& threadID );
	};

	/// <summary> The policy that will adds a process to a sync compatible process executor. <summary>
	/// <template name="SyncProcessExecutor"> The process executor that will be given the process for execution. </template>
	template< class SyncProcessExecutor >
	class SyncProcessPolicy
	{
		public:
			/// <summary> Adds the process to the executor for execution by the specified thread. <summary>
			/// <param name="pProcess"> The  process to schedule when this is notified. </param>
			/// <param name="pProcessExecutor"> The executor to send the process to. </param>
			/// <param name="threadID"> The thread id that the process needs to be executed on. </param>
			void enqueue( const ProcessSPtr& pProcess, const ::std::shared_ptr< SyncProcessExecutor >& pProcessExecutor, const size_t& threadID );
	};
} // namespace tools

#include "EnqueuingObserver.hpp"

#endif // _ENQUEUING_OBSERVER_H
