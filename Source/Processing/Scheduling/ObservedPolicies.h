#pragma once
// This defines the scheduled process observer pattern policies.
//
// The policies are implemented as subjects of the observer pattern.
// This lets us assign observes to the execution state of a process.
//
// The following policies are implemented.
// CompletedRunPolicySubject
// SkippedRunPolicySubject
//
// The following interfaces are also defined.
// CompletedRunPolicyObserver
// SkippedRunPolicyObserver
//
// This allows us to assign multiple observers to a single execution state of a process.
//
// Knowing when a process has executed is important to chaining processes together.
// This lets us create a loop that has a single tick event followed by a render
// and update events which then all the same tick event waiting on them
// to maintain a synchronise cycle.
//
// Project   : NaKama-Tools
// File Name : ObserverPolicies.h
// Date      : 23/07/2014
// Author    : Nicholas Welters

#ifndef _OBSERVED_POLICIES_H
#define _OBSERVED_POLICIES_H

#include <memory>
#include <list>

namespace tools
{
	////////////////
	// Completed //
	//////////////
	/// <summary>
	/// Interface
	/// The observer for completed events will implement this method.
	///</summary>
	class CompletedRunPolicyObserver
	{
		public:
			/// <summary> Notify this that a subject has completed execution. </summary>
			virtual void completedNotification( ) = 0;

			/// <summary> Set the observers that it should wait for an subjects notification signal. </summary>
			virtual void addCompletedSubject( ) = 0;
	};

	/// <summary> A weak pointer here to prevent a circular dependency so that the data clears its self up correctly. </summary>
	typedef ::std::weak_ptr< CompletedRunPolicyObserver > CompletedRunPolicyObserverWPtr;

	/// <summary> A shared pointer type to simplify the locking and working with the type. </summary>
	typedef ::std::shared_ptr< CompletedRunPolicyObserver > CompletedRunPolicyObserverSPtr;

	/// <summary>
	/// This is the event that is called when a process completes its execution.
	/// This notifies all the observers of the event.
	///</summary>
	class CompletedRunPolicySubject
	{
		public:
			/// <summary> ctor. </summary>
			CompletedRunPolicySubject( );

			/// <summary> dtor. </summary>
			virtual ~CompletedRunPolicySubject( ) = default;

			/// <summary> Notifies the observers that this has completed execution. </summary>
			void completed( );

			/// <summary> Adds an observer to the subject. </summary>
			/// <param name="pObserver"> An observer interested in the completion of the processes execution. </param>
			virtual void addCompletedObserver( const CompletedRunPolicyObserverWPtr& pObserver );

		private:
			/// <summary> The subjects observer list. </summary>
			::std::list< CompletedRunPolicyObserverWPtr > observers;

	};

	//////////////
	// Skipped //
	////////////
	/// <summary>
	/// Interface
	/// The observer for skipped events will implement this method.
	///</summary>
	class SkippedRunPolicyObserver
	{
		public:
			/// <summary> Notify this that a subject has skipped its execution. </summary>
			virtual void skippedNotification( ) = 0;
	};

	/// <summary> A weak pointer here to prevent a circular dependency so that the data clears its self up correctly. </summary>
	typedef ::std::weak_ptr< SkippedRunPolicyObserver > SkippedRunPolicyObserverWPtr;

	/// <summary> A shared pointer type to simplify the locking and working with the type. </summary>
	typedef ::std::shared_ptr< SkippedRunPolicyObserver > SkippedRunPolicyObserverSPtr;

	/// <summary>
	/// This is the event that is called when a process skips its execution.
	/// This notifies all the observers of the event.
	///</summary>
	class SkippedRunPolicySubject
	{
		public:
			/// <summary> ctor. </summary>
			SkippedRunPolicySubject( );

			/// <summary> dtor. </summary>
			virtual ~SkippedRunPolicySubject( ) = default;

			/// <summary> Notifies the observers that this has skipped its execution. </summary>
			void skipped( );

			/// <summary> Adds an observer to the subject. </summary>
			/// <param name="pObserver"> An observer interested in the skipping of the processes execution. </param>
			virtual void addSkippedObserver( const SkippedRunPolicyObserverWPtr& pObserver );

		private:
			/// <summary> The subjects observer list. </summary>
			::std::list< SkippedRunPolicyObserverWPtr > observers;
	};
} // namespace tools

#endif // _OBSERVED_POLICIES_H
