#pragma once
// The policy performed after a process is executed.
//
// Project   : NaKama-Tools
// File Name : CompletedRunPolicy.h
// Date      : 11/05/2014
// Author    : Nicholas Welters

# ifndef _COMPLETED_RUN_POLICY_H
# define _COMPLETED_RUN_POLICY_H

namespace tools
{
	/// <summary>
	/// The policy performed after a process is executed.
	/// </summary>
	class CompletedRunPolicy
	{
		public:
			/// <summary> Called on the completion of the processes execution. </summary>
			void completed( )
			{ };
	};
} // namespace tools

# endif // _COMPLETED_RUN_POLICY_H
