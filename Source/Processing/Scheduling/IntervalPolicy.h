#pragma once
// The policy process is run if the event isn't be executed by a thread.
// This usually means that the process should reschedule its self again
// so that it might be executed later.
//
// Project   : NaKama-Tools
// File Name : IntervalPolicy.h
// Date      : 11/05/2014
// Author    : Nicholas Welters

#ifndef _INTERVAL_POLICY_H
#define _INTERVAL_POLICY_H

namespace tools
{
	/// <summary>
	/// The policy performed to test is a scheduled process should
	/// still wait to be executed after having been executed once.
	/// </summary>
	class IntervalPolicy
	{
		public:
			/// <summary> Used to determine if the process is within an interval where is shouldn't be executed. </summary>
			/// <returns> The status of the processes scheduling interval. </returns>
			bool isInInterval( )
			{
				return false;
			};
	};
} // namespace tools

#endif // _INTERVAL_POLICY_H
