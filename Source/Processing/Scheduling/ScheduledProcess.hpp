#pragma once
// The process interface for all tasks that need to
// be executed in the process engine at some specified schedule.
//
// All the default policies do nothing, making the class work
// in the same way as the Process class except execute is replaced.
//
// Project   : NaKama-Tools
// File Name : ScheduledProcess.hpp
// Date      : 11/05/2014
// Author    : Nicholas Welters

#include "ScheduledProcess.h"

#ifndef _SCHEDULED_PROCESS_HPP
#define _SCHEDULED_PROCESS_HPP

namespace tools
{
	/// <summary>  The entry point to a scheduled task. </summary>
	template< class CompletedRunPolicyType, class SkippedRunPolicyType, class DelayPolicyType, class IntervalPolicyType >
	void ScheduledProcess< CompletedRunPolicyType, SkippedRunPolicyType, DelayPolicyType, IntervalPolicyType >::execute( )
	{
		if( DelayPolicyType::isDelayed( ) )
		{
			SkippedRunPolicyType::skipped( );
			return;
		}

		if( IntervalPolicyType::isInInterval( ) )
		{
			SkippedRunPolicyType::skipped( );
			return;
		}

		run( );

		CompletedRunPolicyType::completed( );
	}

	// I have a dream...
	//template
	//<
	//	class CompletedRunPolicyType
	//>
	//void ScheduledProcess< CompletedRunPolicyType, SkippedRunPolicy, DelayPolicy, IntervalPolicy >::execute( )
	//{
	//	run( );
	//
	//	CompletedRunPolicy::completed( );
	//}
} // namespace tools

#endif // _SCHEDULED_PROCESS_HPP