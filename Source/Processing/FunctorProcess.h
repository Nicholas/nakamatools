#pragma once
// A generic process type that takes a functor to be executed in the process engine.
//
// Project   : NaKama-Tools
// File Name : Process.h
// Date      : 11/05/2014
// Author    : Nicholas Welters

#ifndef _FUNCTOR_PROCESS_H
#define _FUNCTOR_PROCESS_H

#include "Process.h"

#include <functional>

namespace tools
{
	/// <summary> This is a wrapper for functors so that we can scheduled functors for execution. </summary>
	class FunctorProcess : public Process
	{
		public:
			/// <summary> ctor. </summary>
			/// <param name="functor"> The functor to be executed in the engine. </param>
			FunctorProcess( const ::std::function< void ( ) >& functor );

			/// <summary> 
			/// The entry point to a tasks execution.
			/// This forwards the request to the functor.
			/// </summary>
			virtual void execute( ) final override;

		private:
			/// <summary> The functor that we want to scheduled for processing. </summary>
			::std::function< void ( ) > functor;
	};
} // namespace tools

#endif // _FUNCTOR_PROCESS_H
