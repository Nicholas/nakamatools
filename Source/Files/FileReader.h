#pragma once
// Read the contents of a text file and then
// pass the contents to a data parser.
//
//
// Project   : NaKama-Tools
// File Name : FileReader.h
// Date      : 26/02/2018
// Author    : Nicholas Welters
#ifndef _FILE_READER_H
#define _FILE_READER_H

#include "Macros.h"

#include <memory>
#include <functional>
#include <unordered_map>

namespace tools
{
	typedef std::function< void ( const std::function< void () >& ) > Scheduler;

	FORWARD_DECLARE( JsonParser );

	/// <summary>
	/// Read the contents of a text file and then
	/// pass the contents to a data parser.
	/// </summary>
	class FileReader
	{
		public:
			/// <summary> ctor </summary>
			/// <param name="scheduler"> A functor that will schedule another functor to execute. </param>
			FileReader( const Scheduler& scheduler );

			/// <summary> dtor </summary>
			~FileReader( ) = default;

			/// <summary> Add a json parser that will parse the contents of a json file based on its meta data type. </summary>
			/// <param name="type"> The parsers unique data type to parse. </param>
			/// <param name="pParser"> A json parser that will process the file contents. </param>
			void add( const std::string& type, const JsonParserSPtr& pParser );

			/// <summary> Read a text file from disk and then process the file. </summary>
			/// <param name="fileName"> The file path and file name. </param>
			void readFile( const std::string& fileName );

		private:
			/// <summary> Read a text file from disk, parse it in to json and then send it off for processing. </summary>
			/// <param name="fileName"> The file path and file name. </param>
			void loadFile( const std::string& fileName );

			/// <summary> Read a text file from disk. </summary>
			/// <param name="fileName"> The file path and file name. </param>
			/// <returns> The total file contents. </returns>
			std::string readFileContents( const std::string& fileName );

		private:
			/// <summary>
			/// A functor that will schedule another functor to execute.
			/// We are going to send the file loading commands to the executor in a controlled manner.
			/// </summary>
			Scheduler scheduler;

			/// <summary>
			/// The parsers will get the json data and then perform some action
			/// based on the contents of the json file.
			/// </summary>
			typedef std::unordered_map< std::string, JsonParserSPtr > ParserMap;
			ParserMap parsers;
	};
} // namespace tools

#endif // _FILE_READER_H