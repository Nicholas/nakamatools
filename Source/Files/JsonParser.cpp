#include "JsonParser.h"
// Interface for JSON parsers that will
// be called when we need to parse some json.
//
//
// Project   : NaKama-Tools
// File Name : JsonParser.cpp
// Date      : 26/02/2018
// Author    : Nicholas Welters

#include <cjson.h>

namespace tools
{
	using std::cout;

	/// <summary> ctor </summary>
	/// <param name="scheduler"> A functor that will schedule another functor to execute. </param>
	JsonParser::JsonParser( const Scheduler& schedule )
		: schedule( schedule )
	{ }

	/// <summary> Schedules the parsing of the json object. </summary>
	/// <param name="contents"> The string data to parse in to json. </param>
	void JsonParser::parse(const std::string& contents)
	{
		///////////////////////////////////////////////////////////////////
		// 1) Convert to JSON format
		//std::unique< cJSON, decltype( &cJSON_Delete ) >
		//std::unique< cJSON, std::function< void ( cJSON* ) > >
		cJSONSPtr pRoot = std::shared_ptr< cJSON >(
			cJSON_Parse( contents.c_str( ) ),
			[ ]( cJSON* pRoot )
			{
				if( pRoot )
				{
					cJSON_Delete( pRoot );
				}
			}
		);

		if( pRoot == nullptr )
		{
			const char *error_ptr = cJSON_GetErrorPtr( );
			if( error_ptr != nullptr )
			{
				cout << "Error before: " << error_ptr << "\n";
			}

			return;
		}

		///////////////////////////////////////////////////////////////////
		// 2) Read Metadata
		cJSON* metaData = cJSON_GetObjectItem( pRoot.get( ), "meta" );

		if( !cJSON_IsObject( metaData ) )
		{
			cout << "Unable read the meta data section.\n";
			return;
		}

		// 4) Send data to data parser
		schedule( [ this, pRoot ]( ) -> void
		{
			parseJSON( pRoot );
		} );
	}

	/// <summary> The parsing function that will interpret the json data. </summary>
	/// <param name="pRoot"> The json object root node. </param>
	void JsonParser::parseJSON(const cJSONSPtr& pRoot )
	{
		(void)( pRoot );
	}
} // namespace tools