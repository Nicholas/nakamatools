#pragma once
// Interface for JSON parsers that will
// be called when we need to parse some json.
//
//
// Project   : NaKama-Tools
// File Name : JsonParser.h
// Date      : 26/02/2018
// Author    : Nicholas Welters

#ifndef _JSON_PARSER_H
#define _JSON_PARSER_H

#include "Macros.h"

#include <functional>
#include <memory>

FORWARD_DECLARE_STRUCT( cJSON );

namespace tools
{
	typedef std::function< void( const std::function< void( ) >& ) > Scheduler;

	/// <summary> 
	/// Interface for JSON parsers that will
	/// be called when we need to parse some json.
	/// </summary>
	class JsonParser
	{
		public:
			/// <summary> ctor </summary>
			/// <param name="scheduler"> A functor that will schedule another functor to execute. </param>
			JsonParser( const Scheduler& schedule );

			/// <summary> dtor </summary>
			virtual ~JsonParser( ) = default;

			/// <summary> Schedules the parsing of the json object. </summary>
			/// <param name="contents"> The string data to parse in to json. </param>
			void parse( const std::string& contents );

		private:
			/// <summary> The parsing function that will interpret the json data. </summary>
			/// <param name="pRoot"> The json object root node. </param>
			virtual void parseJSON( const cJSONSPtr& pRoot ) = 0;

		private:
			/// <summary>
			/// A functor that will schedule another functor to execute.
			/// We are going to send the file loading commands to the executor in a controlled manner.
			/// </summary>
			Scheduler schedule;

	};
} // namespace tools

# endif // _JSON_PARSER_H