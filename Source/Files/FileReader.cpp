#include "FileReader.h"
// Read the contents of a text file and then
// pass the contents to a data parser.
//
//
// Project   : NaKama-Tools
// File Name : FileReader.cpp
// Date      : 26/02/2018
// Author    : Nicholas Welters

#include "JsonParser.h"

#include <iostream>
#include <fstream>
#include <filesystem>

namespace tools
{
	using std::cout;
	using std::ifstream;
	using std::string;

	using std::experimental::filesystem::path;
	using std::experimental::filesystem::exists;

	/// <summary> ctor </summary>
	/// <param name="scheduler"> A functor that will schedule another functor to execute. </param>
	FileReader::FileReader(const Scheduler& scheduler )
		: scheduler(scheduler)
		, parsers( )
	{ }

	/// <summary> Add a json parser that will parse the contents of a json file based on its meta data type. </summary>
	/// <param name="type"> The parsers unique data type to parse. </param>
	/// <param name="pParser"> A json parser that will process the file contents. </param>
	void FileReader::add( const std::string& type, const JsonParserSPtr& pParser )
	{
		//todo: Check if one already exists.
		parsers[ type ] = pParser;
	}

	/// <summary> Read a text file from disk and then process the file. </summary>
	/// <param name="fileName"> The file path and file name. </param>
	void FileReader::readFile(const string& fileName)
	{
		scheduler( [ this, fileName ]( ) -> void
		{
			loadFile( fileName );
		} );
	}


	/// <summary> Read a text file from disk. </summary>
	/// <param name="fileName"> The file path and file name. </param>
	/// <returns> The total file contents. </returns>
	void FileReader::loadFile( const std::string& fileName )
	{
		path filePath( fileName );

		if( !exists( filePath ) )
		{
			return;
		}

		string text = readFileContents( fileName );

		if( text == "" )
		{
			return; 
		}

		ParserMap::iterator search = parsers.find( filePath.extension( ).generic_string( ) );

		if( search != parsers.end( ) )
		{
			search->second->parse( text );
		}
	}

	/// <summary> Read a text file from disk, parse it in to json and then send it off for processing. </summary>
	/// <param name="fileName"> The file path and file name. </param>
	string FileReader::readFileContents(const string& fileName)
	{
		std::streampos size;
		char* memblock;

		string line = "";

		ifstream file( fileName );

		if( file.is_open() )
		{
			file.seekg( 0, std::ios::end );
			size = file.tellg();
			file.seekg( 0, std::ios::beg );

			memblock = new char[ size ];

			file.read( memblock, size );
			file.close();

			line = string( memblock, size );

			delete[] memblock;
		}
		else
		{
			cout << "Unable to open file.\n";
		}

		return line;
	}
} // namespace tools