#pragma once
// A collection for some useful preprocessor macros
// as well as some useful template tools
//
// Project   : NaKama-Tools
// File Name : Macros.h
// Date      : 20/11/2017
// Author    : Nicholas Welters

#ifndef _MACROS_H
#define _MACROS_H

// Concatenate preprocessor tokens A and B without expanding macro definitions
// (however, if invoked from a macro, macro arguments are expanded).
#define PPCAT_NX(A, B) A ## B

// Concatenate preprocessor tokens A and B after macro-expanding them.
#define PPCAT(A, B) PPCAT_NX(A, B)

// Create a name with Ptr appended to the end.
#define POINTER_TYPE_NAME( TYPE ) PPCAT( TYPE, Ptr )

// Prototype a class name
#define PROTOTYPE_CLASS( TYPE ) class TYPE;

// Prototype a struct name
#define PROTOTYPE_STRUCT( TYPE ) struct TYPE;

// Define a shared pointer type name.
// typedef std::shared_ptr< Type > TypeSPtr
#define SHARED_PTR_TYPE_DEF( TYPE ) typedef ::std::shared_ptr< TYPE > POINTER_TYPE_NAME( PPCAT( TYPE, S ) );

// Define a weak pointer type name.
// typedef std::weak_ptr< Type > TypeWPtr
#define WEAK_PTR_TYPE_DEF( TYPE ) typedef ::std::weak_ptr< TYPE > POINTER_TYPE_NAME( PPCAT( TYPE, W ) );

// Define a unique pointer type name.
// typedef std::unique_ptr< Type > TypeUPtr
#define UNIQUE_PTR_TYPE_DEF( TYPE ) typedef ::std::unique_ptr< TYPE > POINTER_TYPE_NAME( PPCAT( TYPE, U ) );

/// Prototype a class with all its smart pointer types.
#define FORWARD_DECLARE( TYPE )\
	    PROTOTYPE_CLASS( TYPE )\
	SHARED_PTR_TYPE_DEF( TYPE )\
	  WEAK_PTR_TYPE_DEF( TYPE )\
	UNIQUE_PTR_TYPE_DEF( TYPE )

/// Prototype a struct with all its smart pointer types.
#define FORWARD_DECLARE_STRUCT( TYPE )\
              PROTOTYPE_STRUCT( TYPE )\
           SHARED_PTR_TYPE_DEF( TYPE )\
             WEAK_PTR_TYPE_DEF( TYPE )\
           UNIQUE_PTR_TYPE_DEF( TYPE )

// Some useful pointer deletion macros
#define SAFE_DELETE( p )       if( p ){ delete p;      p = nullptr; }
#define SAFE_DELETE_ARRAY( p ) if( p ){ delete[] p;    p = nullptr; }
#define SAFE_RELEASE( p )      if( p ){ p->Release( ); p = nullptr; }

// Self check macros (copy and move quick exits)
#define IF_THIS_RETURN( o ) if( this == &o ){ return; }

#include <limits>
#include <random>

namespace tools
{
	/// <summary> Provide some useful raw array tools. </summary>
	class Array
	{
		public:
			/// <summary> Provide the size of an array using template magic. </summary>
			/// <template name="T"> The type of the array. </template>
			/// <template name="S"> The size of the array, this is automatically determined at compile time. </template>
			/// <param name="v"> The array that we want to get the size of. </param>
			/// <returns> The array size. </returns>
			template <typename T, unsigned S>
			static inline unsigned size( const T( &v )[ S ] )
			{
				return S;
			}
	};


	inline bool almostEqual( double lhs, double rhs, int ulp = 2 )
	{
		// the machine epsilon has to be scaled to the magnitude of the values used
		// and multiplied by the desired precision in ULPs (units in the last place)
		return abs( lhs - rhs ) < ::std::numeric_limits< double >::epsilon( ) * abs( lhs + rhs ) * ulp
			// unless the result is subnormal
			|| abs( lhs - rhs ) < ::std::numeric_limits< double >::min( );
	}

	inline bool almostEqual( float lhs, double rhs, int ulp = 2 )
	{
		// the machine epsilon has to be scaled to the magnitude of the values used
		// and multiplied by the desired precision in ULPs (units in the last place)
		return abs( lhs - rhs ) < ::std::numeric_limits< double >::epsilon( ) * abs( lhs + rhs ) * ulp
			// unless the result is subnormal
			|| abs( lhs - rhs ) < ::std::numeric_limits< double >::min( );
	}

	inline float RandF( float min, float max )
	{
		return ( static_cast< float >( rand( ) ) / RAND_MAX ) * ( max - min ) + min;
	}
}

#endif // _MACROS_H
