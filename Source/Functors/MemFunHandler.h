#pragma once
////////////////////////////////////////////////////////////////////////////////
// The Loki Library
// Copyright (c) 2001 by Andrei Alexandrescu
// This code accompanies the book:
// Alexandrescu, Andrei. "Modern C++ Design: Generic Programming and Design
//     Patterns Applied". Copyright (c) 2001. Addison-Wesley.
// Permission to use, copy, modify, distribute and sell this software for any
//     purpose is hereby granted without fee, provided that the above copyright
//     notice appear in all copies and that both that copyright notice and this
//     permission notice appear in supporting documentation.
// The author or Addison-Wesley Longman make no representations about the
//     suitability of this software for any purpose. It is provided "as is"
//     without express or implied warranty.
////////////////////////////////////////////////////////////////////////////////
//
// This concrete functor helper class is used to handle pointers to member functions.

// Changed to use variadic templates.
//
// "Although not very common in day-to-day programming activities,
// pointer to member functions can sometimes be of help" - Now I have
// found this not to be the case. I tend to use them the most and
// resort to lambdas for more complex groupings that have not
// been placed in a class... yet.
//
// "If the result of .* or ->* is a function, then that result can
// be used only as the operand for the function call to operator( )."
//
// Project   : NaKama-Tools
// File Name : MemFunHandler.h
// Date      : 03/11/2013
// Author    : Nicholas Welters

#ifndef _MEM_FUN_HANDLER_H
#define _MEM_FUN_HANDLER_H

#include "FunctorImpl.h"

namespace tools
{
	/// <summary> This concrete functor helper class is used to handle pointers to member functions. </summary>
	/// <template name="ParentFunctor">
	/// The functor type that holds this, its used to provide the return type
	/// (I would have liked to get the arguments list as well but that didn't work out 
	/// so this should just be changed to the return type).
	/// </template>
	/// <template name="ObjectPtr"> The Object type that we are going to call a function from. </template>
	/// <template name="MemberFunctionPtr"> The function type that we are going to call. </template>
	/// <template name="Arguments"> The list of arguments that the functor will take to execute. </template>
	template< class ParentFunctor, typename ObjectPtr, typename MemberFunctionPtr, typename... Arguments >
	class MemFunHandler : public FunctorImpl< typename ParentFunctor::Return, Arguments... >
	{
		public:
			/// <summary> The return type, collected from the parent functor type. </summary>
			typedef typename ParentFunctor::Return Return;

		public:
			/// <summary> ctor. </summary>
			/// <param name="pObject"> The object that we are going to call the function on. </param>
			/// <param name="pFunction"> The function we are going to call from the object. </param>
			MemFunHandler( const ObjectPtr& pObject, MemberFunctionPtr pFunction )
				: FunctorImpl< Return, Arguments... >( )
				, pObject( pObject )
				, pFunction( pFunction )
			{ }

			/// <summary> dtor. </summary>
			virtual ~MemFunHandler( ) = default;

			/// <summary> Forwards the request to the stored object member and it function variables. </summary>
			/// <param name="arguments"> The arguments that we have defined with the Arguments template parameter. </param>
			/// <returns> What ever we wanted based on our first template argument. </returns>
			virtual Return operator( )( Arguments&&... arguments ) final override
			{
				return ( ( *pObject ).*pFunction )( ::std::forward< Arguments >( arguments )... );
			}

			/// <summary> 
			/// Allow us to create a polymorphic copy.
			/// Takes advantage of covariant return types.
			/// </summary>
			/// <returns> A copy of the member functor handler. </returns>
			virtual MemFunHandler* clone( ) const final override
			{
				return new MemFunHandler( *this );
			}

		private:
			/// <summary> The state the we want to execute the function on. </summary>
			ObjectPtr pObject;

			/// <summary> The function we want to run later. </summary>
			MemberFunctionPtr pFunction;
	};
} // namespace tools

#endif // _MEM_FUN_HANDLER_H
