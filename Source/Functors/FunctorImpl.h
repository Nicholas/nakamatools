#pragma once
////////////////////////////////////////////////////////////////////////////////
// The Loki Library
// Copyright (c) 2001 by Andrei Alexandrescu
// This code accompanies the book:
// Alexandrescu, Andrei. "Modern C++ Design: Generic Programming and Design
//     Patterns Applied". Copyright (c) 2001. Addison-Wesley.
// Permission to use, copy, modify, distribute and sell this software for any
//     purpose is hereby granted without fee, provided that the above copyright
//     notice appear in all copies and that both that copyright notice and this
//     permission notice appear in supporting documentation.
// The author or Addison-Wesley Longman make no representations about the
//     suitability of this software for any purpose. It is provided "as is"
//     without express or implied warranty.
////////////////////////////////////////////////////////////////////////////////
//
// Defines a polymorphic interface that abstracts a function call.
//
// Using variadic templates so that we don't have to use the
// fun recursive preprocessor type lists.
//
// Using variadic templates does help a lot, we only have to define one polymorphic
// type rather than a type for each functor that has a different number of arguments.
// We are also saved of having to make an arbitrary decision for the number of arguments
// that is going to be supported.
//
// Project   : NaKama-Tools
// File Name : FunctorImpl.h
// Date      : 03/11/2013
// Author    : Nicholas Welters

#ifndef _FUNCTOR_IMPL_H
#define _FUNCTOR_IMPL_H

namespace tools
{
	/// <summary> A polymorphic interface that abstracts a function call. </summary>
	/// <template name="ReturnType"> The type name for our functor will return. </template>
	/// <template name="Arguments"> The list of arguments that the functor will take to execute. </template>
	template< class ReturnType, typename... Arguments >
	class FunctorImpl
	{
		public:
			/// <summary> dtor. </summary>
			virtual ~FunctorImpl( ) = default;

			/// <summary>
			/// The functors core,
			/// this is used to execute an arbitrary function
			/// with an arbitrary number of arguments with
			/// some arbitrary return type.
			/// </summary>
			/// <param name="arguments"> The arguments that we have defined with the Arguments template parameter. </param>
			/// <returns> What ever we wanted based on our first template argument. </returns>
			virtual ReturnType operator( )( Arguments&&... arguments ) = 0;

			/// <summary> 
			/// Allow us to create a polymorphic copy.
			/// Takes advantage of covariant return types.
			/// </summary>
			/// <returns>
			/// A copy of the functor type.
			/// We can return a pointer to a derived type
			/// instead of a pointer to the base class from
			/// an overridden virtual function
			/// </returns>
			virtual FunctorImpl* clone( ) const = 0;

	};
} // namespace tools

#endif // _FUNCTOR_IMPL_H
