#pragma once
////////////////////////////////////////////////////////////////////////////////
// The Loki Library
// Copyright (c) 2001 by Andrei Alexandrescu
// This code accompanies the book:
// Alexandrescu, Andrei. "Modern C++ Design: Generic Programming and Design
//     Patterns Applied". Copyright (c) 2001. Addison-Wesley.
// Permission to use, copy, modify, distribute and sell this software for any
//     purpose is hereby granted without fee, provided that the above copyright
//     notice appear in all copies and that both that copyright notice and this
//     permission notice appear in supporting documentation.
// The author or Addison-Wesley Longman make no representations about the
//     suitability of this software for any purpose. It is provided "as is"
//     without express or implied warranty.
////////////////////////////////////////////////////////////////////////////////
//
// This concrete functor helper class is used to handle functors.
//
// Changed to use variadic templates.
//
// Dealing with overloaded functions
// typedef ReturnType (*TpFun)( Arguments... )
// TpFun function = OverloadedFunctionName;
// static_cast< TpFun >( OverloadedFunctionName );
// This lets us resolve the function type when the name is no longer good enough.
//
// Project   : NaKama-Tools
// File Name : FunctorHandler.h
// Date      : 03/11/2013
// Author    : Nicholas Welters

#ifndef _FUNCTOR_HANDLER_H
#define _FUNCTOR_HANDLER_H

#include "FunctorImpl.h"

namespace tools
{
	/// <summary> This functor helper class is used to handle functors. </summary>
	/// <template name="ParentFunctor">
	/// The functor type that holds this, its used to provide the return type
	/// (I would have liked to get the arguments list as well but that didn't work out 
	/// so this should just be changed to the return type).
	/// </template>
	/// <template name="FunctorType"> The type name for our functor will return. </template>
	/// <template name="Arguments"> The list of arguments that the functor will take to execute. </template>
	template< class ParentFunctor, typename FunctorType, typename... Arguments >
	class FunctorHandler : public FunctorImpl< typename ParentFunctor::Return, Arguments... >
	{
		public:
			/// <summary> The return type, collected from the parent functor type. </summary>
			typedef typename ParentFunctor::Return Return;

		public:
			/// <summary> ctor. </summary>
			/// <param name="functor"> The functor that we are going the hold and forward requests too. </param>
			FunctorHandler( const FunctorType& functor )
				: FunctorImpl< Return, Arguments... >( )
				, functor( functor )
			{ }

			/// <summary> dtor. </summary>
			virtual ~FunctorHandler( ) = default;

			/// <summary> Forwards the request to the stored member variable. </summary>
			/// <param name="arguments"> The arguments that we have defined with the Arguments template parameter. </param>
			/// <returns> What ever we wanted based on our first template argument. </returns>
			virtual Return operator( )( Arguments&&... arguments ) final override
			{
				return functor( ::std::forward< Arguments >( arguments )... );
			}

			/// <summary> 
			/// Allow us to create a polymorphic copy.
			/// Takes advantage of covariant return types.
			/// </summary>
			/// <returns> A copy of the functor handler. </returns>
			virtual FunctorHandler* clone( ) const final override
			{
				return new FunctorHandler( *this );
			}

		private:
			/// <summary> The handled functor. </summary>
			FunctorType functor;
	};
} // namespace tools

#endif // _FUNCTOR_HANDLER_H
