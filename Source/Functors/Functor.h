#pragma once
////////////////////////////////////////////////////////////////////////////////
// The Loki Library
// Copyright (c) 2001 by Andrei Alexandrescu
// This code accompanies the book:
// Alexandrescu, Andrei. "Modern C++ Design: Generic Programming and Design
//     Patterns Applied". Copyright (c) 2001. Addison-Wesley.
// Permission to use, copy, modify, distribute and sell this software for any
//     purpose is hereby granted without fee, provided that the above copyright
//     notice appear in all copies and that both that copyright notice and this
//     permission notice appear in supporting documentation.
// The author or Addison-Wesley Longman make no representations about the
//     suitability of this software for any purpose. It is provided "as is"
//     without express or implied warranty.
////////////////////////////////////////////////////////////////////////////////
//
// The Generalized Functor Scaffold.

// Changed to use variadic templates.
//
// Allows us to deal with variety functors in a consistent way.
//
// Project   : NaKama-Tools
// File Name : Functor.h
// Date      : 03/11/2013
// Author    : Nicholas Welters

#ifndef _FUNCTOR_H
#define _FUNCTOR_H

#include "FunctorImpl.h"
#include "FunctorHandler.h"
#include "MemFunHandler.h"
#include <memory>

namespace tools
{
	/// <summary> The generalized functor scaffold. </summary>
	/// <template name="ReturnType"> The type name for our functor will return. </template>
	/// <template name="Arguments"> The list of arguments that the functor will take to execute. </template>
	template< class ReturnType, typename... Arguments >
	class Functor
	{
		public:
			/// <summary> An internal typedef of the return type so that we can collect the template types. </summary>
			typedef ReturnType Return;

			/// <summary> The typedef of the internal functor implementation polymorphic type. </summary>
			typedef FunctorImpl< Return, Arguments... > Implementation;

		public:
			/// <summary> ctor. </summary>
			Functor( )
				: pImplementation( nullptr )
			{ }

			/// <summary> copy ctor. </summary>
			/// <param name="original"> The functor to make a copy of. </param>
			Functor( const Functor& original )
				: pImplementation( original.pImplementation->clone( ) )
			{ }

			/// <summary> 
			/// Extension ctor. Allows for classes derived
			/// from FunctorImpl to be used to initialize
			/// Functor directly.
			/// This is explicit so that the user is aware
			/// that this takes ownership of the pointer.
			/// </summary>
			/// <param name="pImplementation"> The functor implementation to take ownership of. </param>
			explicit Functor( ::std::unique_ptr< Implementation > pImplementation )
				: pImplementation( ::std::move( pImplementation ) )
			{ }

			/// <summary>
			/// Functors ctor that accepts a functor (a class that defines operator( )).
			/// Uses functor handler to hold our functor using copy semantics.
			/// </summary>
			/// <template name="FunctorType"> The functor type that we will accept. </template>
			/// <param name="functor"> The functor that will be wrapped to be handled consistently. </param>
			template< class FunctorType >
			Functor( const FunctorType & functor )
				: pImplementation( new FunctorHandler< Functor, typename ::std::decay< FunctorType >::type, Arguments... >( functor ) )
			{ }

			/// <summary>
			/// Functors ctor to handle pointers to member functions.
			/// </summary>
			/// <template name="ObjectPtr"> The Object type that we are going to call a function from. </template>
			/// <template name="MemberFunctionPtr"> The function type that we are going to call. </template>
			/// <param name="functor"> The functor that will be wrapped to be handled consistently. </param>
			template< class ObjectPtr, class MemberFunctionPtr >
			Functor( const ObjectPtr& pObject, const MemberFunctionPtr & pFunction )
				: pImplementation( new MemFunHandler< Functor, ObjectPtr, MemberFunctionPtr, Arguments... >( pObject, pFunction ) )
			{ }

			/// <summary> dtor. </summary>
			virtual ~Functor( ) = default;

			/// <summary> Forwards the request to the stored functor implementation variable. </summary>
			/// <param name="arguments"> The arguments that we have defined with the Arguments template parameter. </param>
			/// <returns> What ever we wanted based on our first template argument. </returns>
			Return operator( )( Arguments&&... arguments )
			{
				return ( ( *pImplementation )( ::std::forward< Arguments >( arguments )... ) );
			}

			/// <summary> copy assignment operator. </summary>
			/// <param name="functor"> The functor to make a copy of. </param>
			Functor& operator=( const Functor& rhs )
			{
				pImplementation = ::std::unique_ptr< Implementation >( rhs.pImplementation->clone( ) );
				return *this;
			}

			/// <summary> Equivalent operator. </summary>
			/// <param name="functor"> The functor to check to see if its the same. </param>
			bool operator==( const Functor& rhs )
			{
				return pImplementation == rhs.pImplementation;
			}

		private:
			/// <summary> The owned functor implementation. </summary>
			::std::unique_ptr< Implementation > pImplementation;
	};
} // namespace tools

#endif // FUNCTOR_HPP_
