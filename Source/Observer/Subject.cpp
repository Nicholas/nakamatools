
// This is a very basic subject interface that
// we can use to implement an observer pattern.
//
// Project   : NaKama-Tools
// File Name : Subject.cpp
// Date      : 23/11/2013
// Author    : Nicholas Welters


#include "Subject.h"
#include "Observer.h"

namespace tools
{
	using ::std::list;

	/// <summary> ctor </summary>
	Subject::Subject( )
		: observers( )
	{
	}

	/// <summary> Attach an observer to this subject. </summary>
	/// <param name="pObserver"> A const pointer to an observer. </param>
	void Subject::attach( Observer* const pObserver )
	{
		observers.push_back( pObserver );
	}

	/// <summary> Remove an observer to this subject. </summary>
	/// <param name="pObserver"> A const pointer to an observer. </param>
	void Subject::detach( Observer* const pObserver )
	{
		observers.remove( pObserver );
	}
		
	/// <summary> Signal the observers that there has been a (very generic) change in the subject. </summary>
	void Subject::notify( )
	{
		for( Observer* observer: observers )
		{
			observer->notified( );
		}
	}
} // namespace tools
