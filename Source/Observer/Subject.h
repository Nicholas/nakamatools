#pragma once
// This is a very basic subject interface that
// we can use to implement an observer pattern.
//
// Project   : NaKama-Tools
// File Name : Subject.h
// Date      : 23/11/2013
// Author    : Nicholas Welters


#ifndef _SUBJECT_H
#define _SUBJECT_H

#include <list>

namespace tools
{
	class Observer;

	/// <summary>
	/// This is a very basic observer interface that
	/// we can use to implement an observer pattern.
	/// </summary>
	// TODO: Create policies for thread safety.
	class Subject
	{
		public:
			/// <summary> ctor </summary>
			Subject( );

			/// <summary> dtor </summary>
			virtual ~Subject( ) = default;

			/// <summary> Attach an observer to this subject. </summary>
			/// <param name="pObserver"> A const pointer to an observer. </param>
			virtual void attach( Observer* pObserver );

			/// <summary> Remove an observer to this subject. </summary>
			/// <param name="pObserver"> A const pointer to an observer. </param>
			virtual void detach( Observer* const pObserver );

			/// <summary> Signal the observers that there has been a (very generic) change in the subject. </summary>
			virtual void notify( );

		private:
			/// <summary> The  list of observers to this subject. </summary>
			::std::list< Observer* > observers;
	};
} // namespace tools

#endif // _SUBJECT_H
