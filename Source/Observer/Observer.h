#pragma once
// This is a very basic observer interface that
// we can use to implement an observer pattern.
//
// Project   : NaKama-Tools
// File Name : Observer.h
// Date      : 23/11/2013
// Author    : Nicholas Welters


#if !defined(_OBSERVER_H)
#define _OBSERVER_H

namespace tools
{
	/// <summary>
	/// This is a very basic observer interface that
	/// we can use to implement an observer pattern.
	/// </summary>
	class Observer
	{
		public:
			/// <summary> dtor </summary>
			virtual ~Observer( ) = default;

			/// <summary> Update the observer state when notified by a subject. </summary>
			virtual void notified( ) = 0;
	};
} // namespace tools

#endif // _OBSERVER_H
