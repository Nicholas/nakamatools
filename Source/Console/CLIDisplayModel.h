#pragma once
// Manages a set of events that we can select using a string value.
// This is supposed to be used by the console output system as we have
// a user readable string value assigned to each event that gets executed.
//
//
// Project   : NaKama-Tools
// File Name : DisplayModel.h
// Date      : 23/11/2013
// Author    : Nicholas Welters


#ifndef _CLI_DISPLAY_MODEL_H
#define _CLI_DISPLAY_MODEL_H

#include "Macros.h"
#include "Observer/Subject.h"

#include <exception>
#include <memory>
#include <map>
#include <vector>

namespace tools
{
	namespace display
	{
		FORWARD_DECLARE( CLIDisplayFunctor );

		/// <summary>
		/// Manages a set of events that we can select using a string value.
		/// This is supposed to be used by the console output system as we have
		/// a user readable string value assigned to each event that gets executed.
		/// </summary>
		class CLIDisplayModel : public Subject
		{
			public:
				typedef ::std::map< ::std::string, CLIDisplayFunctorSPtr > FunctionMap;
				typedef ::std::shared_ptr< FunctionMap > FunctionMapSPtr;

			public:
				/// <summary> default ctor </summary>
				CLIDisplayModel( );

				/// <summary> dtor </summary>
				virtual ~CLIDisplayModel( );

				/// <summary> Get human readable description for this cli options model. </summary>
				/// <returns> The models description. </returns>
				::std::string getText( );

				/// <summary> Get human readable description for a selected cli option. </summary>
				/// <param name="key"> The key to the functor  we want a description for. </param>
				/// <returns> The options description. </returns>
				::std::string getText( const ::std::string& key );

				/// <summary> Set human readable description for a selected cli option. </summary>
				/// <param name="text"> The models description. </param>
				void setText( const ::std::string& text );

				/// <summary> Set the cli options for the model. </summary>
				/// <param name="functions"> The models cli options & function map. </param>
				void setFunctions( const FunctionMapSPtr& functions );

				/// <summary> Set the human readable description & options for the model. </summary>
				/// <param name="functions"> The models cli options & function map. </param>
				/// <param name="text"> The models description. </param>
				void setValue( const FunctionMapSPtr& functions, const ::std::string& text );

				/// <summary> Get the keys used to identify the functions in the function hash map. </summary>
				/// <returns> A cached vector of all the keys in the functions map. </returns>
				const ::std::vector< ::std::string >& getKeys( ) const;

				/// <summary> Check if a function exists with a specific key. </summary>
				/// <param name="key"> The key to the functor we want to validate. </param>
				bool contains( const ::std::string& key ) const;

				/// <summary> Execute a function in the model that is associated with the provided key. </summary>
				/// <param name="key"> The key to the functor we will execute. </param>
				void trigger( const ::std::string& key ) const;

				/// <summary> 
				/// Signal the observers that there has been a (very generic) change in the subject.
				/// We have the opportunity to update our own cached state at the same time.
				///</summary>
				virtual void notify( ) final override;

			private:
				/// <summary> The human readable description for this cli options model. </summary>
				::std::string text;

				/// <summary> The models cli options & function map. </summary>
				FunctionMapSPtr pFunctions;

				/// <summary> Cashed Values </summary>
				::std::vector< ::std::string > keys;

			public:
				/// <summary> A specialised exception for our CLI that we can check for. </summary>
				class CLIDisplayException: public std::exception
				{
					public:
						/// <summary> ctor </summary>
						/// <param name="unknownID"> The key that we tried to use that doesn't exist. </param>
						explicit CLIDisplayException( const ::std::string & unknownID )
							: unknownID( unknownID )
						{ }

						/// <summary> Get the human readable string representing the error. </summary>
						virtual const char* what( )
						{
							return "Unknown CLI Display Functor Identifier";
						}

						/// <summary> Get key that we tried to use that doesn't exist. </summary>
						const ::std::string& getIdentifier( )
						{
							return unknownID;
						}

					private:
						/// <summary> The key that we tried to use that doesn't exist. </summary>
						::std::string unknownID;
				};
		};
	} // namespace display
} // namespace tools

#endif // _CLI_DISPLAY_MODEL_H
