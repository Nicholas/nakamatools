
// Create a console based application that will display
// some options for us that we can look through.
//
// Project   : NaKama-Tools
// File Name : CLIDisplay.h
// Date      : 23/11/2013
// Author    : Nicholas Welters

#include "CLIDisplay.h"
#include "CLIDisplayModel.h"

namespace tools
{
	namespace display
	{
		using std::string;
		using std::ostream;
		using std::istream;
		using std::endl;
		using std::getline;

		/// <summary> ctor </summary>
		/// <param name="model"> Defines the options to display and provides a way to execute the selected option. </param>
		/// <param name="oStream"> The display classes output stream. </param>
		/// <param name="iStream"> The display classes input stream. </param>
		CLIDisplay::CLIDisplay( const CLIDisplayModelSPtr& pModel, ostream& oStream, istream& iStream )
			: running( false )
			, pModel( pModel )
			, oStream( oStream )
			, iStream( iStream )
		{ }
		
		/// <summary> dtor </summary>
		CLIDisplay::~CLIDisplay( )
		{ }

		/// <summary> Update the observer state when notified by a subject. </summary>
		void CLIDisplay::notified( )
		{ }

		/// <summary> Runs the CLI display. Provides the user with options and then responds to the users selection. </summary>
		void CLIDisplay::display( )
		{
			running = true;
			string userInput = "";

			while( running )
			{
				oStream << pModel->getText( ) << endl;
				oStream << endl;
				oStream << "|     |::|" << endl;

				for( const string& key: pModel->getKeys( ) )
				{
					oStream << "|> " << key << " <|::|> " << pModel->getText( key ) << endl;
				}

				oStream << "|     |::|" << endl;
				oStream << endl;
				oStream << "Please enter selection: ";

				getline( iStream, userInput );

				while( !pModel->contains( userInput ) && running )
				{
					oStream << "Unknown command '" << userInput << "'." << endl;

					oStream << "Please enter selection: ";
					getline( iStream, userInput );

					if( userInput == "altF4" )
					{
						return;
					}
				}

				pModel->trigger( userInput );
			}
		}

		/// <summary> Sets the object to exit the display function so that we can shutdown the application. </summary>
		void CLIDisplay::shutdown( )
		{
			running = false;
		}
	} // namespace display
} // namespace tools
