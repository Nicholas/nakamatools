#pragma once
// A functor with a string definition that is human readable.
//
//
// Project   : NaKama-Tools
// File Name : DisplayFunctor.h
// Date      : 23/11/2013
// Author    : Nicholas Welters

#ifndef _CLI_DISPLAY_FUNCTOR_H
#define _CLI_DISPLAY_FUNCTOR_H

#include "Functors/Functor.h"

#include <string>

namespace tools
{
	namespace display
	{
		/// <summary>
		/// A functor with a string definition that is human readable
		/// </summary>
		class CLIDisplayFunctor : public Functor< void >
		{
			public:
				/// <summary> default ctor - Its has explicitly been removed </summary>
				CLIDisplayFunctor( ) = delete;

				/// <summary> copy ctor </summary>
				/// <param name="original"> The object state to copy. </param>
				CLIDisplayFunctor( const CLIDisplayFunctor& original );

				/// <summary> ctor </summary>
				/// <template name="FunctorType"> The functor type that will be executed. </template>
				/// <param name="text"> The human readable name for the functor. </param>
				/// <param name="functor"> The functor that will be executed. </param>
				template< class FunctorType >
				CLIDisplayFunctor( const ::std::string& text, const FunctorType& functor )
					: Functor( functor )
					, text( text )
				{ }

				/// <summary> ctor </summary>
				/// <template name="PrtObj"> Object type that we will execute a function from. </template>
				/// <template name="MemFn"> The member function type belonging to the PtrObj class. </template>
				/// <param name="text"> The human readable name for the functor. </param>
				/// <param name="p"> A pointer to the object we are going to execute a function from. </param>
				/// <param name="memFn"> The member function that we will execute from our pointer. </param>
				template< class PrtObj, class MemFn >
				CLIDisplayFunctor( const ::std::string& text, const PrtObj& p, const MemFn& memFn )
					: Functor( p, memFn )
					, text( text )
				{ }

				/// <summary> dtor </summary>
				virtual ~CLIDisplayFunctor( ) = default;

				/// <summary> Get the functors name. </summary>
				/// <returns> The functors human readable name. </returns>
				::std::string getText( ) const;

				/// <summary> Set the functors human readable name. </summary>
				/// <param name="text"> The human readable name for the functor. </param>
				void setText( const ::std::string& text );

			private:
				/// <summary> The human readable name for the functor. </summary>
				::std::string text;
		};
	} // namespace display
} // namespace tools

#endif // _CLI_DISPLAY_FUNCTOR_H
