#pragma once
// Create a console based application that will display
// some options for us that we can look through.
//
//
// Project   : NaKama-Tools
// File Name : CLIDisplay.h
// Date      : 23/11/2013
// Author    : Nicholas Welters

#ifndef _CLI_DISPLAY_H
#define _CLI_DISPLAY_H

#include "Macros.h"
#include "Observer/Observer.h"

#include <memory>
#include <iostream>

namespace tools
{
	namespace display
	{
		FORWARD_DECLARE( CLIDisplayModel );

		/// <summary>
		/// Create a console based application that will display
		/// some options for us that we can look through.
		/// </summary>
		class CLIDisplay : public Observer
		{
			public:
				/// <summary> ctor </summary>
				/// <param name="model"> Defines the options to display and provides a way to execute the selected option. </param>
				/// <param name="oStream"> The display classes output stream (tends to be cout). </param>
				/// <param name="iStream"> The display classes input stream (tends to be cin). </param>
				explicit CLIDisplay( const CLIDisplayModelSPtr& model, ::std::ostream& oStream = ::std::cout, ::std::istream& iStream = ::std::cin );

				/// <summary> dtor </summary>
				virtual ~CLIDisplay( );

				/// <summary> Update the observer state when notified by a subject. </summary>
				virtual void notified( ) final override;

				/// <summary> Runs the CLI display. Provides the user with options and then responds to the users selection. </summary>
				void display( );

				/// <summary> Sets the object to exit the display function so that we can shutdown the application. </summary>
				void shutdown( );

			private:
				/// <summary> Controls the display loop and when to exit the display function. </summary>
				bool running;

				/// <summary> Defines the options to display and provides a way to execute the selected option. </summary>
				CLIDisplayModelSPtr pModel;

				/// <summary> The display classes output stream (tends to be cout). </summary>
				::std::ostream& oStream;

				/// <summary> The display classes input stream (tends to be cin). </summary>
				::std::istream& iStream;
		};
	} // namespace display
} // namespace tools

#endif // _CLI_DISPLAY_H
