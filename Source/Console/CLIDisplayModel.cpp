
// Manages a set of events that we can select using a string value.
// This is supposed to be used by the console output system as we have
// a user readable string value assigned to each event that gets executed.
//
// Project   : NaKama-Tools
// File Name : DisplayModel.h
// Date      : 23/11/2013
// Author    : Nicholas Welters


#include "CLIDisplayModel.h"
#include "CLIDisplayFunctor.h"

namespace tools
{
	namespace display
	{
		using ::std::string;
		using ::std::vector;

		/// <summary> default ctor </summary>
		CLIDisplayModel::CLIDisplayModel( )
			: Subject( )
			, text( "" )
			, pFunctions( new FunctionMap( ) )
			, keys( )
		{ }

		/// <summary> dtor </summary>
		CLIDisplayModel::~CLIDisplayModel( )
		{ }

		/// <summary> Get human readable description for this cli options model. </summary>
		/// <returns> The models description. </returns>
		string CLIDisplayModel::getText( )
		{
			return text;
		}

		/// <summary> Get human readable description for a selected cli option. </summary>
		/// <param name="key"> The key to the functor  we want a description for. </param>
		/// <returns> The options description. </returns>
		string CLIDisplayModel::getText( const string& key )
		{
			FunctionMap::const_iterator i = pFunctions->find( key );

			if( i == pFunctions->end( ) )
			{
				throw CLIDisplayException( key );
			}

			return i->second->getText( );
		}

		/// <summary> Set human readable description for a selected cli option. </summary>
		/// <param name="text"> The models description. </param>
		void CLIDisplayModel::setText( const string & text_ )
		{
			text = text_;
			notify( );
		}

		/// <summary> Set the cli options for the model. </summary>
		/// <param name="functions"> The models cli options & function map. </param>
		void CLIDisplayModel::setFunctions( const FunctionMapSPtr& functions )
		{
			pFunctions = functions;
			notify( );
		}

		/// <summary> Set the human readable description & options for the model. </summary>
		/// <param name="functions"> The models cli options & function map. </param>
		/// <param name="text"> The models description. </param>
		void CLIDisplayModel::setValue( const FunctionMapSPtr& functions, const string& text_ )
		{
			pFunctions = functions;
			text = text_;
			notify( );
		}

		/// <summary> Get the keys used to identify the functions in the function hash map. </summary>
		/// <returns> A cached vector of all the keys in the functions map. </returns>
		const vector< string >& CLIDisplayModel::getKeys( ) const
		{
			return keys;
		}

		/// <summary> Check if a function exists with a specific key. </summary>
		/// <param name="key"> The key to the functor we want to validate. </param>
		bool CLIDisplayModel::contains( const string& key ) const
		{
			return pFunctions->find( key ) != pFunctions->end( );
		}

		/// <summary> Execute a function in the model that is associated with the provided key. </summary>
		/// <param name="key"> The key to the functor we will execute. </param>
		void CLIDisplayModel::trigger( const string& key ) const
		{
			FunctionMap::const_iterator i = pFunctions->find( key );

			if( i == pFunctions->end( ) )
			{
				throw CLIDisplayException( key );
			}

			i->second->operator( )( );
		}

		/// <summary> 
		/// Signal the observers that there has been a (very generic) change in the subject.
		/// We have the opportunity to update our own cached state at the same time.
		///</summary>
		void CLIDisplayModel::notify( )
		{
			keys.clear( );

			for( FunctionMap::iterator iter = pFunctions->begin( ); iter != pFunctions->end( ); iter++ )
			{
				keys.emplace_back( iter->first );
			}

			Subject::notify( );
		}
	} // namespace display
} // namespace tools
