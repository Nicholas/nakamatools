
// A functor with a string definition that is human readable.
//
// Project   : NaKama-Tools
// File Name : DisplayFunctor.cpp
// Date      : 23/11/2013
// Author    : Nicholas Welters


#include "CLIDisplayFunctor.h"

namespace tools
{
	namespace display
	{
		using ::std::string;

		/// <summary> copy ctor </summary>
		/// <param name="original"> The object state to copy. </param>
		CLIDisplayFunctor::CLIDisplayFunctor( const CLIDisplayFunctor& original )
			: Functor( original )
		{ }

		/// <summary> Get the functors name. </summary>
		/// <returns> The functors human readable name. <returns>
		string CLIDisplayFunctor::getText( ) const
		{
			return text;
		}

		/// <summary> Set the functors human readable name. </summary>
		/// <param name="text"> The human readable name for the functor. </param>
		void CLIDisplayFunctor::setText(const string& newText)
		{
			text = newText;
		}
	} // namespace display
} // namespace tools
