#pragma once
////////////////////////////////////////////////////////////////////////////////
// The Loki Library
// Copyright (c) 2001 by Andrei Alexandrescu
// Copyright (c) 2005 by Peter Kuemmel
// This code DOES NOT accompany the book:
// Alexandrescu, Andrei. "Modern C++ Design: Generic Programming and Design
//     Patterns Applied". Copyright (c) 2001. Addison-Wesley.
//
// Code covered by the MIT License
// The authors make no representations about the suitability of this software
// for any purpose. It is provided "as is" without express or implied warranty.
////////////////////////////////////////////////////////////////////////////////
//
// Just the old style factory reimplemented from The Loki Library.
//
// Project   : NaKama-Tools
// File Name : FunctorDispatcher.h
// Date      : 05/11/2013
// Author    : Nicholas Welters

#include "Factory.h"

#ifndef _FACTORY_HPP
#define _FACTORY_HPP

namespace tools
{
	/// <summary> ctor. </summary>
	template
	<
		class AbstractProduct,
		class IdentifierType,
		class ProductCreator,
		template< typename, class >
		class FactoryErrorPolicy
	>
	Factory< AbstractProduct, IdentifierType, ProductCreator, FactoryErrorPolicy >::Factory( )
		: FactoryErrorPolicy< IdentifierType, AbstractProduct >( )
	{ }

	/// <summary> Registers a creator function with the factory. </summary>
	/// <param name="id"> The product type identifier. </param>
	/// <param name="creator"> The factory function. </param>
	/// <returns> True if the creator was the first one we tried to add with the id, false otherwise. </returns>
	template
	<
		class AbstractProduct,
		class IdentifierType,
		class ProductCreator,
		template< typename, class >
		class FactoryErrorPolicy
	>
	bool Factory< AbstractProduct, IdentifierType, ProductCreator, FactoryErrorPolicy >::registerProduct( const IdentifierType & id, ProductCreator creator )
	{
		return workers.insert( typename Workers::value_type( id, creator ) ).second;
	}

	/// <summary> Removes a registered function from the factory. </summary>
	/// <param name="id"> The product type identifier. </param>
	/// <returns> True if a creator was removed. </returns>
	template
	<
		class AbstractProduct,
		class IdentifierType,
		class ProductCreator,
		template< typename, class >
		class FactoryErrorPolicy
	>
	bool Factory< AbstractProduct, IdentifierType, ProductCreator, FactoryErrorPolicy >::unregisterProduct( const IdentifierType & id )
	{
		return workers.erase( id ) == 1;
	}

	/// <summary> Finds the correct creator and returns a pointer to our product. </summary>
	/// <param name="id"> The product type identifier. </param>
	/// <returns> A pointer to a created product. </returns>
	template
	<
		class AbstractProduct,
		class IdentifierType,
		class ProductCreator,
		template< typename, class >
		class FactoryErrorPolicy
	>
	AbstractProduct * Factory< AbstractProduct, IdentifierType, ProductCreator, FactoryErrorPolicy >::createProduct( const IdentifierType & id )
	{
		typename Workers::iterator i = workers.find( id );

		if( i != workers.end( ) )
		{
			return i->second.operator( )( );
		}

		return FactoryErrorPolicy< IdentifierType, AbstractProduct >::OnUnknownError( id );
	}
} // namespace tools

#endif // _FACTORY_HPP
