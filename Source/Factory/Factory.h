#pragma once
////////////////////////////////////////////////////////////////////////////////
// The Loki Library
// Copyright (c) 2001 by Andrei Alexandrescu
// Copyright (c) 2005 by Peter Kuemmel
// This code DOES NOT accompany the book:
// Alexandrescu, Andrei. "Modern C++ Design: Generic Programming and Design
//     Patterns Applied". Copyright (c) 2001. Addison-Wesley.
//
// Code covered by the MIT License
// The authors make no representations about the suitability of this software
// for any purpose. It is provided "as is" without express or implied warranty.
////////////////////////////////////////////////////////////////////////////////
//
// Just the old style factory reimplemented from The Loki Library.
//
// Project   : NaKama-Tools
// File Name : FunctorDispatcher.h
// Date      : 05/11/2013
// Author    : Nicholas Welters

#ifndef _FACTORY_H
#define _FACTORY_H

#include "FactoryErrorPolicies.h"
#include <unordered_map>

namespace tools
{
	/// <summary> A generic object factory. </summary>
	/// <template name="AbstractProduct"> The products base type. </template>
	/// <template name="IdentifierType"> The concrete products identifier type. </template>
	/// <template name="ProductCreator"> The product factory function type. </template>
	/// <template name="FactoryErrorPolicy"> Determines what the factory will do as a last-resort. </template>
	template
	<
		class AbstractProduct,
		class IdentifierType,
		class ProductCreator = AbstractProduct* ( * )( ),
		template< typename, class >
		class FactoryErrorPolicy = DefaultFactoryError
	>
	class Factory : public FactoryErrorPolicy< IdentifierType, AbstractProduct >
	{
		public:
			/// <summary> ctor. </summary>
			Factory( );

			/// <summary> Registers a creator function with the factory. </summary>
			/// <param name="id"> The product type identifier. </param>
			/// <param name="creator"> The factory function. </param>
			/// <returns> True if the creator was the first one we tried to add with the id, false otherwise. </returns>
			bool registerProduct( const IdentifierType& id, ProductCreator creator );

			/// <summary> Removes a registered function from the factory. </summary>
			/// <param name="id"> The product type identifier. </param>
			/// <returns> True if a creator was removed. </returns>
			bool unregisterProduct( const IdentifierType& id );

			/// <summary> Finds the correct creator and returns a pointer to our product. </summary>
			/// <param name="id"> The product type identifier. </param>
			/// <returns> A pointer to a created product. </returns>
			AbstractProduct* createProduct( const IdentifierType& id );

		private:
			/// <summary> Identifier type to product creator hash map. </summary>
			typedef ::std::unordered_map< IdentifierType, ProductCreator > Workers;

			/// <summary> The factory function hash map. </summary>
			Workers workers;
	};
} // namespace tools

#include "Factory.hpp"

#endif // _FACTORY_H
