#pragma once
////////////////////////////////////////////////////////////////////////////////
// The Loki Library
// Copyright (c) 2001 by Andrei Alexandrescu
// Copyright (c) 2005 by Peter Kuemmel
// This code DOES NOT accompany the book:
// Alexandrescu, Andrei. "Modern C++ Design: Generic Programming and Design
//     Patterns Applied". Copyright (c) 2001. Addison-Wesley.
//
// Code covered by the MIT License
// The authors make no representations about the suitability of this software
// for any purpose. It is provided "as is" without express or implied warranty.
////////////////////////////////////////////////////////////////////////////////
//
// Just the old style factory reimplemented from The Loki Library.
//
// Project   : NaKama-Tools
// File Name : FunctorDispatcher.h
// Date      : 05/11/2013
// Author    : Nicholas Welters


#ifndef _FACTORY_ERROR_POLICIES_H
#define _FACTORY_ERROR_POLICIES_H

#include <exception>

namespace tools
{
	/// <summary> The default policy that throws a unique exception. </summary>
	template< class IdentifierType, class AbstractProduct >
	class DefaultFactoryError
	{
		public:
			/// <summary> The unique exception type. </summary>
			class Exception: public ::std::exception
			{
				public:
					Exception( const IdentifierType& unknownID )
						: unknownID( unknownID )
					{ }

					virtual const char* what( )
					{
						return "Unknown Identifier Type";
					}

					const IdentifierType& getIdentifier( )
					{
						return unknownID;
					}

				private:
					IdentifierType unknownID;
			};

		protected:
			/// <summary> The unique exception type. </summary>
			/// <param name="id"> The product type identifier. </param>
			/// <returns> Nothing as an exception is thrown instead. </returns>
			static AbstractProduct* OnUnknownError( const IdentifierType& id )
			{
				throw Exception( id );
			}
	};
} // namespace tools

# endif // _FACTORY_ERROR_POLICIES_H
