
#include <gtest/gtest.h>
#include <Functors/Functor.h>

class FunctorsTest : public ::testing::Test
{
	protected:
		FunctorsTest( ){ }
		virtual ~FunctorsTest( ) { }

		virtual void SetUp( ) { }
		virtual void TearDown( ) { }
};

bool returnTrue( ) { return true; };
bool returnBool( bool value ) { return value; };

using ::tools::Functor;
using ::std::unique_ptr;

TEST( FunctorsTest, WorksWithLambdas )
{
	Functor< bool > functorTrue( [ ]( ) { return true; } );
	Functor< bool, bool > functorBool( [ ]( bool value ) { return value; } );

	ASSERT_TRUE( functorTrue( ) );
	ASSERT_TRUE( functorBool( true ) );
	ASSERT_FALSE( functorBool( false ) );
}

TEST( FunctorsTest, WorksWithMemberFunction )
{
	struct TestType
	{
		bool returnTrue( ) { return true; }
		bool returnBool( bool value ) { return value; }
	} object;
	
	Functor< bool > functorTrue( &object, &TestType::returnTrue );
	Functor< bool, bool > functorBool( &object, &TestType::returnBool );


	ASSERT_TRUE( functorTrue( ) );
	ASSERT_TRUE( functorBool( true ) );
	ASSERT_FALSE( functorBool( false ) );
}

TEST( FunctorsTest, WorksFunctions )
{
	Functor< bool > functorTrue( returnTrue );
	Functor< bool, bool > functorBool( returnBool );

	ASSERT_TRUE( functorTrue( ) );
	ASSERT_TRUE( functorBool( true ) );
	ASSERT_FALSE( functorBool( false ) );
}

TEST( FunctorsTest, WorksFunctorType )
{
	struct TestType
	{
		bool operator( )( ) { return true; }
		bool operator( )( bool value ) { return value; }
	} object;

	Functor< bool > functorTrue( object );
	Functor< bool, bool > functorBool( object );

	ASSERT_TRUE( functorTrue( ) );
	ASSERT_TRUE( functorBool( true ) );
	ASSERT_FALSE( functorBool( false ) );
}

TEST( FunctorsTest, WorksFromAnotherCopy )
{
	struct TestType
	{
		bool operator( )( ) { return true; }
		bool operator( )( bool value ) { return value; }
	} object;

	Functor< bool > functorTrueOrig( object );
	Functor< bool, bool > functorBoolOrig( object );

	Functor< bool > functorTrue( functorTrueOrig );
	Functor< bool, bool > functorBool( functorBoolOrig );

	ASSERT_TRUE( functorTrue( ) );
	ASSERT_TRUE( functorBool( true ) );
	ASSERT_FALSE( functorBool( false ) );
}

TEST( FunctorsTest, WorksWithCustomImplementation )
{
	typedef Functor< bool > FunctorType;

	class CustomImplementation : FunctorType::Implementation
	{
		bool fun;

		virtual FunctorType::Implementation* clone( ) const
		{
			return new CustomImplementation( fun );
		}

		public:
		CustomImplementation( const bool& fun )
			: fun( fun )
		{
		}

		virtual ~CustomImplementation( ) = default;

		virtual bool operator( )( )
		{
			return fun;
		}
	};

	FunctorType functor( unique_ptr< FunctorType::Implementation >( ( FunctorType::Implementation* ) new CustomImplementation( true ) ) );

	ASSERT_TRUE( functor( ) );
}
