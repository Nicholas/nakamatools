
#include <Timing/TickCounter.h>

#include <gtest/gtest.h>

class TickCounterTest : public ::testing::Test
{
	protected:
		TickCounterTest( ) { }
		virtual ~TickCounterTest( ) { }

		virtual void SetUp( ) { }
		virtual void TearDown( ) { }
};

using ::tools::TickCounter;

TEST( TickCounterTest, Counts3Ticks )
{
	TickCounter counter;

	counter.countTick( 0.3f );
	counter.countTick( 0.3f );
	counter.countTick( 0.4f );

	EXPECT_FLOAT_EQ( 1.0f / 0.4f, counter.getCountByDelta( ) );
	EXPECT_FLOAT_EQ( 3.0f, counter.getCountByPeriod( ) );
}

TEST( TickCounterTest, CountsWeightedTicks )
{
	TickCounter counter( 2 );

	counter.countTick( 1.0f );

	EXPECT_FLOAT_EQ( 2.0f / 0.1f, counter.getCountByRatio( _9_1_ ) );
	EXPECT_FLOAT_EQ( 2.0f / 0.3f, counter.getCountByRatio( _7_3_ ) );

	counter.countTick( 1.0f );

	EXPECT_FLOAT_EQ( 2.0f / ( 0.1f * 0.9f + 0.1f ), counter.getCountByRatio( _9_1_ ) );
	EXPECT_FLOAT_EQ( 2.0f / ( 0.3f * 0.7f + 0.3f ), counter.getCountByRatio( _7_3_ ) );
}

TEST( TickCounterTest, WeightedTicksConverge )
{
	TickCounter counter( 2 );
	
	for( int i = 0; i < 100; i++ )
	{
		counter.countTick( 1.0f );
		counter.countTick( 1.0f );
	}
	
	EXPECT_FLOAT_EQ( 2.0f, counter.getCountByRatio( _9_1_ ) );
	EXPECT_FLOAT_EQ( 2.0f, counter.getCountByRatio( _7_3_ ) );
}

TEST( TickCounterTest, CountsMinAndMaxTicks )
{
	TickCounter counter;

	counter.countTick( 0.1f );
	counter.countTick( 0.9f );

	EXPECT_FLOAT_EQ( 1.0f / 0.9f, counter.getSlowestByPeriod( ) );
	EXPECT_FLOAT_EQ( 1.0f / 0.1f, counter.getFastestByPeriod( ) );
}
