
#include <Timing/Timer.h>

#include <gtest/gtest.h>

#include <thread>
#include <chrono>

class TimerTest : public ::testing::Test
{
	protected:
	TimerTest( ) { }
	virtual ~TimerTest( ) { }

	virtual void SetUp( ) { }
	virtual void TearDown( ) { }
};

using ::tools::Timer;
using namespace ::std::chrono_literals;
using namespace ::std::this_thread;

TEST( TimerTest, CanDetermineTickTimes )
{
	Timer timer;

	timer.reset( );
	sleep_for( 1ms );
	timer.tick( );

	EXPECT_GT( timer.getDelta( ), 0 );
}

TEST( TimerTest, CanBeStopped )
{
	Timer timer;

	timer.reset( );
	sleep_for( 1ms );
	timer.tick( );

	EXPECT_GT( timer.getDelta( ), 0 );

	timer.stop( );
	sleep_for( 1ms );
	timer.tick( );

	EXPECT_EQ( timer.getDelta( ), 0 );
	EXPECT_GT( timer.getTime( ) , 0 );

	timer.start( );
	sleep_for( 1ms );
	timer.tick( );

	EXPECT_GT( timer.getDelta( ), 0 );
}
