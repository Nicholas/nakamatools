
#include <gtest/gtest.h>
#include <Multimethods/BasicFastDispatcher.h>

class BasicFastDispatcherTest : public ::testing::Test
{
	protected:
		BasicFastDispatcherTest( ) { }
		virtual ~BasicFastDispatcherTest( ) { }

		virtual void SetUp( ) { }
		virtual void TearDown( ) { }
};

using ::tools::BasicFastDispatch;
using ::std::cout;
using ::std::endl;
using ::std::flush;
using ::std::exception;

namespace
{
	struct BaseTest
	{
		IMPLEMENT_INDEXABLE_CLASS( BaseTest )
		bool value;
	};

	struct DerivedTestA : BaseTest
	{
		IMPLEMENT_INDEXABLE_CLASS( DerivedTestA )
		int value2;
	};

	struct BaseTester
	{
		bool operator()( BaseTest baseLHS, BaseTest baseRHS )
		{
			return baseLHS.value == baseRHS.value;
		}
	};
}

TEST( BasicFastDispatcherTest, dispatchesOnBaseTypeOnlyFunctor )
{
	BaseTester tester;
	BasicFastDispatch< BaseTest &, BaseTest &, bool, BaseTester > dispatch;

	dispatch.add< BaseTest, BaseTest >( tester );

	BaseTest lhs;
	BaseTest rhs;


	lhs.value = false;
	rhs.value = true;

	DerivedTestA nonFunc;
	nonFunc.value = true;
	nonFunc.value2 = 0;

	try
	{
		ASSERT_FALSE( dispatch.go( lhs, rhs ) );
	}
	catch( exception& e )
	{
		cout << e.what( ) << endl << flush;
		FAIL( );
	}

	EXPECT_ANY_THROW( dispatch.go( lhs, nonFunc ) );
}
