
#include <gtest/gtest.h>
#include <Multimethods/FunctorDispatch.h>

class FunctorDispatcherTest : public ::testing::Test
{
	protected:
		FunctorDispatcherTest( ) { }
		virtual ~FunctorDispatcherTest( ) { }

		virtual void SetUp( ) { }
		virtual void TearDown( ) { }
};

using ::tools::FunctorDispatch;

namespace
{
	struct BaseClass
	{
		IMPLEMENT_INDEXABLE_CLASS( BaseClass )
		int level = 0;
	};

	struct BaseClassChildA : public BaseClass
	{
		IMPLEMENT_INDEXABLE_CLASS( BaseClassChildA )
		BaseClassChildA( )
			: BaseClass( )
		{
			level = 1;
		}
	};
	struct BaseClassChildB : public BaseClass
	{
		IMPLEMENT_INDEXABLE_CLASS( BaseClassChildB )
		BaseClassChildB( )
			: BaseClass( )
		{
			level = 8;
		}
	};
	struct BaseClassChildC : public BaseClass
	{
		IMPLEMENT_INDEXABLE_CLASS( BaseClassChildC )
		BaseClassChildC( )
			: BaseClass( )
		{
			level = 3;
		}
	};

	struct DispatchTestExecutorA
	{
		bool operator( )( BaseClass & l, BaseClassChildA & r )
		{
			return l.level == 0 && r.level == 1;
		}
	};

	struct DispatchTestExecutorB
	{
		bool operator( )( BaseClassChildB& l, BaseClassChildA& r )
		{
			return l.level == 8 && r.level == 1;
		}
	};
}

TEST( FunctorDispatcherTest, dispatchesOnType )
{
	typedef FunctorDispatch<::BaseClass, ::BaseClass, bool> TestDispatch;
	TestDispatch testDispatch;

	::DispatchTestExecutorA a;
	::DispatchTestExecutorB b;

	testDispatch.add<::BaseClass, ::BaseClassChildA, ::DispatchTestExecutorA>( a );
	testDispatch.add<::BaseClassChildB, ::BaseClassChildA, ::DispatchTestExecutorB>( b );

	::BaseClass base;
	::BaseClassChildA baseA;
	::BaseClassChildB baseB;

	EXPECT_TRUE( testDispatch.go( base, ( ::BaseClass& ) baseA ) );
	EXPECT_TRUE( testDispatch.go( ( ::BaseClass& ) baseB, ( ::BaseClass& ) baseA ) );
}
