
#include <Console/CLIDisplay.h>
#include <Console/CLIDisplayModel.h>
#include <Console/CLIDisplayFunctor.h>

#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <sstream>

class CLIDisplayTest : public ::testing::Test
{
	protected:
		CLIDisplayTest( ) { }
		virtual ~CLIDisplayTest( ) { }

		virtual void SetUp( ) { }
		virtual void TearDown( ) { }
};

using ::std::make_shared;
using ::std::string;
using ::std::stringstream;
using ::std::ostringstream;
using ::std::endl;
using ::std::vector;


using ::tools::display::CLIDisplay;
using ::tools::display::CLIDisplayModel;
using ::tools::display::CLIDisplayFunctor;

using ::tools::display::CLIDisplayModelSPtr;
using ::tools::display::CLIDisplayFunctorSPtr;

TEST( CLIDisplayTest, FunctorHasDiscription )
{
	string text = "Test";
	CLIDisplayFunctor functor( text, [ ]( ) { } );

	ASSERT_EQ( text, functor.getText( ) );
}

TEST( CLIDisplayTest, ModelTriggersEvents )
{
	string modelDisc = "Test Model";
	string eventDisc = "Quit";
	string eventKey = "Q";
	int isCalled = 0;
	int isCalledOnce = 1;

	CLIDisplayModel::FunctionMapSPtr pFunctions = make_shared< CLIDisplayModel::FunctionMap >( );
	CLIDisplayFunctorSPtr exit = make_shared< CLIDisplayFunctor >( eventDisc, [ &isCalled ]( ) { isCalled++; } );
	(*pFunctions)[ eventKey ] = exit;
	
	CLIDisplayModel model;

	model.setValue( pFunctions, modelDisc );

	vector< string > keys = model.getKeys( );

	ASSERT_EQ( modelDisc, model.getText( ) );
	ASSERT_EQ( keys.size( ), 1 );
	ASSERT_EQ( keys[ 0 ], eventKey );
	ASSERT_EQ( model.getText( eventKey ), eventDisc );
	ASSERT_TRUE( model.contains( eventKey ) );

	model.trigger( eventKey );

	ASSERT_EQ( isCalledOnce, isCalled );
}

TEST( CLIDisplayTest, ExpectedOutputFickle )
{
	stringstream input;
	ostringstream output;

	CLIDisplayModelSPtr pModel = make_shared< CLIDisplayModel >( );
	CLIDisplay display( pModel, output, input );

	CLIDisplayFunctorSPtr exit = make_shared< CLIDisplayFunctor >( "Quit.", &display, &CLIDisplay::shutdown );

	CLIDisplayModel::FunctionMapSPtr pFunctions = make_shared< CLIDisplayModel::FunctionMap >( );

	pFunctions->insert( CLIDisplayModel::FunctionMap::value_type( "Q", exit ) );

	pModel->setValue( pFunctions, "Test Display" );

	input << "wrong" << endl;
	input << "Q" << endl;

	display.display( );

	string expected;
	expected  = "Test Display\n";
	expected += "\n";
	expected += "|     |::|\n";
	expected += "|> Q <|::|> Quit.\n";
	expected += "|     |::|\n";
	expected += "\n";
	expected += "Please enter selection: ";
	expected += "Unknown command 'wrong'.\n";
	expected += "Please enter selection: ";

	ASSERT_TRUE( ( expected == output.str( ) ) );
}
