
#include <Files/JsonParser.h>
#include <gtest/gtest.h>
#include <gmock/gmock.h>

class JsonParserTest : public ::testing::Test
{
	protected:
	JsonParserTest( ) { }
	virtual ~JsonParserTest( ) { }

	virtual void SetUp( ) { }
	virtual void TearDown( ) { }
};

using ::tools::JsonParser;


namespace
{
	class MockParser : public ::tools::JsonParser
	{
		public:
			MockParser( const ::tools::Scheduler& schedule )
				: JsonParser( schedule )
			{ }

			MOCK_METHOD1( parseJSON, void( const cJSONSPtr& pRoot ) );
	};
}

TEST( JsonParserTest, ParsesAStringToJson )
{
	tools::Scheduler schedule = [ ]( const std::function< void( ) >& fun ) 
	{
		fun( );
	};

	::MockParser parser( schedule );
	EXPECT_CALL( parser, parseJSON( ::testing::_ ) ).Times( 1 );

	::std::string json = "\
	{\
		\"meta\": {}\
	}";

	parser.parse( json );
}
