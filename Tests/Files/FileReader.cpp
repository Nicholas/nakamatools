#include <Files/JsonParser.h>
#include <Files/FileReader.h>
#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <filesystem>

class FileReaderTest : public ::testing::Test
{
	protected:
	FileReaderTest( ) { }
	virtual ~FileReaderTest( ) { }

	virtual void SetUp( ) { }
	virtual void TearDown( ) { }
};

using ::tools::JsonParser;
using ::std::experimental::filesystem::path;
using ::std::experimental::filesystem::exists;


namespace
{
	class MockParser : public ::tools::JsonParser
	{
		public:
		MockParser( const ::tools::Scheduler& schedule )
			: JsonParser( schedule )
		{
		}

		MOCK_METHOD1( parseJSON, void( const cJSONSPtr& pRoot ) );
	};
}

TEST( FileReaderTest, ReadsAFileAndSendsItToAParser )
{
	tools::Scheduler schedule = [ ]( const std::function< void( ) >& fun )
	{
		fun( );
	};


	::MockParser parser( schedule );
	EXPECT_CALL( parser, parseJSON( ::testing::_ ) ).Times( 1 );


	::tools::JsonParserSPtr pParser( &parser, [ ]( ::tools::JsonParser* ) { } );

	::tools::FileReader fileReader( schedule );

	fileReader.add( ".json", pParser );

	path filePath( __FILE__ );

	path testFile( filePath.parent_path( ).string( ) + "/test.json" );
	
	fileReader.readFile( testFile.string( ) );
}
