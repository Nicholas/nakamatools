
#include <gtest/gtest.h>
#include <Factory/Factory.h>

#include <functional>
#include <memory>

class FactoryTest : public ::testing::Test
{
	protected:
		FactoryTest( ) { }
		virtual ~FactoryTest( ) { }

		virtual void SetUp( ) { }
		virtual void TearDown( ) { }
};

using ::tools::Factory;
using ::std::string;

using ::std::function;
using ::std::shared_ptr;

namespace
{
	struct BaseClass
	{
		int level;

		BaseClass( int level )
			: level( level )
		{
		}
	};

	struct BaseClassChildA : public BaseClass
	{
		BaseClassChildA( )
			: BaseClass( 1 )
		{
		}
	};
	struct BaseClassChildB : public BaseClass
	{
		BaseClassChildB( )
			: BaseClass( 2 )
		{
		}
	};
}

TEST( FactoryTest, constructsProducts )
{
	typedef Factory<::BaseClass, string, function< ::BaseClass* ( void ) > > TestFactory;
	TestFactory factory;

	EXPECT_TRUE( factory.registerProduct( "Base"  , [ ]( )-> ::BaseClass* { return new ::BaseClass( 0 );     } ) );
	EXPECT_TRUE( factory.registerProduct( "ChildA", [ ]( )-> ::BaseClass* { return new ::BaseClassChildA( ); } ) );
	EXPECT_TRUE( factory.registerProduct( "ChildB", [ ]( )-> ::BaseClass* { return new ::BaseClassChildB( ); } ) );

	shared_ptr< ::BaseClass > pBase( factory.createProduct( "Base" ) );
	shared_ptr< ::BaseClass > pChildA( factory.createProduct( "ChildA" ) );
	shared_ptr< ::BaseClass > pChildB( factory.createProduct( "ChildB" ) );

	EXPECT_EQ( pBase->level, 0 );
	EXPECT_EQ( pChildA->level, 1 );
	EXPECT_EQ( pChildB->level, 2 );
}
