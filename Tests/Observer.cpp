
#include <Observer/Observer.h>
#include <Observer/Subject.h>
#include <gtest/gtest.h>
#include <gmock/gmock.h>

class ObserverTest : public ::testing::Test
{
	protected:
		ObserverTest( ) { }
		virtual ~ObserverTest( ) { }

		virtual void SetUp( ) { }
		virtual void TearDown( ) { }
};

namespace
{
	class MockObserver : public ::tools::Observer
	{
		public:
			MOCK_METHOD0( notified, void( ) );
	};
}

using ::tools::Subject;

TEST( ObserverTest, SubjectNotifiesObserver )
{
	::MockObserver observer;
	EXPECT_CALL( observer, notified( ) ).Times( 1 );

	Subject subject;

	subject.attach( &observer );
	subject.notify( );
	subject.detach( &observer );
	subject.notify( );
}
