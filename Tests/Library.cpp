
#include <Library/Library.h>
#include <gtest/gtest.h>

class LibraryTest : public ::testing::Test
{
	protected:
	LibraryTest( ) { }
	virtual ~LibraryTest( ) { }

	virtual void SetUp( ) { }
	virtual void TearDown( ) { }
};

using ::tools::Library;
using ::tools::DefaultBookDeleter;

TEST( LibraryTest, ConstructsALibrary )
{
	struct Test
	{
		std::string value;
	};

	typedef Library< Test, std::string, DefaultBookDeleter< Test >, const std::string& > TestLib;

	TestLib::Factory factory = []( const std::string& str )
	{
		return new Test{ str };
	};

	DefaultBookDeleter< Test > deleter = []( Test * old )
	{
		delete old;
	};

	TestLib library( factory, deleter );
}


class TestStaticRefCounter
{
public:
	TestStaticRefCounter( const std::string& str )
		:value( str )
	{
		refCount++;
	}

	~TestStaticRefCounter( )
	{
		refCount--;
	}

	std::string value;

	static int refCount;
};

int TestStaticRefCounter::refCount = 0;

TEST( LibraryTest, DeletesUsedObjects )
{
	typedef Library< TestStaticRefCounter, std::string, DefaultBookDeleter< TestStaticRefCounter >, const std::string& > TestLib;

	TestLib::Factory factory = []( const std::string& str )
	{
		return new TestStaticRefCounter( str );
	};

	DefaultBookDeleter< TestStaticRefCounter > deleter = []( TestStaticRefCounter * old )
	{
		delete old;
	};

	TestLib library( factory, deleter );
	
	{
		TestLib::DataSPtr p_data = library.checkOut( "A", "Test data 1" );
		
		EXPECT_EQ( "Test data 1", p_data->value );
		EXPECT_EQ( 1, p_data->refCount ); // We created a new TestStaticRefCounter

		{
			TestLib::DataSPtr p_dataA = library.checkOut( "B", "Test data 2" );
		
			EXPECT_EQ( "Test data 2", p_dataA->value );
			EXPECT_EQ( 2, p_dataA->refCount ); // We created a new TestStaticRefCounter

			TestLib::DataSPtr p_dataB = library.checkOut( "B", "Test data 2" );
		
			EXPECT_EQ( "Test data 2", p_dataB->value );
			EXPECT_EQ( 2, p_dataB->refCount ); // We found the same TestStaticRefCounter

		} // remove Test data 2

		EXPECT_EQ( 1, p_data->refCount );

	} // remove Test data 1

	EXPECT_EQ( 0, TestStaticRefCounter::refCount );
}
