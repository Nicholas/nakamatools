
#include <Threading/AsyncController.h>
#include <Processing/ProcessExecutor.h>
#include <Processing/Process.h>

#include <gtest/gtest.h>

class AsyncControllerTest : public ::testing::Test
{
};

using ::tools::AsyncController;
using ::tools::ProcessExecutor;
using ::tools::Process;

using ::std::string;

namespace
{
	class TestController : public AsyncController
	{
		public:
			int& updateValue;
			int& setValue;

			TestController( const string& name, int& updateValue, int& setValue )
				: AsyncController( name )
				, updateValue( updateValue )
				, setValue( setValue )
			{ }

			void incrementTestValue( )
			{
				addUpdateEvent( [ this ]( ) { updateValue++; } );
			}

			void update( ) final override
			{
				setValue += 10; // update should only be called once..
			}
	};
}

TEST( AsyncControllerTest, AsyncBufferUpdatesValuesOnExecute )
{
	int beforeExecute = 0;
	int executeUpdate = 2;
	int executeSet    = 10;
	int testUpdate = beforeExecute;
	int testSet    = beforeExecute;

	string expectedName = "Name";

	::TestController controller( expectedName, testUpdate, testSet );

	EXPECT_EQ( beforeExecute, testUpdate );
	EXPECT_EQ( beforeExecute, testSet );

	controller.incrementTestValue( );

	EXPECT_EQ( beforeExecute, testUpdate );
	EXPECT_EQ( beforeExecute, testSet );

	controller.incrementTestValue( );

	EXPECT_EQ( beforeExecute, testUpdate );
	EXPECT_EQ( beforeExecute, testSet );

	controller.execute( );

	EXPECT_EQ( executeUpdate, testUpdate );
	EXPECT_EQ( executeSet   , testSet );

	EXPECT_EQ( expectedName, controller.getName( ) );
}
