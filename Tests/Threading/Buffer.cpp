

#include <Threading/Buffer.h>
#include <Processing/ProcessExecutor.h>
#include <Processing/Process.h>

#include <gtest/gtest.h>

class BufferTest: public ::testing::Test
{
};

using ::tools::Process;
using ::tools::ProcessExecutor;
using ::tools::Buffer;

using ::std::shared_ptr;

typedef Buffer< int > IntBuffer;

namespace
{
	class TestProcess1: public Process
	{
		public:
			IntBuffer* buffer;
			int* value;

			virtual void execute( )
			{
				buffer->push( (*value)++ );
			}
	};

	class TestProcess2: public Process
	{
		public:
			IntBuffer* buffer;
			int* value;

			virtual void execute( )
			{
				buffer->push( ++(*value) );
			}
	};

	class ShutdownProcess: public Process
	{
		public:
			ProcessExecutor< > * engine;

			virtual void execute( )
			{
				engine->shutdown( );
			}
	};
}

TEST( BufferTest, ConcurrentBufferPostIncTest )
{
	IntBuffer buffer;

	int testValue = 10;
	int expectedValue1 = 10;
	int expectedValue2 = 11;

	ProcessExecutor< > engine( 1 );

	shared_ptr< ::TestProcess1 > pProcess( new ::TestProcess1 );
	pProcess->buffer = &buffer;
	pProcess->value = &testValue;

	shared_ptr< ::ShutdownProcess > pShutdownProcess( new ::ShutdownProcess );
	pShutdownProcess->engine = &engine;

	engine.add( pProcess );
	engine.add( pProcess );
	engine.add( pShutdownProcess );

	engine.run( );
	

	EXPECT_EQ( expectedValue1, buffer.front( ) );
	buffer.pop( );
	
	EXPECT_EQ( expectedValue2, buffer.front( ) );
	EXPECT_EQ( expectedValue2, buffer.front( ) );


	engine.join( );
}

TEST( BufferTest, ConcurrentBufferPreIncTest )
{
	IntBuffer buffer;

	int testValue = 10;
	int expectedValue1 = 11;
	int expectedValue2 = 12;

	ProcessExecutor< > engine( 1 );

	shared_ptr< ::TestProcess2 > pProcess( new ::TestProcess2 );
	pProcess->buffer = &buffer;
	pProcess->value = &testValue;

	shared_ptr< ::ShutdownProcess > pShutdownProcess( new ::ShutdownProcess );
	pShutdownProcess->engine = &engine;

	engine.add( pProcess );
	engine.add( pProcess );
	engine.add( pShutdownProcess );

	engine.run( );
	

	EXPECT_EQ( expectedValue1, buffer.front( ) );
	buffer.pop( );
	
	EXPECT_EQ( expectedValue2, buffer.front( ) );
	EXPECT_EQ( expectedValue2, buffer.front( ) );


	engine.join( );
}
