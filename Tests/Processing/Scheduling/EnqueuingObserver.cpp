
#include <Processing/Scheduling/ScheduledProcess.h>
#include <Processing/Scheduling/EnqueuingObserver.h>
#include <Processing/ProcessExecutor.h>
#include <Processing/BalancerDispatchPolicy.h>
#include <gtest/gtest.h>

class EnqueuingObserverTest
{
	protected:
		EnqueuingObserverTest( ) { }
		~EnqueuingObserverTest( ) { }

		virtual void SetUp( ) { }
		virtual void TearDown( ) { }
}; 

using ::tools::ScheduledProcess;
using ::tools::CompletedRunPolicyObserver;
using ::tools::SkippedRunPolicyObserver;
using ::tools::CompletedRunPolicySubject;
using ::tools::SkippedRunPolicySubject;
using ::tools::CompletedRunPolicyObserverSPtr;
using ::tools::SkippedRunPolicyObserverSPtr;
using ::tools::DelayPolicy;
using ::tools::IntervalPolicy;

using ::tools::EnqueuingObserver;
using ::tools::SyncProcessPolicy;

using ::tools::ProcessExecutor;
using ::tools::JoinWorkersPolicy;
using ::tools::BalancerDispatchPolicy;

TEST( EnqueuingObserverTest, ProcessIsScheduledForExecution )
{
	// Get a process executor the we will work with and tries to balance execution between two event sets.
	typedef ProcessExecutor< JoinWorkersPolicy, BalancerDispatchPolicy > ProcessExecutorTester;
	// Get s shared pointer type of our process executor.
	typedef std::shared_ptr< ProcessExecutorTester > SPProcessExecutorTest;
	// Get a EnqueuingObserver that will enqueue a process in to the process executors sync process set.
	typedef EnqueuingObserver< ProcessExecutorTester, SyncProcessPolicy< ProcessExecutorTester > > EnqueuingObserverTester;


	// This will be our test process class.
	// We make it shut down the process executor.
	// This means it works or else the system gets stuck in a live lock. (I have faith that it will work so nothing to worry about then?)
	class testProcess: public ScheduledProcess< CompletedRunPolicySubject, SkippedRunPolicySubject, DelayPolicy, IntervalPolicy >
	{
		private:
			SPProcessExecutorTest pProcessExecutor;

		public:
			int * value;

			testProcess( SPProcessExecutorTest & pProcessExecutor )
				: pProcessExecutor( pProcessExecutor )
			{ }

			virtual void run( )
			{
				pProcessExecutor->shutdown( );
				( *value )++;
			}
	};

	// Get a shared pointer to our shut down process.
	typedef std::shared_ptr< testProcess > SPtestProcess;

	SPProcessExecutorTest pEngineJoin( new ProcessExecutorTester( 1 ) );
	SPtestProcess pTestProcess( new testProcess( pEngineJoin ) );

	EnqueuingObserverTester tester( pTestProcess, pEngineJoin, 0 );

	int testValue = 0;
	int expectedValue = 1;

	pTestProcess->value = &testValue;

	pEngineJoin->initialiseDispatcher( 1, 1, 1, 1 );

	tester.addCompletedSubject( );
	tester.completedNotification( );

	pEngineJoin->run( );
	pEngineJoin->join( );

	EXPECT_EQ( testValue, ( *pTestProcess->value ) );
	EXPECT_EQ( expectedValue, ( *pTestProcess->value ) );
}

TEST( EnqueuingObserverTest, ProcessIsScheduledForExecution10000Times )
{
	// Get a process executor the we will work with and tries to balance execution between two event sets.
	typedef ProcessExecutor< JoinWorkersPolicy, BalancerDispatchPolicy > ProcessExecutorTester;
	// Get s shared pointer type of our process executor.
	typedef std::shared_ptr< ProcessExecutorTester > SPProcessExecutorTest;
	// Get a EnqueuingObserver that will queue a process in to the process executors sync process set.
	typedef EnqueuingObserver< ProcessExecutorTester, SyncProcessPolicy< ProcessExecutorTester > > EnqueuingObserverTester;
	// Get s shared pointer type of our EnqueuingObserver.
	typedef std::shared_ptr< EnqueuingObserverTester > SPEnqueuingObserverTester;


	// This will be our test process class.
	// We make it shut down the process executor.
	// This means it works or else the system gets stuck in a live lock. (I have faith that it will work so nothing to worry about then?)
	class testProcess: public ScheduledProcess< CompletedRunPolicySubject, SkippedRunPolicySubject, DelayPolicy, IntervalPolicy >
	{
		private:
		SPProcessExecutorTest pProcessExecutor;

		public:
			int * value;

			explicit testProcess( const SPProcessExecutorTest& pProcessExecutor )
				: pProcessExecutor( pProcessExecutor )
				, value( nullptr )
			{ }

			~testProcess( ){ }

			virtual void run( )
			{
				( *value )++;

				if( *value == 10000 )
				{
					pProcessExecutor->shutdown( );
				}

			}
	};

	// Get a shared pointer to our shut down process.
	typedef std::shared_ptr< testProcess > SPtestProcess;

	SPProcessExecutorTest pEngineJoin( new ProcessExecutorTester( 1 ) );
	SPtestProcess pTestProcess( new testProcess( pEngineJoin ) );

	SPEnqueuingObserverTester tester( new EnqueuingObserverTester( pTestProcess, pEngineJoin, 0 ) );

	int testValue = 0;
	int expectedValue = 10000;

	pTestProcess->value = &testValue;

	pEngineJoin->initialiseDispatcher( 1, 1, 1, 1 );

	//tester.completedNotification( );
	pTestProcess->addCompletedObserver( tester );
	pEngineJoin->addSync( pTestProcess );

	pEngineJoin->run( );
	pEngineJoin->join( );

	EXPECT_EQ( testValue, ( *pTestProcess->value ) );
	EXPECT_EQ( expectedValue, ( *pTestProcess->value ) );
}



TEST( EnqueuingObserverTest, TwoProcessesExecutedConcurrentlyAndDestroyed )
{
	
	// Get a process executor the we will work with and tries to balance execution between two event sets.
	typedef ProcessExecutor< JoinWorkersPolicy, BalancerDispatchPolicy > ProcessExecutorTester;
	// Get s shared pointer type of our process executor.
	typedef std::shared_ptr< ProcessExecutorTester > SPProcessExecutorTest;
	// Get a EnqueuingObserver that will queue a process in to the process executors sync process set.
	typedef EnqueuingObserver< ProcessExecutorTester, SyncProcessPolicy< ProcessExecutorTester > > EnqueuingObserverTester;
	// Get s shared pointer type of our EnqueuingObserver.
	typedef std::shared_ptr< EnqueuingObserverTester > SPEnqueuingObserverTester;


	// This will be our test process class.
	// We make it shut down the process executor.
	// This means it works or else the system gets stuck in a live lock. (I have faith that it will work so nothing to worry about then?)
	class testProcess: public ScheduledProcess< CompletedRunPolicySubject, SkippedRunPolicySubject, DelayPolicy, IntervalPolicy >
	{
		private:
			SPProcessExecutorTest pProcessExecutor;

		public:
			int& value;
			bool& destroyed;

			testProcess( const SPProcessExecutorTest& pProcessExecutor, int& value, bool& destroyed )
				: pProcessExecutor( pProcessExecutor )
				, value( value )
				, destroyed( destroyed )
			{ }

			~testProcess( )
			{
				destroyed = true;
			}

			virtual void run( )
			{
				value++;

				if( value == 400 )
				{
					pProcessExecutor->shutdown( );
				}
			}
	};

	class concurrentCounter: public ScheduledProcess< CompletedRunPolicySubject >
	{
		private:
			std::atomic_int& counter;

		public:
			concurrentCounter( std::atomic_int& counter )
				: counter( counter )
			{ }

			concurrentCounter& operator=( const concurrentCounter& copy )
			{
				counter.store( copy.counter );
			}

			virtual void run( )
			{
				counter++;
			}
	};

	std::atomic_int counter( { 0 } );
	int testValue = 0;
	int expectedValue = 400;
	int expected18    = 798;

	bool dataWasReleased = false;

	// Get a shared pointer to our shut down process.
	typedef std::shared_ptr< testProcess > SPtestProcess;
	typedef std::shared_ptr< concurrentCounter > SPconcurrentCounter;
	{
		SPProcessExecutorTest pEngineJoin( new ProcessExecutorTester( 1 ) );
		SPtestProcess pTestProcess( new testProcess( pEngineJoin, testValue, dataWasReleased ) );
		SPconcurrentCounter pConcurrent1( new concurrentCounter( counter ) );
		SPconcurrentCounter pConcurrent2( new concurrentCounter( counter ) );

		SPEnqueuingObserverTester tester( new EnqueuingObserverTester( pTestProcess, pEngineJoin, 0 ) );
		SPEnqueuingObserverTester concurrent1( new EnqueuingObserverTester( pConcurrent1, pEngineJoin, ProcessExecutorTester::SHARED_JOB_QUEUE ) );
		SPEnqueuingObserverTester concurrent2( new EnqueuingObserverTester( pConcurrent2, pEngineJoin, ProcessExecutorTester::SHARED_JOB_QUEUE ) );


		pEngineJoin->initialiseDispatcher( 1, 1, 1, 1 );

		// Here we want the two concurrent processes to execute after the test process.
		// we want them to execute concurrently and then only trigger the 
		// execution of the test process once both processes have completed execution.
		// A => B||C => A => B||C => ...
		pTestProcess->addCompletedObserver( concurrent1 );
		pTestProcess->addCompletedObserver( concurrent2 );
		pConcurrent1->addCompletedObserver( tester );
		pConcurrent2->addCompletedObserver( tester );

		pEngineJoin->addSync( pTestProcess );

		pEngineJoin->run( );
		pEngineJoin->join( );

		EXPECT_EQ( testValue, pTestProcess->value );
		EXPECT_EQ( expectedValue, pTestProcess->value );
	}

	EXPECT_EQ( expected18, counter );

	EXPECT_TRUE( dataWasReleased );
}
