
#include <Processing/Scheduling/ScheduledProcess.h>
#include <Processing/Scheduling/ObservedPolicies.h>
#include <gtest/gtest.h>

class ObservedPoliciesTest : public ::testing::Test
{
	protected:
		ObservedPoliciesTest( ) { }

		~ObservedPoliciesTest( ) { }

		virtual void SetUp( ) { }
		virtual void TearDown( ) { }
}; 

using ::tools::ScheduledProcess;
using ::tools::CompletedRunPolicyObserver;
using ::tools::SkippedRunPolicyObserver;
using ::tools::CompletedRunPolicySubject;
using ::tools::SkippedRunPolicySubject;
using ::tools::CompletedRunPolicyObserverSPtr;
using ::tools::SkippedRunPolicyObserverSPtr;
using ::tools::DelayPolicy;
using ::tools::IntervalPolicy;

TEST( ObservedPoliciesTest, OneObservedIsNotified )
{
	class CompletedObserver: public CompletedRunPolicyObserver
	{
		public:
			int * valueC;

			CompletedObserver( int * valueC )
				: valueC( valueC )
			{ }

			virtual void completedNotification( )
			{
				( *valueC )++;
			}

			virtual void addCompletedSubject( ) final override{ }
	};

	class SkippedObserver: public SkippedRunPolicyObserver
	{
		public:
			int * valueS;

			SkippedObserver( int * valueS )
				: valueS( valueS )
			{ }

			virtual void skippedNotification( )
			{
				( *valueS )++;
			}
	};

	class testProcess: public ScheduledProcess< CompletedRunPolicySubject, SkippedRunPolicySubject, DelayPolicy, IntervalPolicy >
	{
		public:
			int * value;

			virtual void run( )
			{
				( *value )++;
			}
	};

	int testValue = 0;
	int expectedValue = 2;


	testProcess process;

	CompletedRunPolicyObserverSPtr pCompletedObserver( new CompletedObserver( &testValue ) );
	SkippedRunPolicyObserverSPtr pSkippedObserver( new SkippedObserver( &testValue ) );

	process.addCompletedObserver( pCompletedObserver );
	process.addSkippedObserver( pSkippedObserver );

	process.value = &testValue;

	process.execute( );

	EXPECT_EQ( testValue, ( *process.value ) );
	EXPECT_EQ( expectedValue, ( *process.value ) );
}
