
#include <Processing/Scheduling/ScheduledProcess.h>
#include <gtest/gtest.h>

class ScheduledProcessTest : public ::testing::Test
{
	protected:
		ScheduledProcessTest( ) { }

		~ScheduledProcessTest( ) { }

		virtual void SetUp( ) { }
		virtual void TearDown( ) { }
};

using ::tools::ScheduledProcess;

TEST( ScheduledProcessTest, RunIsCalled )
{
	class testProcess: public ScheduledProcess< >
	{
		public:
			int * value;

			virtual void run( )
			{
				( *value )++;
			}
	};

	unsigned int expectedPrioriry = 10;
	int testValue = 0;
	int expectedValue = 1;

	testProcess process;
	process.value = &testValue;

	process.setPriority( expectedPrioriry );

	process.execute( );

	EXPECT_EQ( testValue, ( *process.value ) );
	EXPECT_EQ( expectedValue, ( *process.value ) );

	EXPECT_EQ( expectedPrioriry, process.getPriority( ) );
}
