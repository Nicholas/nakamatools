
#include <gtest/gtest.h>

#include <Processing/ProcessExecutor.h>
#include <Processing/BalancerDispatchPolicy.h>
#include <Processing/Process.h>

#include <memory>
#include <chrono>

class BalancerDispatchPolicyTest : public ::testing::Test
{
	protected:
		BalancerDispatchPolicyTest( ) { }
		virtual ~BalancerDispatchPolicyTest( ) { }

		virtual void SetUp( ) { }
		virtual void TearDown( ) { }
};

using namespace ::std::chrono_literals;
using ::tools::ProcessExecutor;
using ::tools::Process;
using ::tools::ProcessSPtr;
using ::tools::JoinWorkersPolicy;
using ::tools::UseWorkersPolicy;
using ::tools::BalancerDispatchPolicy;
using ::std::shared_ptr;

TEST( BalancerDispatchPolicyTest, UsesEngineExits )
{
	ProcessExecutor< UseWorkersPolicy, BalancerDispatchPolicy > engineUse( 1 );

	//engine.initialiseDispatcher( 1, 1, 1, 1 );

	engineUse.run( );

	// Need to give the worker thread some time to get to the point where it will throw its exception.
	::std::this_thread::sleep_for( 50ms );	

	engineUse.shutdown( );
	EXPECT_THROW( engineUse.join( ), ::std::runtime_error );
}

TEST( BalancerDispatchPolicyTest, JoinsEngineExits )
{
	ProcessExecutor< JoinWorkersPolicy, BalancerDispatchPolicy > engineJoin( 1 );

	//engine.initialiseDispatcher( 1, 1, 1, 1 );

	EXPECT_THROW( engineJoin.run( ), ::std::runtime_error );
}

TEST( BalancerDispatchPolicyTest, DispatchProvidesEventsAsExpected )
{
	class TestDispatch: public BalancerDispatchPolicy
	{
		private:
			std::atomic< bool > running;
			std::atomic< bool > shuttingDown;

		public:
			TestDispatch( )
				: BalancerDispatchPolicy( 4 )
			{ }

			std::atomic< bool > & isRunning( )
			{
				running = true;

				return running;
			}

			std::atomic< bool >& isShuttingDown( )
			{
				shuttingDown = false;
				return shuttingDown;
			}

	};

	class TestProcess: public Process
	{
		public:
			virtual void execute( )
			{ }
	};

	typedef std::shared_ptr< TestProcess > SP_TestProcess;

	TestDispatch dispatch;

	dispatch.initialiseDispatcher( 1, 4, 1, 4 );

	size_t thread_0 = 0;
	size_t thread_1 = 1;
	size_t thread_2 = 2;
	size_t thread_3 = 3;

	SP_TestProcess expected_0( new TestProcess );
	SP_TestProcess expected_1( new TestProcess );
	SP_TestProcess expected_2( new TestProcess );
	SP_TestProcess expected_3( new TestProcess );
	SP_TestProcess expected_4( new TestProcess );
	SP_TestProcess expected_5( new TestProcess );
	SP_TestProcess expected_6( new TestProcess );

	expected_0->setPriority( 0 );
	expected_1->setPriority( 1 );
	expected_2->setPriority( 2 );
	expected_3->setPriority( 3 );
	expected_4->setPriority( 4 );
	expected_5->setPriority( 5 );
	expected_6->setPriority( 6 );

	dispatch.addSync( expected_0, thread_0 );
	dispatch.addSync( expected_1 );
	dispatch.addSync( expected_2 );
	dispatch.addSync( expected_6 );

	dispatch.addAsync( expected_3 );
	dispatch.addAsync( expected_4 );
	dispatch.addAsync( expected_5 );

	ProcessSPtr actual_0 = dispatch.next( thread_0 );
	ProcessSPtr actual_5 = dispatch.next( thread_0 );
	ProcessSPtr actual_6 = dispatch.next( thread_1 );
	ProcessSPtr actual_4 = dispatch.next( thread_2 );
	ProcessSPtr actual_2 = dispatch.next( thread_3 );
	ProcessSPtr actual_3 = dispatch.next( thread_2 );
	ProcessSPtr actual_1 = dispatch.next( thread_0 );

	//printf( "%d == %d\n", expected_6->getPriority( ), actual_6->getPriority( ) );
	//printf( "%d == %d\n", expected_5->getPriority( ), actual_5->getPriority( ) );
	//printf( "%d == %d\n", expected_2->getPriority( ), actual_2->getPriority( ) );
	//printf( "%d == %d\n", expected_4->getPriority( ), actual_4->getPriority( ) );
	//printf( "%d == %d\n", expected_1->getPriority( ), actual_1->getPriority( ) );
	//printf( "%d == %d\n", expected_3->getPriority( ), actual_3->getPriority( ) );
	//printf( "%d == %d\n", expected_0->getPriority( ), actual_0->getPriority( ) );

	EXPECT_EQ( expected_6, actual_6 );
	EXPECT_EQ( expected_5, actual_5 );
	EXPECT_EQ( expected_2, actual_2 );
	EXPECT_EQ( expected_4, actual_4 );
	EXPECT_EQ( expected_1, actual_1 );
	EXPECT_EQ( expected_3, actual_3 );
	EXPECT_EQ( expected_0, actual_0 );

	EXPECT_EQ( 6, actual_6->getPriority( ) );
	EXPECT_EQ( 5, actual_5->getPriority( ) );
	EXPECT_EQ( 2, actual_2->getPriority( ) );
	EXPECT_EQ( 4, actual_4->getPriority( ) );
	EXPECT_EQ( 1, actual_1->getPriority( ) );
	EXPECT_EQ( 3, actual_3->getPriority( ) );
	EXPECT_EQ( 0, actual_0->getPriority( ) );

	// NOTE: I have see this fail...
	// Priority order is broken by having 6 from the shared queue execute first rather than the expected
	// using ::std::numeric_limits< size_t >::infinity( ); for the shared queue id could be the issue
	// but I haven't yet seen the issue with ::std::numeric_limits< size_t >::max( );
}
