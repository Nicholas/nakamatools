
#include <gtest/gtest.h>

#include <Processing/ProcessExecutor.h>
#include <Processing/Process.h>

#include <memory>
#include <chrono>

class ProcessEngineTest : public ::testing::Test
{
	protected:
		ProcessEngineTest( ) { }
		~ProcessEngineTest( ) { }

		virtual void SetUp( ) { }
		virtual void TearDown( ) { }
};

using namespace ::std::chrono_literals;
using ::tools::ProcessExecutor;
using ::tools::Process;
using ::tools::ProcessSPtr;
using ::tools::JoinWorkersPolicy;
using ::std::shared_ptr;

TEST( ProcessEngineTest, ProcessEngineExits )
{

// Now we have a dad lock issue here...
// Some how this doesn't clean up a process executor...


// Could there be a time where the thread starts
// but then it doesn't get to its conditional wait
// before the main thread does its kill notification?

// Does this mean I need to read more about
// double locking threads...

// The sleep seems to "fix"/Hide the issue
// What I need is a debugger that I can attach and step
// through the code to see why its gone and deadlocked
// (I can tell this is the case because there is no cpu
// activity).

{
	ProcessExecutor< > engine( 1 );

	engine.run( );
//	::std::this_thread::sleep_for( 50ms );
	engine.shutdown( );
	engine.join( );

	engine.run( );
}

{
	ProcessExecutor< > blankEngine( 2 );
}

}

TEST( ProcessEngineTest, ExecutesConcurrentProcesses )
{
	class TestProcess: public Process
	{
		public:
			int * value;

			virtual void execute( )
			{
				( *value )++;
			}
	};

	class ShutdownProcess: public Process
	{
		public:
			ProcessExecutor< > * engine;

			virtual void execute( )
			{
				engine->shutdown( );
			}
	};

	int testValue = 10;
	int expectedValue = 12;

	ProcessExecutor< > engine( 1 );

	shared_ptr< TestProcess > pProcess( new TestProcess );
	pProcess->value = &testValue;

	shared_ptr< ShutdownProcess > pShutdownProcess( new ShutdownProcess );
	pShutdownProcess->engine = &engine;

	engine.add( pProcess );
	engine.add( pProcess );
	engine.add( pShutdownProcess );

	engine.run( );

	engine.join( );

	EXPECT_EQ( testValue, *pProcess->value );
	EXPECT_EQ( expectedValue, *pProcess->value );
}

TEST( ProcessEngineTest, JoinWorkersExecutesProcessOnMainThread )
{
	class ShutdownProcess: public Process
	{
		public:
		ProcessExecutor< JoinWorkersPolicy > * engine;

		virtual void execute( )
		{
			engine->shutdown( );
		}
	};

	ProcessExecutor< JoinWorkersPolicy > engine( 0 );

	std::shared_ptr< ShutdownProcess > pShutdownProcess( new ShutdownProcess );
	pShutdownProcess->engine = &engine;

	engine.add( pShutdownProcess );

	engine.run( );
}

// TODO more advanced tests
